<?php 

class Home extends Frontend_Controller{



	public function __construct ()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('cart_model');
		$this->load->model('product_m');
		$this->load->model('banner_m');
		$this->load->model('product_image_m');
		$this->load->model('slider_m');
	}
	
	
function index()
	{	
	
		$this->data['home']=true;
        $this->data['slider'] = $this->slider_m->get();
		$this->data['content'] = $this->cart_model->get();
		$this->data['subview'] = 'Home/index';
		$this->load->view('_view', $this->data);
	}
function products()
	{
		$this->data['subview'] = 'products';
		$this->load->view('_view', $this->data);
	}
function details()
	{
		$this->data['subview'] = 'details';
		$this->load->view('_view', $this->data);
	}
	
function search()
	{
		error_reporting(E_ALL);
		$start =0;
		$this->data['home']=false;
		$search = $this->input->get('search'); 
		if(is_numeric($this->input->get('per_page'))){
		$start = $this->input->get('per_page'); 
		}

		$this->load->library('pagination');
		
		$this->data['products'] = $this->product_m->search_product($search,20,$start);		
		$config['page_query_string'] = TRUE;
		$config['base_url']=site_url().'/shop/search?search='.$search;
		$config['total_rows']=$this->product_m->search_product_count($search);
		$config['per_page']=20;
		$config['full_tag_open'] = '<nav class="projects-pagination pagination"><ul class="page-numbers">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><span class="page-numbers current">';
		$config['cur_tag_close'] = '</span></li>';
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$config['uri_segment'] = 4;
		$config['num_links'] = 5;
	
		$this->pagination->initialize($config);
		$this->data['pages_p']=$this->pagination->create_links();

		$this->data['search']=$search;
		$this->data['subview'] = 'search';
		$this->load->view('_view', $this->data);
	}
	
	function psearch(){
		
		if($this->input->get('search')){
		$query = $this->input->get('search');
		$this->load->database();
		$this->db->select('id,small_image_name,name')->from('products')->limit(5,0);
		$this->db->like('name', $query);
		$query=$this->db->get();
		$array= $query->result();
		$array2 = array('name' => '<p class="more">...</p>', 'small_image_name' => '', 'id' => '../../shop/search?search='.$this->input->get('search').'');
		array_push($array,$array2);
		echo json_encode($array);
}
		
	}
	function cart()
	{
		
		$this->data['subview'] = 'cart_review';
		$this->load->view('_view', $this->data);
	}
	

	function forgot_password(){
		// Redirect a visitor if he's already logged in
		$dashboard = 'shop';
		$this->visitor_m->loggedin() == FALSE || redirect($dashboard);
		
		$email= trim(strip_tags($this->input->post('email')));

		// Load view
		$this->data['subview'] = 'client/visitor/forgot_password';
		$this->load->view('client/_view', $this->data);

	}

function about_us()
	{
		
		$this->data['subview'] = 'about';
		$this->load->view('_view', $this->data);
	}
	function contact()
	{
		
		$this->data['subview'] = 'contact';
		$this->load->view('_view', $this->data);
	}
}