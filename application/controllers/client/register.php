<?php 

class Register extends MY_Controller{



	public function __construct ()
	{
		parent::__construct();
		$this->load->model('product_m');
		$this->load->model('cart_model');
		$this->load->helper ('form');
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'session' );
		$this->load->model ( 'visitor_m' );
	
	}
	function index($id=NULL)
	{
			// Fetch a visitor or set a new one
		if ($id) {
			$this->data['visitor'] = $this->visitor_m->get($id);
			count($this->data['visitor']) || $this->data['errors'][] = 'visitor could not be found';
		}
		else {
			$this->data['visitor'] = $this->visitor_m->get_new();
		}
		
		// Set up the form
		$rules = $this->visitor_m->rules_client;
		$id || $rules['password']['rules'] .= '|required';
		$this->form_validation->set_rules($rules);
		
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->visitor_m->array_from_post(array('name', 'email', 'password','phone','ip'));
			$data['password'] = $this->visitor_m->hash($data['password']);
			$this->visitor_m->save($data, $id);
			redirect('order/cart');
		}
		
		// Load the view
		$this->data['subview'] = 'client/visitor/edit';
		$this->load->view('client/_view', $this->data);
	}
	
}
