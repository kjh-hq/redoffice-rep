<?php
class Visitor extends Client_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('product_m');
	}

	public function index ()
	{
		// Fetch all visitors
		//$this->data['visitors'] = $this->visitor_m->get();
		
		// Load view
		//$this->data['subview'] = 'client/visitor/index';
		//$this->load->view('client/_view', $this->data);
	}

	public function register ()
	{
		// Fetch a visitor or set a new one
		
			$this->data['visitor'] = $this->visitor_m->get_new();
		
		
		// Set up the form
		$rules = $this->visitor_m->rules_client;
		$id || $rules['password']['rules'] .= '|required';
		$this->form_validation->set_rules($rules);
		
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->visitor_m->array_from_post(array('name', 'email', 'password'));
			$data['password'] = $this->visitor_m->hash($data['password']);
			$this->visitor_m->save($data);
			redirect('order/cart');
		}
		
		// Load the view
		$this->data['subview'] = 'client/visitor/edit';
		$this->load->view('client/_view', $this->data);
	}

	public function delete ($id)
	{
		$this->visitor_m->delete($id);
		redirect('client/visitor');
	}

	public function login ()
	{
		// Redirect a visitor if he's already logged in
		$dashboard = 'shop/cart';
		$this->visitor_m->loggedin() == FALSE || redirect($dashboard);
		
		// Set form
		$rules = $this->visitor_m->rules;
		$this->form_validation->set_rules($rules);
		
		// Process form
		if ($this->form_validation->run() == TRUE) {
			// We can login and redirect
			if ($this->visitor_m->login() == TRUE) {
				redirect($dashboard);
			}
			else {
				$this->session->set_flashdata('error', 'That email/password combination does not exist');
				redirect('client/visitor/login', 'refresh');
			}
		}
		
		// Load view
		$this->data['subview'] = 'client/visitor/login';
		$this->load->view('client/_view', $this->data);
	}



	public function logout ()
	{
		$this->visitor_m->logout();
		redirect('client/visitor/login');
	}

	public function _unique_email ($str)
	{
		// Do NOT validate if email already exists
		// UNLESS it's the email for the current visitor
		
		$id = $this->uri->segment(4);
		$this->db->where('email', $this->input->post('email'));
		!$id || $this->db->where('id !=', $id);
		$visitor = $this->visitor_m->get();
		
		if (count($visitor)) {
			$this->form_validation->set_message('_unique_email', '%s should be unique');
			return FALSE;
		}
		
		return TRUE;
	}
}