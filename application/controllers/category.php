<?php 

class Category extends MY_Controller{



	public function __construct ()
	{
		parent::__construct();
error_reporting(E_ALL);
		$this->load->model('cart_model');
		$this->load->model('page_m');
		$this->load->model('product_m');
		$this->load->model('product_image_m');
		$this->load->model('product_file_m');
	}
	

function grid($id)
	{
		
		$this->data['id']=$id;
		$this->data['categories'] = $this->page_m->get_child_of($id);


		$this->data['category'] = $this->page_m->get($id);
		if($this->data['category']->_parent_id==0){
		$this->data['logo'] = $this->data['category']->image;
		}else{
			$parent = $this->page_m->get($this->data['category']->_parent_id);
			$this->data['logo'] =$parent->image;
		}

		//$this->load->view('catagory',$this->data);
		$this->data['subview'] = 'mcatagory';
		$this->load->view('_view', $this->data);
	
	}
	
	function details($id,$start=0)
	{
		
		$this->data['id']=$id;
		$this->load->library('pagination');
		if($this->input->get('s_type')){
			if($this->input->get('s_type')=='s'){
				$this->data['products'] = $this->product_m->get_product_sort($id,20,$start,$this->input->get('sort'),$this->input->get('order'));
				$config['per_page']=20;
			}elseif($this->input->get('s_type')=='l'){
			$this->data['products'] = $this->product_m->get_product($id,$this->input->get('limit'),$start);
			$config['per_page']=$this->input->get('limit');
			}
		
		}else{
		$this->data['products'] = $this->product_m->get_product($id,20,$start);
		$config['per_page']=20;
		}
		
		
		$config['base_url']=site_url().'/ps/category/'.$id.'/';
		$config['total_rows']=$this->product_m->get_product_count($id);
		
		$config['full_tag_open'] = '<div class="paging-pages"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="btn btn-current">';
		$config['cur_tag_close'] = '</a></li>';
		$config['next_tag_open'] = "<div class='col m-3 text-right next'>";
		$config['next_tagl_close'] = "</div>";
		$config['prev_tag_open'] = "<div class='col m-3 text-left prev'>";
		$config['prev_tagl_close'] = "</div>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$config['uri_segment'] = 4;
		$config['num_links'] = 5;
	
		$this->pagination->initialize($config);
		$this->data['pages_p']=$this->pagination->create_links();
		$this->data['content'] = $this->cart_model->get();
		$this->data['category'] = $this->page_m->get($id);
		if($this->data['category']->_parent_id==0){
		$this->data['logo'] = $this->data['category']->image;
		}else{
			$parent = $this->page_m->get($this->data['category']->_parent_id);
			$this->data['logo'] =$parent->image;
		}

		//$this->load->view('catagory',$this->data);
		$this->data['subview'] = 'catagory';
		$this->load->view('_view', $this->data);
	
	}

}