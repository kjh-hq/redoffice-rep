<?php 

class Product extends MY_Controller{



	public function __construct ()
	{
		parent::__construct();
error_reporting(E_ALL);
		$this->load->model('cart_model');
		$this->load->model('page_m');
		$this->load->model('product_m');
		$this->load->model('product_image_m');
		$this->load->model('product_file_m');
	}
	

	function details($id)
	{
		$this->data['id']=$id;
		$this->data['seo'] = true;
		$this->data['products'] = $this->product_m->get($id);
		$this->data['relatedProducts'] = $this->product_m->get_related($id);
		$this->data['downloadfile'] = $this->product_file_m->selectdownloadfile($id,'downloadfile');
		$this->data['optionfile'] = $this->product_file_m->selectfile($id,'optionfile');
		$this->data['cfile'] = $this->product_file_m->selectfile($id,'cfile');
		if(!empty($this->data['products']->_related_to) && json_decode($this->data['products']->_related_to,true)){
		$related = array_keys(json_decode($this->data['products']->_related_to,true));
		$this->data['related'] = $this->product_m->get_related_products_with_img(implode(',', $related));
		}else{
			$this->data['related'] = array();
		}
		if($this->data['products']->_manufacturer){
		$this->data['category'] = $this->page_m->get($this->data['products']->_manufacturer);
		$this->data['logo'] = $this->data['category']->image;
		}

		$this->data['content'] = $this->cart_model->get();
		//$this->load->view('product',$this->data);
		$this->data['subview'] = 'products';
		$this->load->view('_view', $this->data);
	
	}

}