<?php
class Client_Controller extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ('form');
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'session' );
		$this->load->model ( 'visitor_m' );

		// Login check
		$exception_uris = array(
				'client/visitor/login',
				'client/visitor/logout'
		);
		if (in_array(uri_string(), $exception_uris) == FALSE) {
			if ($this->visitor_m->loggedin() == FALSE) {
				redirect('client/visitor/login');
			}
		}
		
		
		
	}
}
