
<aside id="column-left" role="complementary">


	<div class="box sticker_left">
		<div class="box-heading">Bestsellers</div>
		<div class="box-content">
			<!-- Megnor Start-->

			<div class="box-product productbox-grid" id="bestseller-grid">
				<!-- Megnor End-->
<?php foreach($products2 as $p): ?>

				<div class="product-items">
					<div class="product-block">
						<div class="product-block-inner" style="text-align:right;">
							<div class="image">
								<a href="<?php echo site_url();?>ps/product/<?php echo $p->id; ?>"><img
									src="<?php echo base_url(); ?>public/img_upload/<?php echo $p->small_image_name; ?>"
									alt="<?php echo $p->name; ?>"></a>
							</div>


							<div class="">
								<a href="<?php echo site_url();?>ps/product/<?php echo $p->id; ?>"><?php echo $p->name; ?></a>
							</div>
							<?php  
										$i=0;
										if($p->price){$i++;};
										if($p->sprice){$i++;};
										if($p->oprice){$i++;};
										if($p->cprice){$i++;};
										?>
										<?php if($i==1):?>
										
										<div class=""><?php
						
										if($p->price){$price=$p->price;};
										if($p->sprice){$price=$p->sprice;};
										if($p->oprice){$price=$p->oprice;};
										if($p->cprice){$price=$p->cprice;};
									
										?>
										<?php if($p->discount):?>
										<span class="price-old">TK.<?php echo $price;?></span> <span class="price-new">TK.<?php echo $price-$p->discount;?></span>
										<?php else:?>
										TK.<?php echo $price;?>
										<?php endif;?>
										</div> 
										<?php else: ?>
										<div class=""><?php
						
										$l=1;
							if($p->price && $l){$price=$p->price;$l=0;};
							if($p->sprice && $l){$price=$p->sprice;$l=0;};
							if($p->oprice && $l){$price=$p->oprice;$l=0;};
							if($p->cprice && $l){$price=$p->cprice;$l=0;};
									
										?>
										<?php if($p->discount):?>
										<span class="price-old">TK.<?php echo $price;?></span> <span class="price-new">TK.<?php echo $price-$p->discount;?></span>
										<?php else:?>
										TK.<?php echo $price;?>
										<?php endif;?>
										</div> 
										
										<?php endif; ?>
							<div class="">
								<?php echo anchor(site_url().'ps/product/' . $p->id, 'Buy Now','class="button" style="float:right;"'); ?>
							</div>
						</div>
					</div>
				</div>
				<br>
				<hr>
				<?php endforeach;?>
			</div>
		</div>
	</div>
	<span class="bestseller_default_width"
		style="display: none; visibility: hidden"></span>


</aside>