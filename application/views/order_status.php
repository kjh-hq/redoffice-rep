<?php 
error_reporting(E_ALL);?>
<div class="main-container full-width">
<div class="container-fluid">
<div class="breadcrumbs">
<a href="<?php echo site_url();?>">Home</a>
<span class="separator">/</span>
<span>Order Confirmation </span>
</div>	</div>
<?php if ($_SESSION['sid']):?>
<header class="entry-header">
<div class="container-fluid">
<h1 class="entry-title">Order Confirmation</h1>
</div>
</header>
<div class="page-content">
<div class="container-fluid">
<h5 style="text-align: center;color:#26B319 ">Your order has been placed. Thank you for shopping with us.</h5>
<table class="shop_table cart" cellspacing="0">
<thead>
<tr>
	<th>
		Order Status
	</th>
	<th>
		Delivery Address
	</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<div >
Order No: <b><?php echo $id;?></b>
</div>
<div >Placed:<?php $da= new DateTime( $confirm_order->created);echo '&nbsp'; 
echo $da->format('g:i:s');echo '&nbsp';
echo $da->format('a');echo '&nbsp';
echo $da->format('F');echo '&nbsp';
echo $da->format('d');echo '&nbsp';
echo $da->format('Y');
?></div>
<div >
Status: <a class="lnk-1">Processing</a>
</div>
</td>
<td>
<div>
<div >
<b><?php echo $confirm_order->name ?></b>
</div>
<div ><?php echo $confirm_order->addr ?>,<?php echo $confirm_order->area?>,<br /><?php echo $confirm_order->ct ?>,<?php echo $confirm_order->cnt ?></div>
<div >Phone: <?php echo $confirm_order->phone ?><br /><?php echo $confirm_order->phone2 ?></div>
<div >Email:  <?php echo $confirm_order->email ?></div>
</div>
</td>
</tr>
</tbody>
</table>
<!-- /shipping address -->
</div>
<!-- invoice table -->
<table class="shop_table cart" cellspacing="0">
<thead>
<tr>
<th>Sr</th>
<th>Image</th>
<th>Name</th>
<th>Item Description</th>
<th>Quantity</th>
<th>Price</th>
<th>Subtotal</th>
</tr>
</thead>
<tbody>
<!-- row 1 -->
<?php if(isset($id)):?>
<?php
$this->load->database ();
$sql = "select * from `order_list` where order_id=".$id."";
$query = $this->db->query ( $sql );
$r = $query->result ();
$i=1;
$total=0;
$_SESSION['sid']=0;
foreach ( $r as $r ) :
?><?php $p=$this->product_m->get($r->product_id)?>
<tr>
<td><?php echo $i;?></td>
<td><img  style="height:153px;width:153px;" src="<?php echo base_url();?>public/img_upload/medium/<?php echo $this->product_image_m->singleImage2($p->id);?>"></td>
<td><?php echo $p->name?></td>
<td><a href="<?php echo site_url();?>ps/product/<?php echo $p->id; ?>"><?php echo $p->name; ?></a></td>
<td><?php echo $this->cart->format_number($r->qty)?></td>
<td><?php echo $this->cart->format_number($p->price-$p->discount);?></td>
<td><?php echo $this->cart->format_number($p->price-$p->discount);$total+=($p->price-$p->discount)*$r->qty;?></td>
</tr>
<?php $i++; ?>
<?php endforeach; ?>
<tr>
<td rowspan="2" colspan="5" class="border-0"></td>
<td>Subtotal</td>
<td><?php echo $this->cart->format_number($total)?></td>
</tr>
<tr>
<td>Shipping</td>
<td><?php echo $this->cart->format_number(40)?></td>
</tr>
<tr>
<td colspan="5" class="border-0"></td>
<td><b>Total</b></td>
<td><b><?php echo $this->cart->format_number($total+40)?> </b></td>
</tr>
</tbody>
</table>
</div>
</div>
<?php endif;?>
<?php else :?>
<h3>You don\'t have any items yet.</h3>
<?php endif;?>
</div>
</div>
</div>
<!-- content inner div end -->
</section>