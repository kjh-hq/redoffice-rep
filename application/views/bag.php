<aside role="complementary" id="column-right" class="sticker"
	style="margin-right: -185px;">
	<div class="box">

		<div class="cart">
			<div class="cart-content">
				<div class="cart-header">
					<div class="title">YOUR ORDER</div>
					<a class="btn btn-default btn-xs vendor-link"
						href="/restaurant/s5vt/burger-n-boost-dhanmondi"> Change Order </a>
				</div>

			

				<div class="cart-details">
					<div class="no-order">Order book!</div>
					<table>
						<tbody>
							<tr class="product">
								<td class="quantity">1</td>
								<td class="title"><span class="name"> Jalapeno Cheese Burger
										Single Layer </span>



									<div class="product-amount-controls">
										<a class="btn btn-xs btn-default"
											href="/cart/decrement-product/product/05323813cbdc94388a0323c05beead4d?currentUrl=%2Frestaurant%2Fs5vt%2Fburger-n-boost-dhanmondi">-</a>
										<a class="btn btn-xs btn-default"
											href="/cart/increment-product/product/05323813cbdc94388a0323c05beead4d?currentUrl=%2Frestaurant%2Fs5vt%2Fburger-n-boost-dhanmondi">+</a>
									</div></td>
								<td class="price">Tk 225.00</td>
							</tr>
						
						
						</tbody>
					</table>

				</div>

				<div class="checkout">
					<a class="btn btn-default btn-block btn-lg disabled" href="#"
						data-is-minimum-order-value-reached="false"
						data-customer-has-valid-location="true" rel="nofollow"> Proceed to
						checkout </a>
				</div>
			</div>

			<div class="cart-footer">
				<span class="opening-hours"> Delivery in 1h </span>
			</div>


		</div>

		<div class="cart-pulltab">
			<span class="total" style="display: none;">0</span> <span
				class="icon icon-map"></span>
		</div>
	</div>


</aside>