<!--begin content-->
<div id="box-content">
	<div class="container">
		<div class="row">
			<div class="slide-show">
				<div class="vt-slideshow">
					<ul>
					<?php if(count($slider)){ 
						$i=0;
						foreach($slider as $slider){ ?>
						<li class="slide<?php echo $i++;?>" data-transition="random"><img src="<?php echo base_url();?>public/img_upload/<?php echo $slider->image ?>" alt="" /></li>
						<?php } } ?>
							
					</ul>
				
				</div>
			</div>
		</div>
	</div>
<!-- 	<div class="position-10">
		<div class="container">
			<div class="title">
			<h5>HomeControl+ App, Security</h5><h5>& Cameras</h5>
			<h6>Control your home from anywhere in the world with HomeControl+</h6>
			<div class="content">
			<?php $after_slider_1 = $this->banner_m->getBannerByPosition('after_slider_1');?>
				<a class="img01" href="#"><img src="<?php echo base_url();?>public/img_upload/<?php echo $after_slider_1->image?>" alt=""/></a>
				<?php $after_slider_2 = $this->banner_m->getBannerByPosition('after_slider_2');?>
				<a class="img02" href="#"><img src="<?php echo base_url();?>public/img_upload/<?php echo $after_slider_2->image?>" alt=""/></a>
				<?php $after_slider_3 = $this->banner_m->getBannerByPosition('after_slider_3');?>
				<a class="img03" href="#"><img src="<?php echo base_url();?>public/img_upload/<?php echo $after_slider_3->image?>" alt=""/></a>
				<?php $after_slider_4 = $this->banner_m->getBannerByPosition('after_slider_4');?>
				<a class="img04" href="#"><img src="<?php echo base_url();?>public/img_upload/<?php echo $after_slider_4->image?>" alt=""/></a>
			</div>
			</div>
		</div>
	</div> -->

	<div class="position-02">
		<div class="container">
		  <div class="row">
			<div class="title-sp">
			BEST SELLING
			<!-- <ul class="view-more">
			<li><a href="#">View more Wireless Systems</a></li>
			<li><a href="#">View more Hybrid Systems</a></li>
			</ul> -->
			</div>
			<div class="block vt-slider vt-slider3">
			   <div class="slider-inner">
				  <div class="container-slider">
					 <div class="products-grid">
						<?php $sales_product=$this->product_m->get_sales_type_products(2);?>
<?php foreach ($sales_product as $key) {?>
						<div class="item">
						   <div class="item-wrap">
							  <div class="item-image">
									 <a class="product-image no-touch" href="<?php echo $key->_link?>" title="<?php echo $key->name;?>">
									 <img class="first_image" src="<?php echo base_url();?>public/img_upload/medium/<?php echo $this->product_image_m->singleImage1($key->id);?>" alt="<?php echo $key->name;?>" /> 
									 </a>
									 
									
								  </div>
								  <div class="pro-info">
									 <div class="pro-inner">
										<div class="pro-title product-name"><a href="<?php echo $key->_link?>"><?php echo $key->name;?></a></div>
										
									 </div>
								  </div>
							   </div>
						   <!--end item wrap -->
							</div>
<?php }?>
						</div>
					 </div>
					 <div class="navslider">
						<a class="prev" href="#">Prev</a>
						<a class="next" href="#">Next</a>
					</div>
				  </div>
			</div>
			
		  </div>
	   </div>
	</div>

	<div class="position-02">
	<div class="container">
			<div class="row">
			<div class="title-sp">
			NEWEST DEALS
			<!-- <div class="std">
			Get these exclusive online store products.
			</div> -->
			</div>
			<div class="block vt-slider vt-slider3">
			   <div class="slider-inner">
				  <div class="container-slider">
					 <div class="products-grid">
<?php $sales_product=$this->product_m->get_sales_type_products(1);?>
<?php foreach ($sales_product as $key) {?>
						<div class="item">
						   <div class="item-wrap">
							  <div class="item-image">
									 <a class="product-image no-touch" href="<?php echo $key->_link?>" title="<?php echo $key->name;?>">
									 <img class="first_image" src="<?php echo base_url();?>public/img_upload/medium/<?php echo $this->product_image_m->singleImage1($key->id);?>" alt="<?php echo $key->name;?>" /> 
									 </a>
									 
									
								  </div>
								  <div class="pro-info">
									 <div class="pro-inner">
										<div class="pro-title product-name"><a style="text-align: center"  href="<?php echo $key->_link?>"><?php echo $key->name;?></a></div>
										
									 </div>
								  </div>
							   </div>
						   <!--end item wrap -->
							</div>
<?php }?>
						</div>
					 </div>
					 <div class="navslider">
						<a class="prev" href="#">Prev</a>
						<a class="next" href="#">Next</a>
					</div>
				  </div>
			
				</div>
			</div>
		</div>
	</div>
	<div class="position-02">
	<div class="container">
			<div class="row">
			<div class="title-sp">
			NEWS
			</div>
			<div class="block vt-slider vt-slider3">
			   <div class="slider-inner">
				  <div class="container-slider">
					 <div class="products-grid">
<?php $sales_product=$this->product_m->get_sales_type_products(1);?>
<?php foreach ($sales_product as $key) {?>
						<div class="item">
						   <div class="item-wrap">
							  <div class="item-image">
									 <a class="product-image no-touch" href="<?php echo $key->_link?>" title="<?php echo $key->name;?>">
									 <img class="first_image" src="<?php echo base_url();?>public/img_upload/medium/<?php echo $this->product_image_m->singleImage1($key->id);?>" alt="<?php echo $key->name;?>" /> 
									 </a>
									 
									
								  </div>
								  <div class="pro-info">
									 <div class="pro-inner">
										<div class="pro-title product-name"><a href="<?php echo $key->_link?>"><?php echo $key->name;?></a></div>
										
									 </div>
								  </div>
							   </div>
						   <!--end item wrap -->
							</div>
<?php }?>
						</div>
					 </div>
					 <div class="navslider">
						<a class="prev" href="#">Prev</a>
						<a class="next" href="#">Next</a>
					</div>
				  </div>
			
				</div>
			</div>
		</div>
	</div>
