<div class="box">
	<div class="custom_footer_main column">
		<div class="custom_footer_inner">
			<div class="box-cms">
				<div class="box-heading-cms"></div>
				<div class="box-content-cms">
					<div class="static_footer">
						<div class="static_footer_inner">
							<div class="block_1">
								<div class="about_icon">&nbsp;</div>

								<h3 class="title">About</h3>

								<ul class="about_content">
									<li>A3Books.com is the largest online store for Academic Books
										In Bangladesh. Buy Textbooks | Revision Guide | Question Paper
										| Markscheme, Online</li>
								</ul>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div id="footer" class="box">
		<div class="column">
			<h3>Information</h3>
			<ul>
				<li><a href="#/about-us.html">About us</a></li>
				<li><a href="#/BookShopbd-Customer-Service">Customer
						Service</a></li>
				<li><a href="#/return-policy">Return Policy</a></li>
				<li><a href="#/how-to-place-an-order">How to
						place an order</a></li>
				<li><a href="#/faq">FAQ</a></li>
			</ul>
		</div>
<?php if ($this->visitor_m->loggedin() == TRUE):?>

		<div class="column">
			<h3>My Account</h3>
			<ul>
				<li><a href="#/index.php?route=account/account">My
						Account</a></li>
				<li><a href="#/index.php?route=account/order">Order
						History</a></li>
				
			
			</ul>
		</div>
						
<?php else: ?>							
<?php endif; ?>	
		<div class="custom_footer_main_block column">
			<div class="custom_footer_inner_block">
				<div class="box-cms">
					<div class="box-heading-cms"></div>
					<div class="box-content-cms">
						<div class="contact">
							<h3>Contact us</h3>

							<ul>
								<li class="tel_no"><strong><?php echo $dashboard->phone?></strong></li>
								<li class="email_add"><a><?php echo $dashboard->email?></a></li>
							</ul>

							
						</div>

					</div>	
					</div>
					</div>
					</div>
					
					<div class="custom_footer_main_block column">
					<div class="custom_footer_inner_block">
				<div class="box-cms">
					<div class="box-heading-cms"></div>
					<h3>Follow Us</h3>
					<div class="box-content-cms">
													<div class="fb-like-box" data-href="https://www.facebook.com/a3books" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>
<div class="fb-follow" data-href="https://www.facebook.com/a3books" data-width="20px" data-colorscheme="light" data-layout="standard" data-show-faces="true"></div>


					</div>
					
				</div>
				</div>
				</div>
				</div>
			
		

		<div class="copyright-container">
			<div id="bottomfooter">
				<ul>
					
				</ul>
			</div>

			<div id="powered">A3Books.com  &copy; <?php echo date('Y');?></div>
			
		
	</div>


</div>


<span class="grid_default_width"
	style="display: none; visibility: hidden;"></span>
<span class="home_products_default_width"
	style="display: none; visibility: hidden"></span>
<span class="module_default_width"
	style="display: none; visibility: hidden"></span>


<style>
[name=search] {
	outline: none;
}

#livesearch,#livesearch * {
	margin: 0;
	padding: 0;
	list-style: none;
}

#livesearch {
	position: absolute;
	width: 200px;
	top: 0px;
	background: #ffffff;
	z-index: 100;
	box-shadow: 0px 10px 30px rgba(0, 0, 0, .5);
}

#livesearch li {
	border-top: 1px solid #eeeeee;
}

#livesearch a {
	display: block;
	clear: both;
	overflow: hidden;
	line-height: 20px;
	padding: 10px;
	text-decoration: none;
}

#livesearch a:hover,#livesearch li.active a {
	background: #38B0E3;
	color: #ffffff;
}

#livesearch img {
	float: left;
	width: 50px;
	height: 50px;
	margin-right: 10px;
}

#livesearch img[src=''] {
	display: none;
}

.more {
	text-align: center;
}

#livesearch a em {
	display: block;
	color: #888888;
	font-style: normal;
	font-weight: bold;
}

#livesearch a:hover em,#livesearch li.active a em {
	color: white;
}

#livesearch strike {
	color: #aaaaaa;
}

#livesearch a:hover strike {
	color: lightblue;
}

#livesearch small {
	display: block;
}
</style>

</div>