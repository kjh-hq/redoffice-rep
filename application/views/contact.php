<div class="container page-title">
    <div class="row">
        <div class="col m-10 m-centered">
            <h1>Contact Us</h1>
        </div>
    </div>
</div>
<div class="fullwidth border-gray-top section">
    <div class="container">
        <div class="row">
            <div class="col m-2"> </div>
            <div class="col m-6 m-offset-1 content-block">
            <h2>Head Office</h2>
                <p>
<table>
<tr><td colspan="2"><strong>RED OFFICE SUPPLIES LTD.</strong></td></tr>
<tr><td colspan="2">EASTERN COMMERCIAL COMPLEX,</td></tr>
<tr><td colspan="2">73 KAKRAIL, 7TH FLOOR, SUITE-12,</td></tr>
<tr><td colspan="2">RAMNA, DHAKA-1000, BANGLADESH.</td></tr>
<tr><td>CONTACT           </td><td>    : +88 (0)191 2907 415</td></tr>
<tr><td>INSTALLER SUPPORT   </td><td>  : +88 (0)199 2180 545</td></tr>
<tr><td>SALES               </td><td>  : +88 (0)199 2180 545</td></tr>
    
</table>
                
                    
                    
                    
                    
                    
                    
                </p>
                <hr/>
                <p>Get in touch with us about sales enquiries, feedback, testimonials, complaints; whatever you would like to talk to us about, we&#39;d love to hear from you!</p>
                <p>You can use this contact form to send us a message. Just put in a few details and use the drop down field to point your message in the right direction.</p>
                
                <form method="post" enctype="multipart/form-data" action="contact">
                    <div class="content-form form-validation">
                        <h3 class="section-title">Your company</h3>
                        <div class="row">
                            <div class="col m-6">
                                <input type="text" name="companyname" data-required="1" placeholder="Company Name *" class="content-form-input" /> </div>
                            <div class="col m-6">
                                <select class="content-form-select" name="businesstype" data-required="1">
                                    <option value="">Business Type *</option>
                                    <option value="Installer">Installer</option>
                                    <option value="End User">End User</option>
                                    <option value="Distributor">Distributor</option>
                                    <option value="Specifier">Specifier</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                        <h3 class="section-title">Your enquiry</h3>
                        <select class="content-form-select enquirytype" name="enquirytype" data-required="1">
                            <option value="">Enquiry type *</option>
                            <option value="Marketing & sales literature">Marketing & sales literature</option>
                            <option value="Technical manuals">Technical manuals</option>
                            <option value="Technical support & advice">Technical support & advice</option>
                            <option value="Order & sales enquiries">Order & sales enquiries</option>
                            <option value="Images request">Images request</option>
                            <option value="Other">Other</option>
                        </select>
                        <div class="enquirytype1 hidden">
                            <p>Did you know our marketing literature can be downloaded in the Pyronix knowledge base, simply <a href='/help-and-support/home-users/product-information'>click here</a> to access.</p>
                            <p>Register today to access to our full library of marketing, training and technical resources.</p>
                        </div>
                        <div class="enquirytype2 hidden">
                            <p>Did you know our technical manuals can be downloaded in the Pyronix knowledge base, simply <a href='#' id='login'>login</a> or <a href='/register'>register</a> an account to get access to these and other marketing, training and technical resources.</p>
                        </div>
                        <div class="enquirytype3 hidden">
                            <p>Did you know our product images can be downloaded in the Pyronix knowledge base, simply <a href='/help-and-support/installers-distributors/product-guides-manuals-and-documents'>click here</a> to access.</p>
                            <p>Register today to access to our full library of marketing, training and technical resources.</p>
                        </div>
                        <h3 class="section-title">Your details</h3>
                        <input type="text" name="name" data-required="1" placeholder="Name *" class="content-form-input" />
                        <input type="text" name="position" data-required="0" placeholder="Position" class="content-form-input" />
                        <div class="row">
                            <div class="col m-6">
                                <input type="text" name="address1" data-required="1" placeholder="Region *" class="content-form-input" /> </div>
                            <div class="col m-6">
                                <input type="text" name="address2" data-required="0" placeholder="Province" class="content-form-input" /> </div>
                        </div>
                        <div class="row">
                            <div class="col m-6">
                                <input type="text" name="address3" data-required="0" placeholder="City" class="content-form-input" /> </div>
                            <div class="col m-6">
                                <input type="text" name="country" data-required="0" placeholder="Country" class="content-form-input" /> </div>
                        </div>
                        <div class="row">
                            <div class="col m-6">
                                <input type="text" name="postcode" data-required="1" placeholder="Post code *" class="content-form-input" /> </div>
                            <div class="col m-6">
                                <input type="text" name="phoneno" data-required="1" placeholder="Phone Number *" class="content-form-input" /> </div>
                        </div>
                        <input type="text" name="email" data-required="1" placeholder="Email Address *" class="content-form-input" />
                        <h3 class="section-title">Your comments</h3>
                        <textarea name="comments" data-required="1" placeholder="Enter your comments *" rows="8" class="content-form-textarea"></textarea>
                        <input type="submit" class="btn btn-red" value="Send" />
                        <input type="text" name="foo" style="display: none;"> </div>
                </form>
                
            </div>
        </div>
    </div>
</div>