<style type="text/css">
	
strong {
  font-weight: 700; }
h1, h2, h3, h4 {
  color: inherit;
  font-weight: 700;
  line-height: 1.2;
  margin-bottom: 20px; }
h1 {
  font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: 700; }
h2, h3, h4 {
  font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-weight: 400; }
h1 {
  font-size: 315.7%; }
h2 {
  font-size: 236.9%; }
h3 {
  font-size: 177.7%; }
h4 {
  font-size: 133.3%; }
small {
  font-size: 75%; }
p {
  margin-bottom: 20px; }
   @media only screen and (min-width: 768px) {
    header {
      padding-bottom: 40px; } }
</style>
<div class="fullwidth fullwidth-light banner">
    <div class="container">
        <div class="row table">
            <div class="col m-6 table-cell-valignmid">
                    <img alt="" src="<?php echo base_url();?>public/img_upload/<?php echo $category->image?>">
                
            </div>
            <div class="col m-6 table-cell-valignmid">
                <div class="col m-10 m-centered">
                    <div class="inner">
                        <h2><?php echo $category->title?></h2>
                        <h3><?php echo strip_tags($category->description)?> </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fullwidth border-gray-top section">
    <div class="container">
        <div class="row">
             <div class="col m-9 m-offset-1">
                
 <div class="product-list product-list-top">
    <div class="row">
<?php 
foreach ($products as $key => $value) {?>
                <div class="col m-6 product-item">
                    <div class="table">
                        <div class="col m-4 table-cell-valignmid border-gray">
                            <a href="<?php echo $value->_link;?>"><img src="<?php echo base_url();?>public/img_upload/<?php echo $this->product_image_m->singleImage1($value->id);?>" alt=""/></a>
                        </div>
                        <div class="col m-8 table-cell-valignmid">
                            <div class="col m-11 m-centered">
                                <h2><a href="<?php echo $value->_link;?>"><?php echo $value->name;?></a></h2>
                                <h3><?php echo strip_tags($value->shortdesc)?></h3>
                            </div>
                        </div>
                    </div>
                </div>
             <?php } ?>
   </div>
</div> 
    <div class="paging">
        <div class="row">
            
            <div class="col m-6">
                <?php echo $pages_p?>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>
</div>
