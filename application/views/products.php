<div class="container page-title">
    <div class="row">
        <div class="col m-10 m-centered">
            <h1><?php echo $products->name?></h1>
        </div>
    </div>
</div>
<div class="fullwidth fullwidth-light">
    <div class="container">
        <div class="row table">
            <div class="col m-6 table-cell-valignmid">
                <img src="<?php echo base_url();?>public/img_upload/large/<?php echo $this->product_image_m->singleImage1($products->id);?>" alt="" />
            </div>
            <div class="col m-6 table-cell-valignmid content-block" style="margin-top:30px;">
                   <?php echo $products->product_feature_short ?>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="sub-menu m-12">
        <ul class="tabs">
            <li><a href="#tab1">Product Features</a></li>
            <!-- <li><a href="#tab2">Certification</a></li> -->
             <li><a href="#tab3">Technical Specification</a></li>
             <!-- <li><a href="#tab6">Product Codes &amp; Options</a></li> -->
            <!-- <li><a href="#tab4">Related Products &amp; Accessories</a></li> -->
            <li><a href="#tab5">Product Downloads</a></li>
        </ul>
    </div>
</div>
<div id="tab1">
    <div class="container">
        <div class="m-6 m-centered header-block header-block-narrow">
            <h2>Features</h2>
        </div>
        <div class="m-6 m-centered content-block">
<?php echo $products->product_features ?>
        </div>
    </div>
</div>
<!-- <div id="tab2">
    <div class="container">
        <div class="m-6 m-centered header-block header-block-narrow">
            <h2>Certification</h2>
        </div>
        <div class="m-6 m-centered content-block">
            <p style="text-align:center"><?php echo $products->certification ?></p>
<h4>Certificate Downloads:</h4>
<?php foreach($cfile as $value){?>
 <?php if($value->pfile_ext=='.jpg' || $value->pfile_ext=='.png' || $value->pfile_ext=='.gif'){?>
                                    <p><a href=""><img  style="height:50px; width:39px" src="<?php echo site_url();?>public/img_upload/<?php echo $value->pfile_filename;?>">
                                    <?php }else{ ?>
                                    <p><a href=""><img  style="height:50px; width:39px" src="<?php echo site_url();?>public/images/<?php echo $value->pfile_image;?>">
                                    <?php } ?>
 </a><a href="" target="_blank"><?php echo $value->pfile_filename;?></a></p>
  <?php } ?>
        </div>
    </div>
</div> -->
    <div id="tab3">
        <div class="container">
            <div class="col m-12 header-block">
                <h2>Technical Specifications</h2>
            </div>
            <div class="col m-12 technical-block">
                <div class="col m-9">
            <?php echo $products->technical_specifications ?>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-anchor-bottom"></div>
<!-- <div id="tab4">
    <div class="container">
        <div class="m-12 header-block">
            <h2>Related Products &amp; Accessories</h2>
        </div>
            <?php foreach($related as $kei=>$value){?>
                <div class="row downloads-block">
                    <div class="col m-3">
                        <h2><?php echo $kei?></h2>
                    </div>
                    <div class="col m-9 downloads-container downloads-container-top">
<?php foreach ($value as $key) { ?>
                            <div class="col m-4 downloads-item">
                                <div class="table">
                                    <div class="col m-4 table-cell-valignmid">
                                        <a href=""><img src="<?php echo site_url();?>public/img_upload/<?php echo $key->image;?>" alt="" /></a>
                                    </div>
                                    <div class="col m-8 table-cell-valignmid">
                                        <h3>
                                           <a href=""><?php echo $key->name?></a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                    
                    <?php }?>
                    </div>
                </div>
<?php } ?>
        
    </div>
</div> -->
<div id="tab5">
    <div class="container">
        <div class="m-12 header-block">
            <h2>Product Downloads</h2>
        </div>
<?php foreach($downloadfile as $kei=>$value){?>
                <div class="row downloads-block">
                    <div class="col m-3">
                        <h2><?php echo $kei?></h2>
                    </div>
                     <div class="col m-9 downloads-container downloads-container-top">
<?php foreach ($value as $key) { ?>
                   
                            <div class="col m-4 downloads-item">
                                <div class="table">
                                    <div class="col m-4 table-cell-valignmid">
                                        <a href=""><img src="<?php echo site_url();?>public/images/<?php echo $key->pfile_image;?>" alt="" /></a>
                                    </div>
                                    <div class="col m-8 table-cell-valignmid">
                                        <h3>
                                           <a href="<?php echo site_url();?>public/img_upload/<?php echo $key->pfile_filename?>"><?php echo $key->pfile_filename?></a>
                                        </h3>
                                        <span><?php echo strtoupper(str_replace('.', '', $key->pfile_ext));?> <?php echo round($key->pfile_size/1024,2);?> MB</span>
                                    </div>
                                </div>
                            </div>
                   
                    <?php }?>
                     </div>
                </div>
<?php } ?>
    </div>
</div>
 <!-- <div id="tab6">
        <div class="container">
            <div class="m-12 header-block">
                <h2>Product Codes &amp; Options</h2>
            </div>
            <div class="row downloads-block">
                <p style="text-align:center"><?php echo $products->code ?></p>
                <div class="col m-8 m-offset-2 downloads-container downloads-container-top">
                        <div class="col m-6 downloads-item">
                        <?php foreach($optionfile as $value){?>
                            <div class="table">
                                <div class="col m-4 table-cell-valignmid">
                                    <?php if($value->pfile_ext=='.jpg' || $value->pfile_ext=='.png' || $value->pfile_ext=='.gif'){?>
                                    <img src="<?php echo site_url();?>public/img_upload/<?php echo $value->pfile_filename;?>">
                                    <?php }else{ ?>
                                    <img src="<?php echo site_url();?>public/images/<?php echo $value->pfile_image;?>">
                                    <?php } ?>
                                </div>
                                <div class="col m-8 table-cell-valignmid">
                                    <h3><?php echo $value->pfile_name;?></h3>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                </div>
            </div>
        </div>
        </div> -->
