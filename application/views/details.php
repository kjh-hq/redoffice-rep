		<!-- ================================ END Navigation =================== -->
		<!-- section code start -->
		<section id="content-wrapper">
			<div class="content-inner">
				<div id="notification"></div>
				<aside id="column-left" role="complementary">
					<div id="banner0" class="banner">
						<div style="display: block;">
							<img
								src="<?php echo base_url();?>public/shop/delivery-charge50-186x98.gif"
								alt="Only 40 TK Delivery Charge"
								title="Only 40 TK Delivery Charge"><span class="hover_shine"> </span>
						</div>
					</div>
					<script type="text/javascript"><!--
$(document).ready(function() {
	$('#banner0 div:first-child').css('display', 'block');
});
var banner = function() {
	$('#banner0').cycle({
		before: function(current, next) {
			$(next).parent().height($(next).outerHeight());
		}
	});
}
setTimeout(banner, 2000);
//--></script>
					<div class="box">
						<div class="box-heading">Bestsellers</div>
						<div class="box-content">
							<!-- Megnor Start-->
							<div class="box-product productbox-grid" id="bestseller-grid">
								<!-- Megnor End-->
								<div class="product-items">
									<div class="product-block">
										<div class="product-block-inner">
											<div class="image">
												<a
													href="#/index.php?route=product/product&amp;product_id=161"><img
													src="<?php echo base_url();?>public/shop/a_level_physics_question_paper_3-80x80.jpg"
													alt="CIE A level Physics Paper 2 Question Paper"></a>
											</div>
											<div class="name">
												<a
													href="#/index.php?route=product/product&amp;product_id=161">CIE
													A level Physics Paper 2 Question Paper</a>
											</div>
											<div class="price">TK.200</div>
											<div class="cart">
												<input value="Add to Basket" onclick="addToCart('161');"
													class="button" type="button">
											</div>
										</div>
									</div>
								</div>
								<div class="product-items">
									<div class="product-block">
										<div class="product-block-inner">
											<div class="image">
												<a
													href="#/index.php?route=product/product&amp;product_id=166"><img
													src="<?php echo base_url();?>public/shop/a_level_chemistry_question_paper-80x80.jpg"
													alt="CIE A level Chemistry Paper 4 Question Paper"></a>
											</div>
											<div class="name">
												<a
													href="#/index.php?route=product/product&amp;product_id=166">CIE
													A level Chemistry Paper 4 Question Paper</a>
											</div>
											<div class="price">TK.220</div>
											<div class="cart">
												<input value="Add to Basket" onclick="addToCart('166');"
													class="button" type="button">
											</div>
										</div>
									</div>
								</div>
								<div class="product-items">
									<div class="product-block">
										<div class="product-block-inner">
											<div class="image">
												<a
													href="#/index.php?route=product/product&amp;product_id=159"><img
													src="<?php echo base_url();?>public/shop/a_level_physics_question_paper_3-80x80.jpg"
													alt="CIE A level Physics Paper 5 Question Paper"></a>
											</div>
											<div class="name">
												<a
													href="#/index.php?route=product/product&amp;product_id=159">CIE
													A level Physics Paper 5 Question Paper</a>
											</div>
											<div class="price">TK.160</div>
											<div class="cart">
												<input value="Add to Basket" onclick="addToCart('159');"
													class="button" type="button">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<span class="bestseller_default_width"
						style="display: none; visibility: hidden"></span>
					<div class="box">
						<div class="box-heading">Specials</div>
						<div class="box-content">
							<!-- Megnor Start-->
							<div class="box-product productbox-grid" id="special-grid">
								<!-- Megnor End-->
								<div class="product-items">
									<div class="product-block">
										<div class="product-block-inner">
											<div class="image">
												<a
													href="#/CGP-AS-Level-Physics-Edexcel-Complete-Revision-Practice-in-bangladesh"><img
													src="<?php echo base_url();?>public/shop/AS-Level%252520Physics%252520Edexcel%252520Complete%252520Revision%252.jpg"
													alt="CGP AS-Level Physics Edexcel Complete Revision &amp; Practice"></a>
											</div>
											<span class="saleicon sale">Sale</span>
											<div class="name">
												<a
													href="#/CGP-AS-Level-Physics-Edexcel-Complete-Revision-Practice-in-bangladesh">CGP
													AS-Level Physics Edexcel Complete Revision &amp; Practice</a>
											</div>
											<div class="price">
												<span class="price-old">TK.200</span> <span
													class="price-new">TK.180</span>
											</div>
											<div class="cart">
												<input value="Add to Basket" onclick="addToCart('131');"
													class="button" type="button">
											</div>
										</div>
									</div>
								</div>
								<div class="product-items">
									<div class="product-block">
										<div class="product-block-inner">
											<div class="image">
												<a
													href="#/edexcel-A2-Biology-Student-Book-in-bangladesh"><img
													src="<?php echo base_url();?>public/shop/Edexcel%2520A2%2520Biology%2520Student%2520Book-80x80.jpg"
													alt="Edexcel A2 Biology Student Book"></a>
											</div>
											<span class="saleicon sale">Sale</span>
											<div class="name">
												<a
													href="#/edexcel-A2-Biology-Student-Book-in-bangladesh">Edexcel
													A2 Biology Student Book</a>
											</div>
											<div class="price">
												<span class="price-old">TK.260</span> <span
													class="price-new">TK.245</span>
											</div>
											<div class="cart">
												<input value="Add to Basket" onclick="addToCart('105');"
													class="button" type="button">
											</div>
										</div>
									</div>
								</div>
								<div class="product-items">
									<div class="product-block">
										<div class="product-block-inner">
											<div class="image">
												<a
													href="#/Edexcel-AS-Biology-Student-Book-in-bangladesh"><img
													src="<?php echo base_url();?>public/shop/Edexcel%2520AS%2520Biology%2520Student%2520Book-80x80.jpg"
													alt="Edexcel AS Biology Student Book"></a>
											</div>
											<span class="saleicon sale">Sale</span>
											<div class="name">
												<a
													href="#/Edexcel-AS-Biology-Student-Book-in-bangladesh">Edexcel
													AS Biology Student Book</a>
											</div>
											<div class="price">
												<span class="price-old">TK.260</span> <span
													class="price-new">TK.245</span>
											</div>
											<div class="cart">
												<input value="Add to Basket" onclick="addToCart('104');"
													class="button" type="button">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<span class="special_default_width"
						style="display: none; visibility: hidden"></span>
				</aside>
				<div id="content">
					<div class="breadcrumb" xmlns:v="" id="brd-crumbs">
						<ul>
							<li typeof="v:Breadcrumb"><a property="v:title" rel="v:url"
								href="#/index.php?route=common/home"><span>Home</span></a></li>
							<li typeof="v:Breadcrumb">� <a property="v:title" rel="v:url"
								href="#/Cambridge-International-AS-and-A-Level-Chemistry-Coursebook-in-bangladesh"><span>Cambridge
										International AS and A Level Chemistry Coursebook</span></a></li>
						</ul>
					</div>
					<div class="product-info" itemscope=""
						itemtype="http://schema.org/Product">
						<div class="left product-image">
							<!-- Megnor Cloud-Zoom Image Effect Start -->
							<span class="image"><img id="cloud-zoom"
								src="<?php echo base_url();?>public/shop/Cambridge%252520International%252520AS%252520and%252520A%252520Level%252.jpg"
								title="Cambridge International AS and A Level Chemistry Coursebook"
								alt="Cambridge International AS and A Level Chemistry Coursebook"
								data-zoom-image="#/image/cache/data/Cambridge International AS and A Level Chemistry-500x500.jpg"></span>
						</div>
						<div class="right">
							<h1 class="item name fn" itemprop="name">Cambridge International
								AS and A Level Chemistry Coursebook</h1>
							<div class="description">
								<table class="product-description">
									<!-- Megnor <table> Start -->
									<tbody>
										<tr>
											<td><span>Product Code:</span></td>
											<td class="description-right">CA6601</td>
										</tr>
										<tr>
											<td><span>Availability:</span></td>
											<td class="description-right">In Stock</td>
										</tr>
									</tbody>
								</table>
								<!-- Megnor <table> End -->
							</div>
							<div class="price">
								Price: <span style="display: inline;" id="price_container">TK.0</span>
								<script type="text/javascript"
									src="<?php echo base_url();?>public/shop/index.php"></script>
							</div>
							<div class="options">
								<h2>Available Options</h2>
								<div id="option-265" class="option">
									<span class="required">*</span> <b>Type:</b><br>
									<div class="tm-radio">
										<input class="tm-hide" name="option[265]" value="107"
											id="option-value-107" type="radio">
									</div>
									<label for="option-value-107">Regular(low price edition) </label>
									<br>
									<div class="tm-radio tm-selected">
										<input checked="checked" class="tm-hide" name="option[265]"
											value="108" id="option-value-108" type="radio">
									</div>
									<label for="option-value-108">Standard (+TK.100) </label> <br>
								</div>
								<br>
							</div>
							<div class="cart">
								<div>
									Qty: <input name="quantity" size="2" type="text"> <input
										name="product_id" size="2" value="139" type="hidden"> &nbsp; <input
										value="Add to Basket" id="button-cart" class="button"
										type="button"> <span>&nbsp;&nbsp;- OR -&nbsp;&nbsp;</span> <span
										class="links"> <a onclick="addToWishList('139');"
										class="product_wishlist">Add to Wish List</a> <a
										onclick="addToCompare('139');" class="product_compare">Add to
											Compare</a></span>
								</div>
							</div>
							<div class="review">
								<div itemscope="" itemtype="http://schema.org/AggregateRating">
									<img itemprop="ratingValue"
										src="<?php echo base_url();?>public/shop/stars-0.png"
										alt="0 reviews">&nbsp;&nbsp;<a itemprop="reviewCount"
										onclick="$('a[href=\'#tab-review\']').trigger('click');">0
										reviews</a>&nbsp;&nbsp;&nbsp;&nbsp; <a
										onclick="$('a[href=\'#tab-review\']').trigger('click');"
										class="review-write">Write a review</a>
								</div>
								<div class="share">
									<!-- AddThis Button BEGIN -->
									<div class="addthis_default_style">
										<a href="#" class="addthis_button_compact at300m"><span
											class="at16nc at300bs at15nc at15t_compact at16t_compact"><span
												class="at_a11y">More Sharing Services</span></span>Share</a>
										<a href="#" title="Email" target="_blank"
											class="addthis_button_email at300b"><span
											class="at16nc at300bs at15nc at15t_email at16t_email"><span
												class="at_a11y">Share on email</span></span></a><a href="#"
											title="Print" class="addthis_button_print at300b"><span
											class="at16nc at300bs at15nc at15t_print at16t_print"><span
												class="at_a11y">Share on print</span></span></a> <a href="#"
											title="Facebook" class="addthis_button_facebook at300b"><span
											class="at16nc at300bs at15nc at15t_facebook at16t_facebook"><span
												class="at_a11y">Share on facebook</span></span></a> <a
											href="#" title="Tweet" class="addthis_button_twitter at300b"><span
											class="at16nc at300bs at15nc at15t_twitter at16t_twitter"><span
												class="at_a11y">Share on twitter</span></span></a>
									</div>
									<script type="text/javascript"
										src="<?php echo base_url();?>public/shop/addthis_widget.js"></script>
									<!-- AddThis Button END -->
								</div>
							</div>
						</div>
					</div>
					<div id="tabs" class="htabs">
						<a class="selected" style="display: inline;"
							href="#tab-description">Description</a> <a
							style="display: inline;" href="#tab-review">Reviews (0)</a>
					</div>
					<div style="display: block;" id="tab-description"
						class="tab-content">
						<p>
							<span
								style="font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;">by&nbsp;</span><span
								class="author notFaded" data-width=""
								style="box-sizing: border-box; font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;"><span
								class="a-declarative"
								data-a-popover="{&quot;position&quot;:&quot;triggerBottom&quot;,&quot;name&quot;:&quot;contributor-info-B005CES4C0&quot;,&quot;allowLinkDefault&quot;:&quot;true&quot;}"
								data-action="a-popover" style="box-sizing: border-box;">Roger
									Norris&nbsp;&nbsp;</span><span class="contribution"
								spacing="none" style="box-sizing: border-box;"><span
									class="a-color-secondary"
									style="box-sizing: border-box; color: rgb(136, 136, 136) !important;">(Author),&nbsp;</span></span></span><span
								class="author notFaded" data-width=""
								style="box-sizing: border-box; font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;">Lawrie
								Ryan&nbsp;<span class="contribution" spacing="none"
								style="box-sizing: border-box;"><span class="a-color-secondary"
									style="box-sizing: border-box; color: rgb(136, 136, 136) !important;">(Author),&nbsp;</span></span>
							</span><span class="author notFaded" data-width=""
								style="box-sizing: border-box; font-family: Arial, sans-serif; font-size: 13px; line-height: 19px; display: inline;">David
								Acaster<span class="contribution" spacing="none"
								style="box-sizing: border-box;"><span class="a-color-secondary"
									style="box-sizing: border-box; color: rgb(136, 136, 136) !important;">(Consultant
										Editor)</span></span>
							</span>
						</p>
						<p>
							<span
								style="font-family: Arial, sans-serif; font-size: 14px; line-height: 22.399999618530273px;">Covers
								all the requirements of the Cambridge International AS and A
								Level Chemistry syllabus (9701). Written by highly experienced
								authors, Cambridge International AS and A Level Chemistry
								comprehensively covers the Cambridge syllabus and is now
								available in both print and e-book formats. Simple and clear
								language, colourful photos and international examples make this
								book accessible to students around the world. Exam-style
								questions at the end of each chapter reinforce knowledge and
								skills and offer thorough exam practice. The print book includes
								a Student CD-ROM of supplementary materials including
								test-yourself-questions, study skills guidance, animations and
								advice on the practical examination. These supplementary
								materials (with the exception of the animations) are also
								included in the e-book version.</span>
						</p>
						<ul
							style="box-sizing: border-box; margin: 0px; list-style-type: none; padding-right: 0px; padding-left: 0px; font-family: Arial, sans-serif; font-size: 13px; line-height: 19px;">
							<li
								style="box-sizing: border-box; list-style-type: none; word-wrap: break-word; margin: 0px;"><b
								style="box-sizing: border-box; font-family: verdana, arial, helvetica, sans-serif;">Series:</b>&nbsp;Cambridge
								International Examinations</li>
							<li
								style="box-sizing: border-box; list-style-type: none; word-wrap: break-word; margin: 0px;"><b
								style="box-sizing: border-box; font-family: verdana, arial, helvetica, sans-serif;">Paperback:</b>&nbsp;559
								pages</li>
							<li
								style="box-sizing: border-box; list-style-type: none; word-wrap: break-word; margin: 0px;"><b
								style="box-sizing: border-box; font-family: verdana, arial, helvetica, sans-serif;">Publisher:</b>&nbsp;Cambridge
								University Press; Pap/Cdr edition (April 18, 2011)</li>
							<li
								style="box-sizing: border-box; list-style-type: none; word-wrap: break-word; margin: 0px;"><b
								style="box-sizing: border-box; font-family: verdana, arial, helvetica, sans-serif;">Language:</b>&nbsp;English</li>
						</ul>
					</div>
					<div style="display: none;" id="tab-review" class="tab-content">
						<div id="review">
							<div class="content">There are no reviews for this product.</div>
						</div>
						<h2 id="review-title">Write a review</h2>
						<label>Your Name:</label> <input name="name" class="entry_name"
							type="text"> <br> <br> <label>Your Review:</label>
						<textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
						<span style="font-size: 11px;"><span style="color: #FF0000;">Note:</span>
							HTML is not translated!</span><br> <br> <label
							class="entery_rating">Rating:</label> <span>Bad</span>&nbsp;
						<div class="tm-radio">
							<input class="tm-hide" name="rating" value="1" type="radio">
						</div>
						&nbsp;
						<div class="tm-radio">
							<input class="tm-hide" name="rating" value="2" type="radio">
						</div>
						&nbsp;
						<div class="tm-radio">
							<input class="tm-hide" name="rating" value="3" type="radio">
						</div>
						&nbsp;
						<div class="tm-radio">
							<input class="tm-hide" name="rating" value="4" type="radio">
						</div>
						&nbsp;
						<div class="tm-radio">
							<input class="tm-hide" name="rating" value="5" type="radio">
						</div>
						&nbsp;<span>Good</span><br> <br> <label class="entery_captcha">Enter
							the code in the box below:</label> <input name="captcha"
							class="captch_input" type="text"> <img
							src="<?php echo base_url();?>public/shop/index.jpg" alt=""
							id="captcha"><br> <br>
						<div class="buttons">
							<div class="right">
								<a id="button-review" class="button">Continue</a>
							</div>
						</div>
					</div>
					<div class="box">
						<div class="box-heading">Related Products (1)</div>
						<div id="products-related" class="related-products">
							<!-- Megnor Related Products Start -->
							<div class="box-content">
								<div style="width: 746px;" class="box-product productbox-grid"
									id="related-grid">
									<div style="width: 186px;" class="product-items first_item_tm">
										<div style="height: 332px;" class="product-block">
											<div class="product-block-inner">
												<div class="image">
													<a
														href="#/Cambridge-International-AS-and-A-level-physics-coursebook-in-bangladesh"><img
														src="<?php echo base_url();?>public/shop/Cambridge%252520International%252520AS%252520and%252520A%252520Lev_004.jpg"
														alt="Cambridge International AS and A Level Physics Coursebook"></a>
												</div>
												<div class="name">
													<a
														href="#/Cambridge-International-AS-and-A-level-physics-coursebook-in-bangladesh">Cambridge
														International AS and A Level Physics Coursebook</a>
												</div>
												<div class="price">TK.250</div>
												<a onclick="addToCart('148');" class="button">Add to Basket</a>
											</div>
										</div>
									</div>
								</div>
								<span class="related_default_width"
									style="display: none; visibility: hidden;"></span>
								<!-- Megnor Related Products Start -->
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "colorbox"
	});
});
//--></script>
				<script type="text/javascript"><!--
$('select[name="profile_id"], input[name="quantity"]').change(function(){
    $.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name="product_id"], input[name="quantity"], select[name="profile_id"]'),
		dataType: 'json',
        beforeSend: function() {
            $('#profile-description').html('');
        },
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
            
			if (json['success']) {
                $('#profile-description').html(json['success']);
			}	
		}
	});
});
    
$('#button-cart').bind('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
                
                if (json['error']['profile']) {
                    $('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
                }
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});
//--></script>
				<script type="text/javascript"
					src="<?php echo base_url();?>public/shop/ajaxupload.js"></script>
				<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			
$('#review').load('index.php?route=product/product/review&product_id=139');
$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=139',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> Please Wait!</div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script>
				<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script>
				<script type="text/javascript"
					src="<?php echo base_url();?>public/shop/jquery-ui-timepicker-addon.js"></script>
				<script type="text/javascript"><!--
$(document).ready(function() {
	if ($.browser.msie && $.browser.version == 6) {
		$('.date, .datetime, .time').bgIframe();
	}
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});
});
//--></script>
			</div>
			<!-- content inner div end -->
		</section>
		<!-- section code end -->