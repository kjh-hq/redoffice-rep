<script type="text/javascript" src="<?php echo base_url();?>public/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/js/jquery.selectboxes.pack.js"></script>
<section id='content-wrapper'>
<div class="content-inner">
<div class="content_home">
<div class="main-container full-width">
<div class="container-fluid">
<div class="breadcrumbs"><a href="<?php echo site_url(); ?>">Home</a><span class="separator">/</span><span>Delivery Address</span></div>	</div>
<header class="entry-header">
<div class="container-fluid">
<h1 class="entry-title">Delivery Address</h1>
</div>
</header>
<div class="row-container">
<div class="vc_column_container vc_col-sm-2">
</div>
<div class="vc_column_container vc_col-sm-8">
<?php echo validation_errors(); ?>		
<form class="form-horizontal" role="form" method="post" >
<div class="form-group">
<label for="name" class="col-sm-2 control-label">Full Name</label>
<div class="col-sm-10">
<?php echo form_input('name', set_value('name', $order->name),'id="nm" class="tb-order"'); ?>
</div>
</div>
<div class="form-group">
<label for="name" class="col-sm-2 control-label">Email</label>
<div class="col-sm-10">
<?php echo form_input('email', set_value('email',$this->visitor_m->email()),'id="email" class="tb-order"'); ?>
</div>
</div>
<div class="form-group">
<label for="name" class="col-sm-2 control-label">Phone</label>
<div class="col-sm-10">
<?php echo form_input('phone', set_value('phone', $order->phone),'id="phone" class="tb-order"'); ?>
</div>
</div>
<div class="form-group">
<label for="name" class="col-sm-2 control-label">Alternative Phone</label>
<div class="col-sm-10">
				
<?php echo form_input('phone2', set_value('phone2', $order->phone2),'id="phone2" class="tb-order"'); ?>
</div>
</div>
<div class="form-group">
<label for="name" class="col-sm-2 control-label">Country</label>
<div class="col-sm-10">
<?php echo form_dropdown('cnt', array('0'=>'Select Country','1'=>'Bangladesh','2'=>'India','4'=>'USA','5'=>'Canada','6'=>'Australia','7'=>'German','8'=>'Netherland'), $this->input->post('cnt') ? $this->input->post('cnt') : $order->cnt,'id="cnt" onchange="cntChangeHandler();"'); ?>
</div>
</div>
<div class="form-group"  id="divct">
<label for="name" class="col-sm-2 control-label">City/District</label>
<div class="col-sm-10">
<?php echo form_dropdown('ct', '', $this->input->post('ct') ? $this->input->post('ct') : $order->ct,'id="ct" onchange="ctChangeHandler();"'); ?>
</div>
</div>
<div class="form-group" id="divarea">
<label for="name" class="col-sm-2 control-label">Area/Thana</label>
<div class="col-sm-10">
<?php echo form_dropdown('area', '', $this->input->post('area') ? $this->input->post('area') : $order->area,'id="area"'); ?>
</div>
</div>
<div class="form-group">
<label for="name" class="col-sm-2 control-label">Delivery Address</label>
<div class="col-sm-10">
<?php echo form_textarea('addr',set_value('addr', $order->addr),'id="addr" class="tb-order"'); ?>
</div>
</div>
<div class="form-group">
<div class="col-sm-2"></div>
<div class="col-sm-10">
<a href="<?php echo site_url();?>shop/cart" class="btn btn-info">Back</a>
<?php echo form_submit('submit', 'submit', 'class="btn btn-primary"'); ?>
</div>
</div>
<?php echo form_close();?>
</div>
<script type="text/javascript">
$(document).ready(function(){
if($('#cnt').val() != 1) {
$('#divct').slideUp(400);
$('#divarea').slideUp(400);
}
});
function cntChangeHandler() {
if($("#cnt").val()!=null && $("#cnt").val()!=0){
$.getJSON("<?php echo site_url();?>data/cts?id="+$("#cnt").val(), function(data) {
$("#ct").removeOption(/./);	
$("#area").removeOption(/./);
$.each(data, function(key, val) {
$("#ct").addOption(val.id, val.nm);
});
$('#ct option:first-child').attr('selected', 'selected');
if(data.length > 0) {
$.getJSON("<?php echo site_url();?>data/areas?id="+data[0].id, function(data) {
$("#area").removeOption(/./);	
$.each(data, function(key, val) {
$("#area").addOption(val.id, val.nm);
});
});
}
});
if($('#cnt').val() == 1) {
$('#divct').slideDown(400);
$('#divarea').slideDown(400);
}
else {
$('#divct').slideUp(400);
$('#divarea').slideUp(400);
}
}
}
function ctChangeHandler() {
if($("#ct").val()!=null && $("#ct").val()!=0) {
$.getJSON("<?php echo site_url();?>data/areas?id="+$("#ct").val(), function(data) {
$("#area").removeOption(/./);	
$.each(data, function(key, val) {
$("#area").addOption(val.id, val.nm);
});
});
}
}
</script>
</div>
<!-- /content right -->
</div>
</div>
</div>
</div><!-- content inner div end -->
</section>
