<div class="container page-title">
    <div class="row">
        <div class="col m-10 m-centered">
            <h1>News</h1>
        </div>
    </div>
</div>
<div class="fullwidth border-gray-top section">
    <div class="container">
        <div class="row">
            <div class="col m-2">
                <div class="sidenav">
                                <h3 class="m-only">Categories</h3>
        <h3 class="s-only categorydropdown">Categories <i class="icon-chevron-down"></i></h3>
        <ul>
                <li><a href="http://www.pyronix.com/news-and-events/news?filter=Awards" class="">Awards</a></li>
                <li><a href="http://www.pyronix.com/news-and-events/news?filter=Enforcer Club" class="">Enforcer Club</a></li>
                <li><a href="http://www.pyronix.com/news-and-events/news?filter=News" class="">News</a></li>
                <li><a href="http://www.pyronix.com/news-and-events/news?filter=Offers" class="">Offers</a></li>
                <li><a href="http://www.pyronix.com/news-and-events/news?filter=People" class="">People</a></li>
                <li><a href="http://www.pyronix.com/news-and-events/news?filter=Press" class="">Press</a></li>
                <li><a href="http://www.pyronix.com/news-and-events/news?filter=Product" class="">Product</a></li>
                <li><a href="http://www.pyronix.com/news-and-events/news?filter=Shows and Training " class="">Shows and Training </a></li>
        </ul>
                </div>
            </div>
            <div class="col m-6 m-offset-1 areafix">
                
    <div>
                <div class="article-expanded row">
                    <div class="col m-12">
                        <h2><a href="http://www.pyronix.com/news-and-events/news/pyronix-back-at-ifsec-with-a-bang-34225">Pyronix back at IFSEC with a bang</a>
                        </h2>
                        <h3>
                            <span class="icon-clock"></span> 25 May 2016
                            <span>&nbsp;</span>
                            <span class="icon-tag"></span> News, People, Press, Product, Shows and Training 
                        </h3>
                    </div>
                        <div class="col m-5"><img src="/_assets/media/article/33_323.jpg"/></div>
                        <div class="col m-7">
                            <p>Pyronix 30-year anniversary and an extremely bright vision for the future ...</p>
                            <a href="http://www.pyronix.com/news-and-events/news/pyronix-back-at-ifsec-with-a-bang-34225" class="btn btn-red">Read more</a>
                        </div>
                </div>
                <div class="article-expanded row">
                    <div class="col m-12">
                        <h2><a href="http://www.pyronix.com/news-and-events/news/pyronix-announces-its-acquisition-by-hikvision-38692">Pyronix Announces Its Acquisition by Hikvision</a>
                        </h2>
                        <h3>
                            <span class="icon-clock"></span> 19 May 2016
                            <span>&nbsp;</span>
                            <span class="icon-tag"></span> News, People, Press
                        </h3>
                    </div>
                        <div class="col m-12">
                            <p>Pyronix, an award-winning security manufacturer focusing on intrusion alarm systems, has announced its acquisition by Hikvision ...</p>
                            <a href="http://www.pyronix.com/news-and-events/news/pyronix-announces-its-acquisition-by-hikvision-38692" class="btn btn-red">Read more</a>
                        </div>
                </div>
                <div class="article-expanded row">
                    <div class="col m-12">
                        <h2><a href="http://www.pyronix.com/news-and-events/news/thank-you-for-visiting-pyronix-at-intersec-2016-13506">Thank you for visiting Pyronix at Intersec 2016</a>
                        </h2>
                        <h3>
                            <span class="icon-clock"></span> 16 February 2016
                            <span>&nbsp;</span>
                            <span class="icon-tag"></span> News, People, Press, Shows and Training 
                        </h3>
                    </div>
                        <div class="col m-5"><img src="/_assets/media/article/24_304.jpg"/></div>
                        <div class="col m-7">
                            <p>The Intersec 2016 exhibition was a very good show for us and it was essential that we had a presence at the most important show for the region. ...</p>
                            <a href="http://www.pyronix.com/news-and-events/news/thank-you-for-visiting-pyronix-at-intersec-2016-13506" class="btn btn-red">Read more</a>
                        </div>
                </div>
                <div class="article-expanded row">
                    <div class="col m-12">
                        <h2><a href="http://www.pyronix.com/news-and-events/news/technical-bulletin-tb008-windows-xp-support-to-cease-in-six-months-66421">Technical Bulletin TB008: Windows XP support to cease in six months</a>
                        </h2>
                        <h3>
                            <span class="icon-clock"></span> 16 February 2016
                            <span>&nbsp;</span>
                            <span class="icon-tag"></span> News, Press, Product
                        </h3>
                    </div>
                        <div class="col m-5"><img src="/_assets/media/article/25_307.jpg"/></div>
                        <div class="col m-7">
                            <p>As Microsoft have withdrawn support on Windows XP, Pyronix are notifying customers with six months’ prior notice, that all support will cease on PC applications running on the Windows XP platform.  ...</p>
                            <a href="http://www.pyronix.com/news-and-events/news/technical-bulletin-tb008-windows-xp-support-to-cease-in-six-months-66421" class="btn btn-red">Read more</a>
                        </div>
                </div>
                <div class="article-expanded row">
                    <div class="col m-12">
                        <h2><a href="http://www.pyronix.com/news-and-events/news/upgrades-to-the-ur2-we-77740">Upgrades to the UR2-WE</a>
                        </h2>
                        <h3>
                            <span class="icon-clock"></span> 18 January 2016
                            <span>&nbsp;</span>
                            <span class="icon-tag"></span> News, Product
                        </h3>
                    </div>
                        <div class="col m-5"><img src="/_assets/media/article/23_263.jpg"/></div>
                        <div class="col m-7">
                            <p>Walk Test Mode and Active Input Indication added. ...</p>
                            <a href="http://www.pyronix.com/news-and-events/news/upgrades-to-the-ur2-we-77740" class="btn btn-red">Read more</a>
                        </div>
                </div>
    </div>
    <div class="paging">
        <div class="row">
            <div class="col m-3 text-left prev">
                <a href="" class="med-text-link"><span class="icon-chevron-left"></span> Previous</a>
            </div>
            <div class="col m-6">
                <div class="paging-pages">
                    <ul>
                            <li><a href="http://www.pyronix.com/news-and-events/news?page=1" class="btn btn-current">1</a></li>
                            <li><a href="http://www.pyronix.com/news-and-events/news?page=2" class="btn btn-red">2</a></li>
                            <li><a href="http://www.pyronix.com/news-and-events/news?page=3" class="btn btn-red">3</a></li>
                    </ul>
                </div>
            </div>
            <div class="col m-3 text-right next">
                <a href="http://www.pyronix.com/news-and-events/news?page=2" class="med-text-link">Next <span class="icon-chevron-right"></span></a>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
</div>
