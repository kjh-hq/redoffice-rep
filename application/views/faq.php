<div class="container page-title">
    <div class="row">
        <div class="col m-10 m-centered">
            <h1>FAQs</h1>
        </div>
    </div>
</div>
<div class="fullwidth border-gray-top section">
    <div class="container">
        <div class="row">
            <div class="col m-2">
                <div class="sidenav">
                                <h3 class="m-only">Help & Support<br>Home Users</h3>
        <h3 class="s-only categorydropdown">Help & Support<br>Home Users <i class="icon-chevron-down"></i></h3>
        <ul>
                <li><a href="http://www.pyronix.com/help-and-support/home-users/user-guides" class="">User Guides</a></li>
                <li><a href="http://www.pyronix.com/help-and-support/home-users/faqs" class="current">FAQs</a></li>
                <li><a href="http://www.pyronix.com/help-and-support/home-users/product-information" class="">Product Information</a></li>
                <li><a href="http://www.pyronix.com/help-and-support/home-users/videos" class="">Videos</a></li>
        </ul>
                </div>
            </div>
            <div class="col m-6 m-offset-1 areafix">
                
    <div class="row content-form-inline simple-search-form">
        <input type="hidden" name="baseurl" id="baseurl" value="http://www.pyronix.com/help-and-support/home-users/faqs" />
        <div class="col m-10">
            <input type="text" class="content-form-input" data-query="searchcriteria" placeholder="Enter your search criteria ..." />
        </div>
        <div class="col m-2">
            <a href="#" class="btn btn-red btn-search">Search</a>
        </div>
    </div>
    <div class="faqs-list">
            <h3>End User - System General <a name="cat11"></a></h3>
                <div class="item">
                    <strong><a href="#">Q : How do I know when the batteries in my devices need replacing?</a></strong>
                    <div class="hidden"><p>Under normal circumstances batteries in all devices should last up to two years. However, the battery in the Mk1 Shock Sensor will last for up to one year. Excessive use or poor signal strength may adversely affect battery life. When a battery reaches the end of its life the transmitter will send a &lsquo;Low Battery&rsquo; message or a &lsquo;Supervision Fault&rsquo; will be indicated at the panel, warning you that the battery requires replacing. We recommend that batteries are replaced routinely after two years (one year in the case of the Mk1 Shock Sensor).</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : How can I tell if my children have arrived home safely from school?</a></strong>
                    <div class="hidden"><p>Your installation company may be able to program a &lsquo;Special Unset&rsquo; feature, which will send you a text message when certain user codes are used to unset the system.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : Is it necessary to have my security system maintained?</a></strong>
                    <div class="hidden"><p>We would always recommend that you have your system maintained by a reputable and professional service company, particularly with respect to wireless systems that may require batteries to be checked and replaced.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : How reliable are wireless security systems?</a></strong>
                    <div class="hidden"><p>Wireless systems have the advantage of being extremely quick to install with minimal disruption to a property. Reliability of wireless devices has improved significantly over the last few years and with regular maintenance a wireless system can perform just as well as a traditionally wired system.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : How do I change my user code?</a></strong>
                    <div class="hidden"><p>Press the &lsquo;D&rsquo; key on the keypad and then enter your user code. Press the &lsquo;No&rsquo; key until &lsquo;CHANGE USER CODES&rsquo; is displayed. Press &lsquo;Yes&rsquo; and then follow the screen prompts from there.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : How can I unset my system without a tag/fob?</a></strong>
                    <div class="hidden"><p>On some systems (usually police calling systems) you may have to use a tag or fob to unset the system instead of a code. This is to comply with the requirements of police policy and EN standards relating to the use of intruder alarm systems - if you do not have a tag or fob available, you will be unable to unset the system using a code until after the entry time has expired and an alarm has been generated.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : How can I change the time and date on my system?</a></strong>
                    <div class="hidden"><p>Press the &lsquo;D&rsquo; key on the keypad and then enter your user code. Press the &lsquo;No&rsquo; key until &lsquo;SET DATE &amp; TIME?&rsquo; is displayed. Press &lsquo;Yes&rsquo; then follow the screen prompts from there.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : Can I get a discount on my home insurance if I have a security system fitted?</a></strong>
                    <div class="hidden"><p>With some properties your insurance company may insist that you have a security system fitted before they will cover you. In the majority of cases however, a discount may be available on your annual premium. In such cases you would typically be expected to have the system professionally installed by an installer that is a member of one of the industry inspectorate bodies and have the system regularly maintained. Contact your insurance company for specific details.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : Can I disable/enable the display backlight on my keypad?</a></strong>
                    <div class="hidden"><p>The display backlight on a keypad is normally programmed to turn off after approximately one minute. The display can be programmed to permanently lit, however this is an engineer function, so please ask your installer if this is required.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
            <h3>End User - Product Specification <a name="cat12"></a></h3>
                <div class="item">
                    <strong><a href="#">Q : What is the advantage of having wireless smoke detectors connected to my security systrem?</a></strong>
                    <div class="hidden"><p>Stand-alone smoke detectors warn you of a potential fire at your property by sounding when they detect smoke. Smoke sensors connected to a security system can be configured to warn you by sending a text message or signal to a monitoring station. Should a potential fire occur when you are away from your property, you will then be notified and take the necessary action.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
            <h3>End User - Product Faults <a name="cat13"></a></h3>
                <div class="item">
                    <strong><a href="#">Q : What can cause false alarms with smoke detectors?</a></strong>
                    <div class="hidden"><p>Harsh or dusty environments can cause a false alarm with smoke detectors. Additionally, at certain times of year (usually around harvest time) tiny insect can penetrate the smoke chamber, which may also cause false activations. To minimise this, ensure that detectors are kept clean by vacuuming them at least once per year.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : What can cause false alarms with movement detectors?</a></strong>
                    <div class="hidden"><p>Passive Infrared (PIR) detectors can be affected by any source of heat, such as: convection currents from radiators or open fires. Swaying curtains or decorations can also cause problems; therefore ensure that all windows and doors in the area are closed and that there are no draughts to minimise the risk of false alarms.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
            <h3>End User - Maintenance and Support <a name="cat14"></a></h3>
                <div class="item">
                    <strong><a href="#">Q : What happens in the event of a power cut?</a></strong>
                    <div class="hidden"><p>The internal battery located within the end station will take over and the battery will recharge once power is restored. The battery should last for around 12 hours (depending on the grade of system) in the event of a power failure.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : The range of my device seems less than declared?</a></strong>
                    <div class="hidden"><p>Ensure that the device is mounted at the correct height (refer to instructions). With Passive Infrared (PIR) detectors, if a pet mask is fitted, ensure that the sensitivity jumper is set to &lsquo;High&rsquo;. With Dual-Technology detectors ensure that the range is correctly set using the range potentiometer.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : I have pets - will the system still work?</a></strong>
                    <div class="hidden"><p>Pets are likely to activate standard movement detectors, however, pet tolerant detectors can be fitted that will ignore animals below approximately 24kg in weight. Please note, siting and configuration of pet tolerant devices is critical to obtain the best results.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : How long does the outside bell ring for? do I really need one?</a></strong>
                    <div class="hidden"><p>An external sounder must switch off after ringing for a maximum of 15 minutes to comply with UK noise pollution laws. On some systems it may not be a requirement to fit an external sounder, however check with your insurance company first.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : How long do batteries last in Enforcer peripheral devices?</a></strong>
                    <div class="hidden"><p>Under normal circumstances batteries in all devices should last up to two years. However, the battery in the Mk1 Shock Sensor will last for up to one year. Excessive use or poor signal strength may adversely affect battery life.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : How do I silence alarms?</a></strong>
                    <div class="hidden"><p>Enter a valid user code or present a valid tag at the keypad and this should be acknowledged and silence any alarm present. Ensure you make a note of the display to determine the cause of the alarm.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : Can I be notified when my system is set or unset?</a></strong>
                    <div class="hidden"><p>Most panels are capable of sending text messages and if programmed correctly they can send a message when the system is set and/or unset. This can be done either by a standard PSTN telephone line or a GSM modem with compatible panels. Please note however that call charges will apply depending on the method used to send messages.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : Can I set my alarm system at night?</a></strong>
                    <div class="hidden"><p>All Euro and Enforcer panels are capable of being set using various partition configurations. In its simplest form this will allow a daytime (full) set and a night-time (part) set mode.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
            <h3>End User - General Questions <a name="cat15"></a></h3>
                <div class="item">
                    <strong><a href="#">Q : What products do Pyronix offer?</a></strong>
                    <div class="hidden"><p>Pyronix offers a coveted and award-winning range of security solutions, from dual tech motion detectors and carbon monoxide detectors to sounders, control panels and HD IP cameras.&nbsp;</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
                <div class="item">
                    <strong><a href="#">Q : How do I find a Installer of Pyroinx&#39; products?</a></strong>
                    <div class="hidden"><p>Visit the NSI or SSAIB websites to find local professional security installation companies and then contact them separately to enquire.</p>
<p><a href="#" class="close">Close</a></p></div>
                </div>
    </div>
            </div>
                <div class="col m-2 m-offset-1">
                    <div class="sticky-anchor"></div>
                    <div class="sidenav m-only">
            <h3 class="m-only">Jump to:</h3>
        <h3 class="s-only categorydropdown">Jump to: <i class="icon-chevron-down"></i></h3>
        <ul>
                <li><a href="#cat15" class="">End User - General Questions</a></li>
                <li><a href="#cat14" class="">End User - Maintenance and Support</a></li>
                <li><a href="#cat13" class="">End User - Product Faults</a></li>
                <li><a href="#cat12" class="">End User - Product Specification</a></li>
                <li><a href="#cat11" class="">End User - System General</a></li>
        </ul>
                    </div>
                </div>
    </div>
</div>
</div>
