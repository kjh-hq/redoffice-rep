<section id="content-wrapper">
<div class="content-inner">
<div id="notification"></div><aside role="complementary" id="column-left">
  </aside>
 
<div id="content">  <div class="breadcrumb">
        <a href="">Home</a>
         � <a href="">Account</a>
         � <a href="">Register</a>
      </div>
  <h1>Register Account</h1>
  <p>If you already have an account with us, please login at the <a href="">login page</a>.</p>
 <?php echo validation_errors(); ?>
<?php echo form_open(); ?>
    <h2>Your Personal Details</h2>
    <div class="content">
      <table class="form">
        <tbody>
        <tr>
          <td><span class="required">*</span> Name:</td>
          <td><?php echo form_input('name', set_value('name', $visitor->name)); ?>
            </td>
        </tr>
        <tr>
          <td><span class="required">*</span> E-Mail:</td>
          <td><?php echo form_input('email', set_value('email', $visitor->email)); ?>
            </td>
        </tr>
        <tr>
          <td><span class="required">*</span> Mobile:</td>
          <td><?php echo form_input('phone', set_value('phone', $visitor->phone)); ?>
            </td>
        </tr>
        
      </tbody></table>
    </div>
    <h2></h2>
    
    <h2>Your Password</h2>
    <div class="content">
      <table class="form">
        <tbody><tr>
          <td><span class="required">*</span> Password:</td>
          <td><?php echo form_password('password'); ?>
            </td>
        </tr>
        <tr>
          <td><span class="required">*</span> Password Confirm:</td>
          <td><?php echo form_password('password_confirm'); ?>
            </td>
        </tr>
	
          <?php echo form_input('ip', set_value('id', get_client_ip()),'style="display:none;"'); ?>
           
      </tbody></table>
    </div>
    <div class="buttons">
      <div class="right">
        <?php echo form_submit('submit', 'Continue', 'class="button"'); ?>
      </div>
    </div>
    
        
     <?php echo form_close();?>
  </div>
<script type="text/javascript">&lt;!--
$('input[name=\'customer_group_id\']:checked').live('change', function() {
	var customer_group = [];
	
	customer_group[1] = [];
	customer_group[1]['company_id_display'] = '1';
	customer_group[1]['company_id_required'] = '0';
	customer_group[1]['tax_id_display'] = '0';
	customer_group[1]['tax_id_required'] = '0';
	
	if (customer_group[this.value]) {
		if (customer_group[this.value]['company_id_display'] == '1') {
			$('#company-id-display').show();
		} else {
			$('#company-id-display').hide();
		}
		
		if (customer_group[this.value]['company_id_required'] == '1') {
			$('#company-id-required').show();
		} else {
			$('#company-id-required').hide();
		}
		
		if (customer_group[this.value]['tax_id_display'] == '1') {
			$('#tax-id-display').show();
		} else {
			$('#tax-id-display').hide();
		}
		
		if (customer_group[this.value]['tax_id_required'] == '1') {
			$('#tax-id-required').show();
		} else {
			$('#tax-id-required').hide();
		}	
	}
});
$('input[name=\'customer_group_id\']:checked').trigger('change');
//--&gt;</script> 
<script type="text/javascript">&lt;!--
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/country&amp;country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('&lt;span class="wait"&gt;&amp;nbsp;&lt;img src="catalog/view/theme/default/image/loading.gif" alt="" /&gt;&lt;/span&gt;');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
			
			html = '&lt;option value=""&gt; --- Please Select --- &lt;/option&gt;';
			
			if (json['zone'] != '') {
				for (i = 0; i &lt; json['zone'].length; i++) {
        			html += '&lt;option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '&gt;' + json['zone'][i]['name'] + '&lt;/option&gt;';
				}
			} else {
				html += '&lt;option value="0" selected="selected"&gt; --- None --- &lt;/option&gt;';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});
$('select[name=\'country_id\']').trigger('change');
//--&gt;</script> 
<script type="text/javascript">&lt;!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		width: 640,
		height: 480
	});
});
//--&gt;</script> 
</div><!-- content inner div end -->
</section>