<div class="main-container full-width">
<div class="container-fluid">
<div class="breadcrumbs">
<a href="<?php echo site_url();?>">Home</a>
<span class="separator">/</span>
<span> Login</span>
</div>  
</div>
<header class="entry-header">
<div class="container-fluid">
<h1 class="entry-title">Authentication Process </h1>
</div>
</header>
<div class="row-container">
<div class="wpb_column vc_column_container vc_col-sm-12">
<?php echo form_open(); ?>
<div class="vc_col-sm-6">
   <table class="shop_table" cellspacing="0">
   <thead>
   <tr>
      <th><h2>New Customer</h2></th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>
        <p><b>Register Account</b></p>
    </td>
    </tr>
    <tr>
    <td>
        <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
        </td>
    </tr>
</tbody>
<tfoot>
 <tr>
    <td>
        <a class="button" href="<?php echo site_url();?>client/register">Continue</a>
        </td>
    </tr>
</tfoot>
    </table>
</div>
<div class="vc_col-sm-6">
 <table class="shop_table" cellspacing="0">
   <thead>
   <tr>
   <th colspan="2">
      <h2>Returning Customer</h2>
  </th>
  </tr>
  </thead>
  <tbody>
  <?php echo $this->session->userdata('error');?>
              <?php echo validation_errors(); ?>
     <?php echo form_open(); ?>
    <tr>
    <td>
          <b>E-Mail Address:</b></td>
          <td>
          <?php echo form_input('email','','class="input-text"'); ?>
          </td>
    </tr>
    <tr>
    <td>
          <b>Password:</b></td><td>
         <?php echo form_password('password','','class="input-text"'); ?>
         </td>
    </tr>
    <tr>
    <td colspan="2">
          <a href="<?php echo site_url();?>shop/forgot_password">Forgotten Password</a><br>
         </td>
    </tr>
</tbody>
<tfoot>
<tr>
    <td colspan="2">
          <input type="submit" class="button" value="Login">
     <?php echo form_close();?>
     </td>
     </tr>
     </tfoot>
   </table>
</div>
</div>
</div>
</div>
