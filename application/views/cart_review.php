<div class="main-container full-width">
<div class="container-fluid">
<div class="breadcrumbs">
<a href="<?php echo site_url();?>">Home</a>
<span class="separator">/</span>
<span> Shopping Cart</span>
</div>	</div>
<header class="entry-header">
<div class="container-fluid">
<h1 class="entry-title">Shopping Cart</h1>
</div>
</header>

<div class="page-content">
<div class="container-fluid">

<article id="post-20" class="post-20 page type-page status-publish hentry">

<div class="entry-content">
<div class="vc_row wpb_row vc_row-fluid">
<div class="row-container">
<div class="wpb_column vc_column_container vc_col-sm-12">
<div class="wpb_wrapper">
<div class="wpb_text_column wpb_content_element ">
<div class="wpb_wrapper">
<div class="woocommerce">

<?php echo form_open('cart/update_cart'); ?>


<table class="shop_table cart" cellspacing="0">
<thead>
<tr>
<th class="product-thumbnail">
</th>
<th class="product-name">Product</th>
<th class="product-price">Price</th>
<th class="product-quantity">Quantity</th>
<th class="product-subtotal">Total</th>
<th class="product-remove">
</th>
</tr>
</thead>
<tbody>
<?php foreach($this->cart->contents() as $items){ ?>

<?php echo form_hidden('rowid[]', $items['rowid']); ?>

<tr class="cart_item">

<td class="product-thumbnail">
<a href="<?php echo site_url();?>ps/product/<?php echo $items['id'];?>">
<img width="200" height="200" src="<?php echo base_url();?>public/img_upload/small/<?php echo $items['img'];?>" class="attachment-shop_thumbnail wp-post-image" alt="6" />
</a>
</td>
<td class="product-name">
<a href="<?php echo site_url();?>ps/product/<?php echo $items['id'];?>"><?php echo $items['name']; ?></a></td>

<td class="product-price">
<span class="amount"><?php echo $this->cart->format_number($items['price']); ?></span></td>

<td class="product-quantity">
<div class="quantity">
<input type="number" step="1" min="0"  name="qty[]" value="<?php echo $items['qty'] ?>" title="Qty" class="input-text qty text" size="4" />
</div>
</td>

<td class="product-subtotal">
<span class="amount">Tk.<?php echo $this->cart->format_number($items['subtotal']); ?></span></td>

<td class="product-remove">
<a href="<?php echo site_url ();?>cart/update_cart_single/?id=<?php echo $items['rowid']?>&curl=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>"  class="remove" title="Remove this item">
<i class="fa fa-times-circle">
</i>
</a></td>
</tr>

<?php } ?>

</tbody>
</table>

<div class="row">
<div class="col-xs-12">
<div class="buttons-cart">
<input type="submit" class="button" name="update_cart" value="Update Cart" />
<a class="continue button" href="<?php echo site_url();?>shop">Continue Shopping</a>
</div>
</div>
<div class="col-xs-12 col-md-4">
</div>
<div class="col-xs-12 col-md-4">
</div>
<div class="col-xs-12 col-md-4">
<div class="cart_totals ">


<h2>Cart Totals</h2>

<table cellspacing="0">

<tr class="cart-subtotal">
<th>Subtotal</th>
<td>
<span class="amount">Tk.<?php echo $this->cart->format_number($this->cart->total()); ?></span>
</td>
</tr>




<tr class="shipping">
<th>Shipping</th>
<td>
Flat Rate: <span class="amount">Tk.40.0</span>				
</td>
</tr>
<tr class="order-total">
<th>Total</th>
<td>
<strong>
<span class="amount">TK.<?php $total=intval($this->cart->total()+40);echo $this->cart->format_number($total); ?></span>
</strong> </td>
</tr>


</table>


<div class="wc-proceed-to-checkout">

<a href="<?php echo site_url();?>order/shipping" class="checkout-button button alt wc-forward">Proceed to Checkout</a>
</div>


</div>
</div>
</div>
<div class="loading">
</div>
</form>

</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</article>					</div>
</div>
</div>