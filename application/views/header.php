<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/style/css/style-main.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/style/css/jquery.fancybox.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/style/css/all.css"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans" media="all" />
<script type="text/javascript" src="<?php echo base_url();?>public/js/jquery-1.11.1.min.js"></script>
<title>REDOFFICE</title>
</head>
<body class="home home-01">
<!--begin header-->
<div id="box-header"> 
  <div class="header-container">
    <div class="container overflow-show">

        <header class="clearfix">

           <a href="#" id="menu-button">Menu</a>

            <nav class="top-menu">
                <ul>
                    
                        <li><a href="#" id="login"><i class="icon-login"></i> Login</a></li>
                        <li><a href="#"><i class="icon-add-user"></i> Register</a></li>

                    <li><a href="#"><i class="icon-old-phone"></i> Contact us</a></li>
                    <li><a href="<?php echo site_url();?>inquiry"><i class="icon-old-phone"></i> Inquiry</a></li>
                    <li><a href="#" id="sitesearch"><i class="icon-magnifying-glass"></i> <span>Search</span></a><input type="text" class="searchinput" /></li>
                </ul>
            </nav>

       <a href="<?php echo site_url();?>" class="logo">
<?php if(!empty($logo)){?>
<img style="height: 75px;max-width: 250px;" src="<?php echo base_url();?>public/img_upload/<?php echo $logo?>">
<?php }else{?>
  <h2 style="color:red">REDOFFICE</h2>

<?php } ?>
<!-- <img style="height: 75px;max-width: 250px;" title="REDOFFICE" src="<?php echo base_url();?>public/img_upload/"> -->

</a>

           <nav class="main-menu">

                <ul>
 <li><a href="<?php echo site_url();?>">Home</a></li>
                  <?php 
                 
foreach ($menu_category as $key) {
    ?>
                  <li  style="position: static;" ><a  class="fullmenu-link" href="<?php echo $key['_link']?>"><?php echo ucfirst($key['title']);?> <i class="icon-chevron-down"></i></a>
                  <div class="fullmenu">

                  <?php 
        foreach ($key['child'] as $k=>$value) {?>
        <div class="menu-col">
                  <ul>
                  <li><strong><a href="/products-and-solutions"><?php echo ucwords(str_replace('_', ' ', $k))?></a></strong></li>
        <?php 
        foreach ($value as $k2=>$value2) {
            ?><li><a href="<?php echo $value2['_link']?>"><?php echo ucfirst($value2['title']);?></a></li><?php } ?>
                  </ul>
                  </div>
                  <?php } ?>
</div>
</li>
                  
    <?php } ?>
    
      </ul>
      </nav>
       </header>
</div>
<!--end header-->