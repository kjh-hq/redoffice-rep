<script type="text/javascript" src="<?php echo base_url();?>public/shop/jquery-1.5.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/shop/jquery.selectboxes.pack.js"></script>
<div class="main-container full-width">
<div class="container-fluid">
<div class="breadcrumbs"><a href="<?php echo site_url(); ?>">Home</a><span class="separator">/</span><span>Place Order</span></div>	</div>
<header class="entry-header">
<div class="container-fluid">
<h1 class="entry-title">Place Order</h1>
</div>
</header>
<div class="row-container">
<div class="vc_column_container vc_col-sm-2">
</div>
<div class="vc_column_container vc_col-sm-8">
		<?php echo validation_errors(); ?>
		<?php echo form_open(); ?>
		<div class="cont-order-ship">
			<div class="gen-div">
				<div class="order-row-a">&nbsp;</div>
				<div class="order-row-b">
					<div class="gen-div fnt-9">
						Payment System:
					</div>
					<div class="gen-div-2">
						
							<div class="gen-div" style="height: 18px;">
								<input type="radio" style="opacity: 1;" name="ptype" value="0" checked="checked">Cash on Delivery (COD)<br>
							</div>
						
						
						<div class="gen-div-2" style="height: 18px;">
							<input type="radio" style="opacity: 1;" name="ptype" value="1" >Mobile Payment
							<div id="mtype" class="display-none">
								
								<?php echo form_dropdown('mtype', array('0'=>'bKash','1'=>'DBBL Mb'), $this->input->post('mtype') ? $this->input->post('mtype') : $order->mtype); ?>
								&nbsp;
								<a name="dialog1" href="#dialog1" id="amodal" class="bkash-help"></a>
								<a name="dialog2" href="#dialog2" id="amodal" class="dbblmb-help" style="display:none;"></a>
							</div>
						</div>
						
						
					</div>
					<script type="text/javascript">
						$(document).ready(function(){
							$('input[name=ptype]').click(function(){
								if($(this).val() == 1) { // mobile payment
									$('select[id=ctype]').css('display', 'none');
									$('div[id=itype]').css('display', 'none');
									$('div[id=mtype]').css('display', 'inline');
								}
								else if($(this).val() == 2) { // card payment
									$('select[id=ctype]').css('display', 'inline');
									$('div[id=itype]').css('display', 'none');
									$('div[id=mtype]').css('display', 'none');
								}
								else if($(this).val() == 3) { // internet payment
									$('select[id=ctype]').css('display', 'none');
									$('div[id=itype]').css('display', 'inline');
									$('div[id=mtype]').css('display', 'none');
								}
								else { // cod
									$('select[id=ctype]').css('display', 'none');
									$('div[id=mtype]').css('display', 'none');
								}
							});
							
							$('select[name=mtype]').change(function(){
								var mtypev = $(this).val();
								if(mtypev == 1) {
									$('.bkash-help').css('display', 'block');
									$('.dbblmb-help').css('display', 'none');
								}
								else {
									$('.bkash-help').css('display', 'none');
									$('.dbblmb-help').css('display', 'block');
								}
							});
							
						});
					</script>
				</div>
			
				<div class="order-row-a">&nbsp;</div>
				<div class="order-row-b margin-top-10 fnt-9">
					Amount payable: <span class="fnt-16">Tk.<?php if($this->cart->format_number($this->cart->total())){echo $this->cart->format_number($this->cart->total()+40);}else{echo '0';}; ?></span>
				</div>
			</div>
			
				
				
				<div class="gen-div-3">
					<div class="order-row-a">&nbsp;</div>
					<div class="order-row-b">
						<input id="isterms1" style="opacity: 1;" name="isterms1" type="checkbox" value="true"/><input type="hidden" name="_isterms" value="on"/>
						&nbsp;
						
						
						I agree to the <a class="lnk-1" href="/terms-of-use">Terms of Use</a> and <a class="lnk-1" href="/privacy-policy">Privacy Policy</a> 
					</div>
				</div>
				<div class="gen-div-3">
					<div class="order-row-a">&nbsp;</div>
					<div class="order-row-b fnt-13">
						(
						Some of your ordered books might be unavailable due to out of print situation. We apologize for the inconvenience. We hope that you will consider that situation with a positive attitude.
						)
					</div>
				</div>
				<div class="gen-div-3">
					<div class="order-row-a">&nbsp;</div>
					<div class="order-row-b">
						<div class="gen-div-3">
							<a href="<?php echo site_url();?>order/shipping" class="btn-back"></a> 
							<input type="submit" value="submit" class="btn btn-success">
						</div>
					</div>
				</div>
				</div>
		<?php echo form_close();?>
	
                <script type="text/javascript">
                    $('a[id=amodal]').click(function(e) {
                        e.preventDefault();
                        var id = $(this).attr('href');
                        var maskHeight = $(document).height();
                        var maskWidth = $(window).width();
                        $('#mask').css({'width':maskWidth,'height':maskHeight});
                        $('#mask').fadeIn(1000);
                        $('#mask').fadeTo("medium",0.5);
                        var winH = $(window).height();
                        var winW = $(window).width();
                        $(id).css('top',  '100px');
                        $(id).css('left', (winW/2-$(id).width()/2));
                        //$(id).fadeIn(2000);
                        $(id).fadeIn(1000);
	                    
                        if(e.target.id=='login')
                            $('#tbEmailAddress').focus();
                        else
                            $('#tbFullName').focus();
                    });
                    //if close button is clicked
                    $('.window2 .close').click(function (e) {
                        e.preventDefault();
                        $('#mask').hide();
                        $('.window2').hide();
                    });
                    //if mask is clicked
                    $('#mask').click(function () {
                        $(this).hide();
                        $('.window2').hide();
                    });
                </script>
                <!-- /modal popup operations -->
	</div>
	<!-- content right -->
	
	<!-- /content right -->
</div>
</div>
</div>
</div>
</div><!-- content inner div end -->
</section>
