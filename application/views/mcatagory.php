<div class="container page-title">
        <div class="row">
            <div class="col m-10 m-centered">
                <!-- <h1><?php echo strtoupper($category->title)?></h1> -->
               <h1>All Categories</h1> 
            </div>
        </div>
    </div>
<?php foreach ($categories as $key => $value) { 
if(strlen($value->image)){
    ?>
            <div class="<?php echo $value->fullwidthclass?>">
                <div class="container">
                    <div class="row table">
                        <div class="col m-6 table-cell-valignmid">
                            <a href="<?php echo $value->_link;?>"><img src="<?php echo base_url();?>public/img_upload/<?php echo $value->image;?>" alt="" /></a>
                        </div>
                        <div class="col m-6 table-cell-valignmid">
                            <div class="row">
                                <div class="col m-10 m-centered">
                                    <div class="inner">
                                        <h2><?php echo $value->title?></h2>
                                        <h3><?php echo strip_tags($value->description)?></h3>
                                            <a href="<?php echo $value->_link;?>" class="btn btn-red ">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php } }?>
            
