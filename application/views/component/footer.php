<div class="position-06">
    <div class="fullwidth fullwidth-light fullwidth-centered" id="loginform" style="margin-top: 70px;">
        <div class="container">
            <div class="row">
                <div class="col m-12">
                    <h2>Log in</h2>
                    <div class="login-form">
                        <h3>View your account information &amp; access Installation manuals</h3>
                        <div class="login-error-message"></div>
                        <div class="inlineform-form">
                            <div class="logininner">
                                <input type="text" id="login-username" placeholder="Email" class="input input-dual" />
                                <input type="password" id="login-password" placeholder="Password" class="input input-dual" />
                                <input type="button" value="Login" class="btn btn-red login-button" /> </div>
                        </div>
                        <br/><a href="/register">Register</a> | <a href="#" class="forgotten-password">Forgotten password</a> </div>
                    <div class="forgot-form hidden">
                        <div class="forgot-message"></div>
                        <div class="inlineform-form">
                            <div class="logininner">
                                <input type="text" id="forgot-username" placeholder="Email" class="input input-single" />
                                <input type="button" value="Send Reset" class="btn btn-red forgot-button" /> </div>
                        </div>
                    </div>
                    <div class="login-thanks hidden">
                        <h3>You have successfully logged in</h3> <a href="#" class="btn btn-red">View My Account</a> </div>
                    <div class="forgot-confirmation hidden">
                        <h3>Your password has been reset, please check your email</h3>
                        <input type="button" value="Login" class="btn btn-red return-login-button" /> </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
 $('.login-button').click(function (event) {

        event.preventDefault();

        var username = $("#login-username").val();
        var password = $("#login-password").val();

        $.ajax({
            url: '/Callback/CheckLogin',
            type: 'POST',
            data: {
                username: username,
                password: password
            },
            success: function(data) {

                if (data != "Invalid Username or Password.") {

                    //$(".login-form").hide();
                    //$(".login-thanks").show();
                    //$(".login-thanks .btn").attr("href", data);
                    
                    document.location = data;

                } else {

                    $(".login-error-message").text(data);

                }

            },
            error: function (data) { }
        });

    });


    // form validation //

    $('.form-validation input[type=submit]').click(function (event) {

        var passed = true;

        var containingForm = $(this).closest("form");

        containingForm.find(':input').each(function () {

            $(this).removeClass("has-error");

            if ($(this).data("required") == "1" && $(this).val().trim() == "")
            {
                $(this).addClass("has-error");
                passed = false;
            }

        });

        containingForm.find('select').each(function () {

            $(this).removeClass("has-error");

            if ($(this).data("required") == "1" && $(this).val().trim() == "") {
                $(this).addClass("has-error");
                passed = false;
            }

        });

        return passed;

    });


    $("#login, .login").click(function (event) {

        event.preventDefault();

        $('html, body').animate({

            scrollTop: $("#loginform").offset().top
            
        }, 1000);
        
        $("#login-username").focus();

    });
</script>
</div>
<!--end content-->
<!--begin footer-->
<div id="box-footer">
    <div class="box-footer-top">
        <div class="container">
            <div class="row">
                <div class="row">
                    <div class="box-connect col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="block-title"><span>Connect with</span>
                        </div> <a href="<?php echo $snippets['facebook'];?>" class="face-book">face book</a> <a href="<?php echo $snippets['twitter'];?>" class="tweeter">tweeter</a> <a href="<?php echo $snippets['linkin'];?>" class="no-name">no-name</a> <a href="<?php echo $snippets['pinit'];?>" class="pinseter">pinseter</a> <a href="<?php echo $snippets['googleplus'];?>" class="google">google+</a> <a href="<?php echo $snippets['youtube'];?>" class="init">init</a> </div>
                    <!--                    <div class="box-tweeter col-lg-6 col-md-6 col-sm-6 col-xs-6">                        <div class="block-title"><span>tweeter</span></div>	                        <div class="content">                            Leo aliquet, dictum orci at, varius ligula. Duis aliquet pellentesque tincidunt. Vestibulum finibus ceo aliquet, dictum orci at, varius ligula. Duis aliquet                         </div>                    </div>	--></div>
            </div>
        </div>
    </div>
    <div class="box-footer-middle">
        <div class="container">
            <div class="row">
                <div class="row">
                    <div class="block block-info col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="block-title"><span>Information</span>
                        </div>
                        <div class="block-content">
                            <ul>
                                <li><a href="<?php echo site_url();?>">Home</a>
                                </li>
                                <li><a href="<?php echo site_url();?>contact">Contact Us</a>
                                </li>
                                <li><a href="<?php echo site_url();?>search">Advanced Search</a>
                                </li>
                                <li><a href="<?php echo site_url();?>login">Login</a>
                                </li>
                                <li><a href="#">Register</a>
                                </li>
                                <li><a href="<?php echo site_url();?>inquiry">Inquiry</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="block block-customer col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="block-title"><span>Brands</span>
                        </div>
                        <div class="block-content">
                            <ul>
                            <?php
                                foreach ($menu_category as $key) {
                                    ?>
                                <li><a href="<?php echo $key['_link'] ?>"><?php echo ucwords($key['title']); ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="block block-cu col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="block-title"><span>Contact us</span>
                        </div>
                        <div class="block-content">
                            <ul>
                                <!--<li class="item1"><span>Our business address is 1063 <br />Freelon Street San Francisco</span></li>-->
                                <li class="item2"><span><?php echo $snippets['support_phone'];?></span>
                                </li>
                                <li class="item3">
                                    <a href="mailto:<?php echo $snippets['support_email'];?>">
                                        <?php echo $snippets[ 'support_email'];?>
                                    </a>
                                </li>
                            </ul>
                            <div class="phone-nb">
                                <a href="tel:<?php echo $snippets['support_phone'];?>">
                                    <?php echo $snippets[ 'support_phone'];?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <div class="box-footer-bottom">        <div class="container">            <div class="row">                <div class="box-left">                    <ul>                        <li class="home"><a>REDOFFICE</a></li>                        <li><a href="#">About Us   </a><span>|</span></li>                        <li><a href="#">Contact Us</a></li>                    </ul>                    <h6>All Rights Reserved. Designed by <a title="" href=""></a></h6>                </div>            </div>        </div>    </div>--><a id="gototop">Top</a>
    <!--js-->
    <script type="text/javascript" src="<?php echo base_url(); ?>public/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/galary-image/js/jquery.blueimp-gallery.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/galary-image/js/bootstrap-image-gallery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/slideshow/jquery.themepunch.revolution.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/slideshow/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/theme.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>public/js/ui.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/custom.js"></script>
</div>
<!--end footer-->
</body>

</html>