<!DOCTYPE html>
<html xml:lang="en" lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/style/css/style-main.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/style/css/jquery.fancybox.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/style/css/all.css"/>
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans" media="all" />
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery-1.11.1.min.js"></script>
        <title>REDOFFICE</title>
    </head>
    <body class="home home-01">
        <!--begin header-->
        <div id="box-header"> 
            <div class="header-container">
                <div class="container overflow-show">
                    <header class="clearfix">
                        <a href="#" id="menu-button">Menu</a>
                        <nav class="top-menu">
                            <ul>
                                <li><a href="#" id="login"><i class="icon-login"></i> Login</a></li>
                                <li><a href="#"><i class="icon-add-user"></i> Register</a></li>
                                <li><a href="<?php echo site_url(); ?>contact"><i class="icon-old-phone"></i> Contact us</a></li>
                                <li><a href="<?php echo site_url(); ?>inquiry"><i class="icon-old-phone"></i> Inquiry</a></li>
                                <li><a href="#" id="sitesearch"><i class="icon-magnifying-glass"></i> <span>Search</span></a><input type="text" class="searchinput" /></li>
                            </ul>
                        </nav>
                        <div class="row">
                        <div class="col-md-2">
                            <a href="<?php echo site_url(); ?>" class="logo">
                            <?php if (!empty($logo)) { ?>
                                <img style="height: 75px;max-width: 250px;" src="<?php echo base_url(); ?>public/img_upload/<?php echo $logo ?>">
                            <?php } else { ?>
                                <img style="height: 75px;max-width: 250px;" src="<?php echo base_url(); ?>public/logo.jpg">
                            <?php } ?>
<!-- <img style="height: 75px;max-width: 250px;" title="REDOFFICE" src="<?php echo base_url(); ?>public/img_upload/"> -->
                        </a>
                        </div>
                        <div class="col-md-10">
                            <nav class="main-menu" style="width:100%">
                            <ul style="float:right;">
                                <li><a href="<?php echo site_url(); ?>" ><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                                <?php
                                foreach ($menu_category as $key) {
                                    ?>
                                    <li  style="position: static;" ><a style="font-weight: bold !important;" class="fullmenu-link" href="<?php echo $key['_link'] ?>"><?php echo ucwords($key['title']); ?> <i class="icon-chevron-down"></i></a>
                                        
                                            <?php 
                                            $fullmenuCheck=true;
                                            $fullmenuCheck2=false;


                                            foreach ($key['child'] as $k => $value) { 
                                                
                                                ?>
                                                <?php if (count($value)) { ?>
                                                     <?php if ($fullmenuCheck) { ?>
                                                    <div class="fullmenu" style="width:<?php echo $key['child_count']*20?>%">
                                                     <?php $fullmenuCheck=false;  $fullmenuCheck2=true; } ?>
                                                    <div class="menu-col" style="width:<?php echo $key['child_size']?>%">
                                                        <ul>
                                                            <li><strong><a href="#"><?php echo ucwords(str_replace('_', ' ', $k)) ?></a></strong></li>
                                                            <?php
                                                            foreach ($value as $k2 => $value2) {
                                                                ?><li><a href="<?php echo $value2['_link'] ?>"><?php echo ucwords($value2['title']); ?></a></li><?php } ?>
                                                        </ul>
                                                    </div>
                                                     
                                                <?php } ?>
                                            <?php } ?>

                                            <?php if ($fullmenuCheck2) { ?>
                                                     </div>
                                                <?php } ?>
                                       
                                    </li>
                                <?php } ?>
                            </ul>
                        </nav>
                        </div>
                        <div></div>
                            
                        </div>
                        
                        
                    </header>
                </div>
                <!--end header-->