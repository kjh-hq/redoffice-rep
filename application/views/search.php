<div class="main-container">
<div class="page-content">
<div class="container-fluid">
<nav class="woocommerce-breadcrumb" >
<a href="<?php echo site_url();?>">Home</a>
<span class="separator">>
</span>
<a href="<?php echo site_url();?>shop">Shop</a>		
<span class="separator">>
</span>		
Search result of "<?php echo $search;?>"	
</nav>
</div>
<div class="shop_content">
<div class="container-fluid">
<div class="row">
<div id="secondary" class="col-xs-12 col-md-2 sidebar-shop">
</div>
<div id="archive-product" class="col-xs-12 col-md-10">
<div class="shop-products products row grid-view sidebar">
<?php if($products!=null){?>
<?php foreach ($products as $key) {?>
<div class="item-col col-xs-12 col-sm-3 post-<?php echo $key->id;?> product type-product status-publish has-post-thumbnail  featured  product-type-simple  instock" style="height:320px;">
<div class="product-wrapper" style="height:320px;">
<div class="quickviewbtn">
<a class="detail-link quickview fa fa-external-link" data-quick-id="<?php echo $key->id;?>" href="<?php echo site_url(); ?>ps/product/<?php echo $key->id;?>" title="<?php echo $key->name;?>">Quick View</a>
</div>
<div class="list-col4 ">
<div class="product-image">
<a href="<?php echo site_url(); ?>ps/product/<?php echo $key->id;?>" title="<?php echo $key->name;?>">
<img style="height:153px;width:153px;"  src="<?php echo base_url();?>public/img_upload/medium/<?php echo $this->product_image_m->singleImage1($key->id);?>" class="primary_image wp-post-image" alt="68" />
<?php if($this->product_image_m->singleImage2($key->id)){?>
<img style="height:153px;width:153px;"  src="<?php echo base_url();?>public/img_upload/medium/<?php echo $this->product_image_m->singleImage2($key->id);?>" class="secondary_image" alt="64" />
<?php } ?>
</a>	
</div>
</div>
<div class="list-col8 ">
<div class="gridview">
<h2 class="product-name">
<a href="<?php echo site_url(); ?>ps/product/<?php echo $key->id;?>"><?php echo $key->name;?></a>
</h2>
<div class="price-box">
<span class="special-price">
<span class="amount">TK.<?php echo $key->price;?></span>
</span>
</div>
<div class="actions clearfix">
<div class="add-to-cart">
<p class="product woocommerce add_to_cart_inline " style="border:4px solid #ccc; padding: 12px;">
<span class="special-price">
<span class="amount">TK.<?php echo $key->price;?></span>
</span>			
<a href="<?php echo site_url(); ?>ps/product/<?php echo $key->id;?>" rel="nofollow" data-product_id="<?php echo $key->id;?>" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add to cart</a>
</p>
</div>
</div>
</div>
<div class="listview">
<h2 class="product-name">
<a href="<?php echo site_url(); ?>ps/product/<?php echo $key->id;?>"><?php echo $key->name;?></a>
</h2>
<div class="price-box">
<span class="special-price">
<span class="amount">TK.<?php echo $key->price;?></span>
</span>
</div>
<div class="product-desc">
<p><?php echo limit_to_numwords(strip_tags($key->body), 20);?></p>
</div>
<div class="actions clearfix">
<div class="add-to-cart">
<p class="product woocommerce add_to_cart_inline " style="border:4px solid #ccc; padding: 12px;">
<span class="special-price">
<span class="amount">TK.<?php echo $key->price;?></span>
</span>			
<a href="<?php echo site_url(); ?>ps/product/<?php echo $key->id;?>" rel="nofollow" data-product_id="<?php echo $key->id;?>" data-product_sku="" data-quantity="1" class="button add_to_cart_button product_type_simple">Add to cart</a>
</p>
</div>
<div class="add-to-links">
<div class="clear">
</div>
<div class="quickviewbtn">
<a class="detail-link quickview fa fa-external-link" data-quick-id="<?php echo $key->id;?>" href="<?php echo site_url(); ?>ps/product/<?php echo $key->id;?>" title="<?php echo $key->name;?>">Quick View</a>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix">
</div>
</div>
</div>
<?php } ?>
<?php }else{ ?>
<span ><h2>No product found by "<?php echo $search?>"</h2></span>
<?php } ?>
</div>
<?php echo $pages_p?>
</div>
</div>
</div>
</div>
</div>
</div>
