<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/style/css/shortcodes.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/style/css/js_composer.css"/>
<style type="text/css">
	/* ------------------------------------------------------------------------ */
/* Forms
/* ------------------------------------------------------------------------ */
*::-moz-selection {
    background: #1e9bc9 none repeat scroll 0 0 !important;
}
form,
fieldset { }
/* Reset iPad / iPhone Button Styling */
button, input[type=submit] {
    -webkit-appearance: none; /*Safari/Chrome*/
    -webkit-border-radius: 2px; 
}
input[type="text"],
input[type="password"],
input[type="email"],
textarea{
    -webkit-appearance: none; /*Safari/Chrome*/
}
input, textarea{
    -webkit-border-radius: 2px; 
}
input::-ms-clear {
  width : 0;
  height: 0;
}
input[type="text"],
input[type="password"],
input[type="email"],
input[type="tel"],
textarea{
    /*-moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;*/
    padding: 10px 12px;
    outline: none !important;
    border-radius: 2px;
    font-family: Arial, Helvetica, sans-serif;
    -webkit-font-smoothing: antialiased;
    font-size: 13px;
    margin: 0 0 20px 0;
    width: 200px;
    max-width: 100%;
    display: block;
    color: #999;
    line-height: 1.6;
    border: 1px solid #dddddd;
    background: #ffffff;
    -webkit-transition: all 0.2s;
       -moz-transition: all 0.2s;
        -ms-transition: all 0.2s;
         -o-transition: all 0.2s;
            transition: all 0.2s;
}
#page-wrap input[type="text"]:focus,
#page-wrap input[type="password"]:focus,
#page-wrap input[type="email"]:focus,
#page-wrap input[type="tel"]:focus,
#page-wrap textarea:focus {
    color: #666666;
    background: #ffffff;
    outline: none !important;
    border-color: #999999;
}
textarea { min-height: 60px }
select {
    display: block;
    padding: 10px 12px;
    outline: none !important;
    border-radius: 2px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 13px;
    line-height: 1.6;
    color: #666;
    margin: 0;
    max-width: 100%;
    color: #999;
    border: 1px solid #dddddd;
    background: #ffffff !important;
}
label,
legend {
    cursor: pointer;
    display: block;
}
label span,
legend span { font-weight: 600 }
button,
 input[type="submit"],
input[type="reset"],
input[type="button"]{
    cursor: pointer;
    outline: none !important;
    -webkit-font-smoothing: antialiased;
}
.wpcf7-form input[type="text"], .wpcf7-form input[type="email"] {
    background: #ffffff none repeat scroll 0 0;
    box-sizing: border-box;
    margin: 5px 0 0;
    max-width: 100%;
    width: 100% !important;
}
.wpcf7-form textarea {
    box-sizing: border-box;
    margin: 5px 0 0;
    max-width: 100%;
    width: 100%;
}
.wpcf7-form select {
    box-sizing: border-box;
    margin: 5px 0 0;
    max-width: 100%;
    width: 100%;
}
.wpcf7-form .customSelect {
    margin: 5px 0 0;
    padding: 8px 10px;
}
.wpcf7-form {
    margin: 0;
    padding: 0;
}
.wpcf7-form .wpcf7-submit {
    background: #999999 none repeat scroll 0 0;
    border: medium none;
    color: #ffffff;
    font-weight: 700;
}
.wpcf7-form input[type="checkbox"] {
    width: auto;
}
.wpcf7 form.wpcf7-form p {
    font-weight: 600;
    margin: 0 0 20px !important;
    position: relative;
}
.wpcf7-list-item {
    display: inline-block;
    margin: 0 10px 0 0 !important;
}
.wpcf7-list-item-label {
    font-weight: normal;
}
.wpcf7-response-output {
    display: none;
}
.wpcf7-form .invalid {
    color: #bf1515 !important;
}
.wpcf7-not-valid {
    border-color: #bf1515 !important;
}
.wpcf7 span.wpcf7-not-valid-tip, .wpcf7-validation-errors {
    display: none !important;
}
div.wpcf7-response-output {
    margin: 0 !important;
    padding: 5px 10px !important;
}
div.wpcf7-mail-sent-ok, div.wpcf7-mail-sent-ng, div.wpcf7-spam-blocked, div.wpcf7-validation-errors {
    background-color: #44bdbd;
    border: medium none !important;
    border-radius: 2px;
    color: #ffffff;
    display: block;
    font-weight: 700;
    margin: 0 0 30px !important;
    padding: 12px 40px 12px 20px !important;
    position: relative;
    text-align: center;
}
div.wpcf7-mail-sent-ok {
    color: #ffffff;
}
.one-line-form .wpcf7-submit {
    box-sizing: border-box;
    font-weight: 700;
    margin: 5px 0 0 !important;
    max-width: 100%;
    padding: 15px 16px;
    width: 100%;
}
.wpcf7-form .simpleselect {
    margin: 5px 0 0 !important;
}
</style>
<div id="page-wrap"  >
	<div id="content" class="page-section nopadding">
	
		<div  class="wpb_row vc_row-fluid standard-section section  section-parallax  no-repeat   " data-speed="1" style="background-image: url(http://www.virditech.com/wp-content/uploads/2015/09/top_2.png); background-repeat: no-repeat; padding-top: 150px; padding-bottom: 150px; "><div class="col span_12 color-dark left">
	<div class="vc_col-sm-12 wpb_column column_container col no-padding color-dark" style="" data-animation="" data-delay="">
		<div class="wpb_wrapper">
			<h1 class="headline font-inherit fontsize-xxl fontweight-400 lh-inherit align-center transform-inherit " style="margin: 0 0 0px 0; color: #ffffff;">Inquiry<br />
</h1>
		</div> 
	</div> 
</div>
</div>
<div  class="wpb_row vc_row-fluid standard-section section  section-no-parallax  stretch   " data-speed="1" style=""><div class="col span_12 color-dark left">
	<div class="vc_col-sm-12 wpb_column column_container col no-padding color-dark" style="" data-animation="" data-delay="">
		<div class="wpb_wrapper">
			<div class="spacer" style="height: 40px;"></div>
		</div> 
	</div> 
</div>
</div>
<div  class="wpb_row vc_row-fluid standard-section section  section-no-parallax  stretch   " data-speed="1" style=""><div class="col span_12 color-dark left">
	<div class="vc_col-sm-12 wpb_column column_container col no-padding color-dark" style="" data-animation="" data-delay="">
		<div class="wpb_wrapper">
			
	<div class="wpb_tabs wpb_content_element" data-interval="0">
		<div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">
			<ul class="wpb_tabs_nav ui-tabs-nav vc_clearfix"><li><a href="#tab-50eb17c9-44a1-5">Sales inquiry</a></li><li><a href="#tab-1445274228106-2-9">Technical inquiry</a></li><li><a href="#tab-1445274247190-2-10">etc</a></li></ul>
			
			<div id="tab-50eb17c9-44a1-5" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
				<div role="form" class="wpcf7" id="wpcf7-f17399-p16255-o1" lang="ko-KR" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/support/inquiry/#wpcf7-f17399-p16255-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="17399" />
<input type="hidden" name="_wpcf7_version" value="4.2.2" />
<input type="hidden" name="_wpcf7_locale" value="ko_KR" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f17399-p16255-o1" />
<input type="hidden" name="_wpnonce" value="54217e59f1" />
</div>
<div class="vc_row">
<div class="vc_col-sm-3"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your name" /></span></div>
<div class="vc_col-sm-3"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your email" /></span></div>
<div class="vc_col-sm-3"><span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Your Company" /></span></div>
</div>
<div style="padding:5px">
	<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea></span>
</div>
<div style="text-align:center; padding:30px 0 120px 0 ">
	<input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" />
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
			</div> 
			<div id="tab-1445274228106-2-9" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
				<div role="form" class="wpcf7" id="wpcf7-f17400-p16255-o2" lang="ko-KR" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/support/inquiry/#wpcf7-f17400-p16255-o2" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="17400" />
<input type="hidden" name="_wpcf7_version" value="4.2.2" />
<input type="hidden" name="_wpcf7_locale" value="ko_KR" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f17400-p16255-o2" />
<input type="hidden" name="_wpnonce" value="306c3636ea" />
</div>
<div class="vc_row">
<div class="vc_col-sm-3"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your name" /></span></div>
<div class="vc_col-sm-3"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your email" /></span></div>
<div class="vc_col-sm-3"><span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Your Company" /></span></div>
</div>
<div style="padding:5px">
	<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea></span>
</div>
<div style="text-align:center; padding:30px 0 120px 0 ">
	<input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" />
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
			</div> 
			<div id="tab-1445274247190-2-10" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
				<div role="form" class="wpcf7" id="wpcf7-f16292-p16255-o3" lang="ko-KR" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/support/inquiry/#wpcf7-f16292-p16255-o3" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="16292" />
<input type="hidden" name="_wpcf7_version" value="4.2.2" />
<input type="hidden" name="_wpcf7_locale" value="ko_KR" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f16292-p16255-o3" />
<input type="hidden" name="_wpnonce" value="e119e3163d" />
</div>
<div class="vc_row">
<div class="vc_col-sm-3"><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Your name" /></span></div>
<div class="vc_col-sm-3"><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your email" /></span></div>
<div class="vc_col-sm-3"><span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Your Company" /></span></div>
</div>
<div style="padding:5px">
	<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea></span>
</div>
<div style="text-align:center; padding:30px 0 120px 0 ">
	<input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" />
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
			</div> 
		</div> 
	</div> 
		</div> 
	</div> 
</div></div>
<div class="korea-sns">
<div class="korea-sns-post korea-sns-pos-center">
<div class="korea-sns-button korea-sns-facebook" OnClick="SendSNS('facebook', 'Inquiry - VIRDI', 'http://www.virditech.com/support/inquiry/', '');"  style="background-image:url('http://www.virditech.com/wp-content/plugins/korea-sns/icons/facebook.png');"></div>
<div class="korea-sns-button korea-sns-twitter" OnClick="SendSNS('twitter', 'Inquiry - VIRDI', 'http://www.virditech.com/support/inquiry/', '');"  style="background-image:url('http://www.virditech.com/wp-content/plugins/korea-sns/icons/twitter.png');"></div>
<div class="korea-sns-button korea-sns-google" OnClick="SendSNS('google', 'Inquiry - VIRDI', 'http://www.virditech.com/support/inquiry/', '');"  style="background-image:url('http://www.virditech.com/wp-content/plugins/korea-sns/icons/google.png');"></div>
<div class="korea-sns-button korea-sns-kakaostory" id="kakao-story-btn-16255-1"  OnClick="ShareKakaostory('', 'http://www.virditech.com/support/inquiry/', 'Inquiry')"  style="background-image:url('http://www.virditech.com/wp-content/plugins/korea-sns/icons/kakaostory.png');"></div>
</div>
<div style="clear:both;"></div>
</div>
		
		
			</div> <!-- end content -->
	
</div> 
<script type="text/javascript" src="<?php echo base_url();?>public/js/widget.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/js/tabs.min.js"></script>
<!-- <script type='text/javascript' src='<?php echo base_url();?>public/js/scripts.js?ver=4.2.2'></script> -->
<script type='text/javascript' src='<?php echo base_url();?>public/js/jquery.easing.min.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/waypoints.min.js?ver=4.5.1'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/waypoints-sticky.min.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/prettyPhoto.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/isotope.pkgd.min.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/functions.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/flexslider.min.js'></script>
<!-- <script type='text/javascript' src='<?php echo base_url();?>public/js/comment-reply.min.js?ver=4.3.1'></script> -->
<!-- <script type='text/javascript' src='http://www.virditech.com/wp-content/plugins/wordpress-popup/js/public.min.js?ver=4.3.1'></script> -->
<script type='text/javascript' src='<?php echo base_url();?>public/js/js_composer_front.js?ver=4.5.1'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/tabs.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?php echo base_url();?>public/js/jquery-ui-tabs-rotate.js?ver=4.5.1'>