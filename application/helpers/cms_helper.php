<?php

function make_obj_to_array($obj){
    if($obj){
            $result = json_encode($obj);
            return json_decode($result,TRUE);
        }
        return 'Object Parameter Need';
}
function btn_edit ($uri)
{
	return anchor($uri, '<i class="fa fa-edit"></i>');
}
function btn_delete ($uri)
{
	return anchor($uri, '<i class="fa fa-remove"></i>', array(
		'onclick' => "return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
	));
}
function article_link($article){
	return 'article/' . intval($article->id) . '/' . e($article->slug);
}
function article_links($articles){
	$string = '<ul>';
	foreach ($articles as $article) {
		$url = article_link($article);
		$string .= '<li>';
		$string .= '<h3>' . anchor($url, e($article->title)) .  ' â€º</h3>';
		$string .= '<p class="pubdate">' . e($article->pubdate) . '</p>';
		$string .= '</li>';
	}
	$string .= '</ul>';
	return $string;
}
function get_excerpt($article, $numwords = 50){
	$string = '';
	$url = article_link($article);
	$string .= '<h2>' . anchor($url, e($article->title)) .  '</h2>';
	$string .= '<p>' . e(limit_to_numwords(strip_tags($article->body), $numwords)) . '</p>';
	$string .= '<p>...</p>';
	$string .= '<p>' . anchor($url, 'read more ...', array('title' => e($article->title))) . '</p>';
	$string .= '<p class="pubdate">' . e($article->pubdate) . '</p>';
	return $string;
}
function limit_to_numwords($string, $numwords){
	$excerpt = explode(' ', $string, $numwords + 1);
	if (count($excerpt) >= $numwords) {
		array_pop($excerpt);
	}
	$excerpt = implode(' ', $excerpt);
	return $excerpt;
}
function e($string){
	return htmlentities($string);
}
function get_client_ip() {
	$ipaddress = '';
	if (getenv('HTTP_CLIENT_IP'))
		$ipaddress = getenv('HTTP_CLIENT_IP');
	else if(getenv('HTTP_X_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	else if(getenv('HTTP_X_FORWARDED'))
		$ipaddress = getenv('HTTP_X_FORWARDED');
	else if(getenv('HTTP_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
	else if(getenv('HTTP_FORWARDED'))
		$ipaddress = getenv('HTTP_FORWARDED');
	else if(getenv('REMOTE_ADDR'))
		$ipaddress = getenv('REMOTE_ADDR');
	else
		$ipaddress = 'UNKNOWN';
	return $ipaddress;
}
function get_menu ($array, $child = FALSE)
{
	$CI =& get_instance();
	$str = '';
	
	//foreach ($query->result() as $r){
	
	
	
	if (count($array)) {
		$str .= $child == FALSE ? '<ul class="level1" style="margin-top: 20px;">' . PHP_EOL : '<ul class="level2">' . PHP_EOL;
		
		foreach ($array as $r ) {
			
			$active = $CI->uri->segment(1) == $r->slug ? TRUE : FALSE;
			if (isset($r['children']) && count($r['children'])) {
				$str .= $active ? '<li><a class="popout level1">'. e($r['title']).'</a>' : '<li><ul>';
				//$str .= '<a  href="'.site_url(e($r['slug'])).'/'.e($r['id']).'">' .;
			//	$str .= ' &nbsp;&nbsp; <i class="fa fa-angle-down"></i> </a>' . PHP_EOL;
			
				$str .= get_menu($r['children'], TRUE);
			}
			else {
				$str .= $active ? '<li><a href="'.site_url(e($r['slug'])).'/'.e($r['id']).'" class="popout level2">'. e($r['title']).'</a>' : '<li><ul>';
				//$str .= '<a href="'.site_url(e($r['slug'])).'/'.e($r['id']).'">&nbsp;' . e($r['title']) . '</a>';
			}
			
			
			$str .= '</li>' . PHP_EOL;
		}
		
		$str .= '</ul>' . PHP_EOL;
	}
	
	return $str;
}
function place1($id){
	
			$CI =& get_instance();
			$str = '';
			
			$CI->load->database();
			$sql="select * from `articles` where parent_id = ".$id."";
			$query=$CI->db->query($sql);
			
			foreach ($query->result() as $r ) {
			$str .= '<li class="item first">';
			$str .= '<div class="showhome_hover">';
			$str .= '<div class="product_img_name">';
			$str .= '<a href="'.site_url().''.e($r->slug).''.e($r->id).'"
					title="'.e($r->name).'"
					class="product-image"> 
					<img  src="'.base_url().'/public/'.e($r->full_image_name).'"
													alt="" width="175" height="175"></a>';
			$str .= '</div>';
			$str .= '<h2 class="product-name">';
			$str .= '<a href="'.site_url().''.e($r->slug).''.e($r->id).'" title="'.e($r->name).'">'.e($r->name).'</a>';
			$str .= '</h2>';
			
			$str .= '<div class="home_price">
					<div class="price-box">
					<p class="old-price">
					<span class="price" id="old-price-8436">'.e($r->price).'</span>
					</p>
					<p class="special-price">
					<span class="price" id="product-price-8436">'.e($r->price-($r->price*(e($r->discount)/100))).'</span>
					</p>
					<div class="price_discounts">
					<span class="discount"> <span class="disount_label">Discount
					</span> <span class="disount_percnt">'.e($r->discount).'%</span>
					</span> <span class="saved_amount"> <span class="save_label">Save </span> '.e($r->price*(e($r->discount)/100)).' </span>
					</div>	
					</div>
					</div>';
			$str .= '</div>';
			$str .= '	</li>';
			
			
			}
	
	
	return $str;
}
function place2($id1){
	
			$CI =& get_instance();
			$str = '';
			
			$CI->load->database();
			$sql="select * from `articles` where parent_id = ".$id1." order by id desc limit 4";
			$query=$CI->db->query($sql);
			
			
			foreach ($query->result() as $r ) {
			$str .= '<li class="item">';
			$str .= '<div class="showhome_hover">';
			$str .= '<div class="product_img_name">';
			$str .= '<a href="'.site_url().''.e($r->slug).''.e($r->id).'"
					title="'.e($r->name).'"
					class="product-image"> 
					<img  src="'.base_url().'/public/'.e($r->full_image_name).'"
													alt="" width="175" height="175"></a>';
			$str .= '</div>';
			$str .= '<h2 class="product-name">';
			$str .= '<a href="'.site_url().''.e($r->slug).''.e($r->id).'" title="'.e($r->name).'">'.e($r->name).'</a>';
			$str .= '</h2>';
			
			$str .= '<div class="home_price">
					<div class="price-box">
					<p class="old-price">
					<span class="price" id="old-price-8436">'.e($r->price).'</span>
					</p>
					<p class="special-price">
					<span class="price" id="product-price-8436">'.e($r->price-($r->price*(e($r->discount)/100))).'</span>
					</p>
					<div class="price_discounts">
					<span class="discount"> <span class="disount_label">Discount
					</span> <span class="disount_percnt">'.e($r->discount).'%</span>
					</span> <span class="saved_amount"> <span class="save_label">Save </span> '.e($r->price*(e($r->discount)/100)).' </span>
					</div>	
					</div>
					</div>';
			$str .= '</div>';
			$str .= '	</li>';
			
			
			}
			
		
	
	
	return $str;
}
function place_also($id){
	
			$CI =& get_instance();
			$str = '';
			
			$CI->load->database();
			$sql="select * from `articles` where id = ".$id."";
			$query=$CI->db->query($sql);
			$ri=$query->row(0);
			$CI->load->database();
			$sql="select * from `articles` where parent_id = ".$ri->parent_id." limit 4";
			$query=$CI->db->query($sql);
			
			foreach ($query->result() as $r ) {
			$str .= '<li class="item first">';
			$str .= '<div class="showhome_hover">';
			$str .= '<div class="product_img_name">';
			$str .= '<a href="'.site_url().''.e($r->slug).''.e($r->id).'"
					title="'.e($r->name).'"
					class="product-image"> 
					<img  src="'.base_url().'/public/'.e($r->mid_image_name).'"
													alt="" width="175" height="175"></a>';
			$str .= '</div>';
			$str .= '<h2 class="product-name">';
			$str .= '<a href="'.site_url().''.e($r->slug).''.e($r->id).'" title="'.e($r->name).'">'.e($r->name).'</a>';
			$str .= '</h2>';
			
			$str .= '<div class="home_price">
					<div class="price-box">
					<p class="old-price">
					<span class="price" id="old-price-8436">'.e($r->price).'</span>
					</p>
					<p class="special-price">
					<span class="price" id="product-price-8436">'.e($r->price-($r->price*(e($r->discount)/100))).'</span>
					</p>
					<div class="price_discounts">
					<span class="discount"> <span class="disount_label">Discount
					</span> <span class="disount_percnt">'.e($r->discount).'%</span>
					</span> <span class="saved_amount"> <span class="save_label">Save </span> '.e($r->price*(e($r->discount)/100)).' </span>
					</div>	
					</div>
					</div>';
			$str .= '</div>';
			$str .= '	</li>';
			
			
			}
	
	
	return $str;
}
/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump')) {
	function dump ($var, $label = 'Dump', $echo = TRUE)
	{
		// Store dump in variable
		ob_start();
		var_dump($var);
		$output = ob_get_clean();
		// Add formatting
		$output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
		$output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';
		// Output
		if ($echo == TRUE) {
			echo $output;
		}
		else {
			return $output;
		}
	}
}
if (!function_exists('dump_exit')) {
	function dump_exit($var, $label = 'Dump', $echo = TRUE) {
		dump ($var, $label, $echo);
		exit;
	}
}
