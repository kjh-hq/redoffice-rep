<?php

class MY_Controller extends CI_Controller {

    public $data = array();

    function __construct() {

        parent:: __construct();

        $this->data['errors'] = array();

        $this->data['site_name'] = config_item('site_name');

        $this->load->library('form_validation');

        $this->load->model('dashboard_m');
        $this->load->model('snippets_m');

        $this->load->model('page_m');

        $this->data['dashboard'] = $this->dashboard_m->get(1);
        $snippets_data = $this->snippets_m->get();
        $snippets = array();
        foreach ($snippets_data as $key => $value) {
            $snippets[$value->snippets_tag] = $value->snippets_details;
        }
        $this->data['snippets'] = $snippets;

        $this->data['menu_category'] = $this->page_m->get_menu();
    }

}
