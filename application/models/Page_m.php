<?php
class Page_m extends MY_Model
{
	protected $_table_name = 'pages';
	protected $_order_by = '_parent_id, _sort asc';
	public $rules = array(
		'_parent_id' => array(
			'field' => '_parent_id', 
			'label' => 'Parent', 
			'rules' => 'trim|intval'
		), 
		
		'title' => array(
			'field' => 'title', 
			'label' => 'Title', 
			'rules' => 'trim|required|max_length[100]|xss_clean'
		), 
	

	);

	public function get_new ()
	{
		$page = new stdClass();
		$page->title = '';
		$page->slug = '';
		$page->_parent_id = 0;
		return $page;
	}

	public function get_child_of($id){
		$this->db->select();
		$this->db->where('_parent_id',$id);
		return parent::get();

	}

	public function get_menu(){
		$this->db->select();
		$this->db->where('_parent_id',0);
		$result = parent::get();
		$data = array();
		foreach ($result as $key => $value) {
			$d=make_obj_to_array($value);
			$this->db->select();
			$this->db->where('_parent_id',$value->id);
			$result2 = parent::get();
			$d['child'] = array(
				'product'=>[],
				'solution'=>[],
				'technology'=>[],
				'support'=>[],
				'project_referrance'=>[],
				);
			
			foreach($result2 as $key2=>$value2){
				$d['child'][$value2->topic][] = make_obj_to_array($value2);
			}
			$d['child_count'] = 0;
			$d['child_size'] = 0;	
			foreach($d['child'] as $value3){
				if(count($value3)){
					$d['child_count']=$d['child_count']+1;	
				}
			}
			$d['child_size'] = 100/$d['child_count'];
			$data[] = $d;
		}
		return $data;
	}

}