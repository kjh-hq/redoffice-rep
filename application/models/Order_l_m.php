<?php
class Order_l_m extends MY_Model
{
	protected $_table_name = 'order_list';
	protected $_order_by = 'id desc';
	protected $_timestamps = FALSE;
	public $rules = array(
			'name' => array(
					'field' => 'name',
					'label' => 'Name',
					'rules' => 'trim|required'
			),
			'phone' => array(
					'field' => 'phone',
					'label' => 'Phone Number',
					'rules' => 'trim|required'
			),
			'cnt' => array(
					'field' => 'cnt',
					'label' => 'Country',
					'rules' => 'trim|required'
			),
			'addr' => array(
					'field' => 'addr',
					'label' => 'Address',
					'rules' => 'trim|required'
			),
			
	);
	
	
	public function index(){
	
	}
	

	public function get_new ()
	{
		$order = new stdClass();
		$order->product_id = '';
		$order->order_id = '';
		return $order;
	}
	
	function get_order($id){
		$this->db->select('id')->from('order_list')->where('order_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	
		function order_count(){
		$this->db->select('id')->from('order_list');
		$query=$this->db->get();
		return $query->num_rows();
	}
}