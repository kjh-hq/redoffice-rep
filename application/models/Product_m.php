<?php
class Product_m extends MY_Model
{
	protected $_table_name = 'products';
	protected $_order_by = 'pubdate desc, id desc';
	protected $_timestamps = TRUE;
	public $rules = array(
		'pubdate' => array(
			'field' => 'pubdate', 
			'label' => 'Publication date', 
			'rules' => 'trim|required'
			), 
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|max_length[100]|xss_clean'
			), 
		'image_name' => array(
			'field' => 'image_name', 
			'label' => 'image_name', 

			), 
		'full_image_name' => array(
			'field' => 'full_image_name', 
			'label' => 'full_image_name', 

			), 
		'mid_image_name' => array(
			'field' => 'mid_image_name', 
			'label' => 'mid_image_name', 

			), 
		'small_image_name' => array(
			'field' => 'small_image_name', 
			'label' => 'small_image_name', 

			), 

		'body' => array(
			'field' => 'body', 
			'label' => 'Body', 
			'rules' => 'trim|required'
			)
		);
	
	
	public function index(){

	}
	

	public function get_new ()
	{
		$article = new stdClass();
		$article->title = '';
		$article->slug = '';
		$article->image_name='';
		$article->full_image_name='';
		$article->mid_image_name='';
		$article->small_image_name='';
		$article->body = '';
		$article->header = '';
		$article->pubdate = date('Y-m-d');
		return $article;
	}
	
	public function get_with_parent ($id = NULL, $single = FALSE)
	{
		$this->db->select('pages.*, p.slug as parent_slug, p.title as parent_title');
		$this->db->join('pages as p', 'pages._parent_id=p.id', 'left');
		$pages = parent::get();

		// Return key => value pair array
		$array = array(

			);
		if (count($pages)) {
			foreach ($pages as $page) {
				$array[$page->id] = $page->title;
			}
		}

		return $array;
	}
	
	function get_post_count(){
		$this->db->select('id')->from('products');
		$query=$this->db->get();
		return $query->num_rows();
	}
	
	function get_post($num=20,$start=0){
		$this->db->select()->from('products')->limit($num,$start);
		$query=$this->db->get();
		return $query->result();
	}

	function get_product_count($id){
		$this->db->select('id')->from('products')->where('_parent_id',$id);
		$query=$this->db->get();
		return $query->num_rows();
	}
	function get_product($id,$num=20,$start=0){
		$this->db->select()->from('products')->where('_parent_id',$id)->order_by('name','asc')->limit($num,$start);
		$query=$this->db->get();
		return $query->result();
	}

	function search_product_count($search){
		$this->db->select('id')->from('products')->like('name',$search);
		$query=$this->db->get();
		return $query->num_rows();
	}
	function search_product($search,$num=20,$start=0){
		$this->db->select()->from('products')->like('name',$search)->order_by('name','asc')->limit($num,$start);
		$query=$this->db->get();
		return $query->result();
	}
	
	function get_products($ids){
		$sql ="SELECT * from products where _parent_id in ($ids) limit 20";
		$query=$this->db->query($sql);
		return $query->result();
	}

	function get_products_home($ids){
		$sql ="SELECT * from products where _parent_id in ($ids) AND _status = 1 limit 20";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	function get_child_of($id){
		$this->db->select()->from('products')->where('_parent_id',$id);
		$query=$this->db->get();
		return $query->result();
	}

	function get_product_code($id){
		$this->db->select()->from('products')->where('id',$id);
		$query=$this->db->get();
		$product = $query->row();
		$page = $this->product_m->get_parent_cat($product->_parent_id);
		$language = explode(" ", $page->title);
		$str="";
		foreach ($language as $value) {
			$str.=$value[0];
		}
		$str.="_";
		$str.=$product->id;
		return $str;
	}
	function get_parent_cat($id){
		$this->db->select()->from('pages')->where('id',$id);
		$query=$this->db->get();
		return $query->row();
	}
	
	function get_related($id){
		$this->db->select('_parent_id')->from('products')->where('id',$id);
		$query=$this->db->get();
		$parent=$query->row();
		$this->db->select()->from('products')->where('_parent_id',$parent->_parent_id)->limit(6,0);
		$this->db->where('id !=', $id);
		$query2=$this->db->get();
		return $query2->result();
	}
	
	function get_product_sort($id,$num=20,$start=0,$name_type,$name){
		$this->db->select()->from('products')->where('_parent_id',$id)->order_by($name_type,$name)->limit($num,$start);
		$query=$this->db->get();
		return $query->result();
	}

	function get_sales_type_products($id){
		$sql ="SELECT * from products where sales_type =$id limit 10";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_related_products_with_img($str){
		$sql ="SELECT `products`.*,pages.title,(SELECT image from `product_image` WHERE `product_image`.`product_id`=`products`.`id` limit 1) as image FROM `products` 
		join `pages`
		on `pages`.`id`=`products`.`_parent_id`
		WHERE `products`.`id` in (".$str.")";
		$query=$this->db->query($sql);
		$result=$query->result();
		$array = array();
		foreach ($result as $key => $value) {
			$array[$value->title][] = $value;
		}
		return $array;
	}
	
}