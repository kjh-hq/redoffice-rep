<?php
class Visitor_M extends MY_Model
{
	
	protected $_table_name = 'visitor';
	protected $_order_by = 'id desc';
	protected $_timestamps = TRUE;
	public $rules = array(
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'trim|required|valid_email|xss_clean'
		), 
		'password' => array(
			'field' => 'password', 
			'label' => 'Password', 
			'rules' => 'trim|required'
		)
	);
	public $rules_client = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|xss_clean'
		), 
		'phone' => array(
			'field' => 'phone', 
			'label' => 'Phone', 
			'rules' => 'trim|required|xss_clean'
		), 
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean'
		), 
		'password' => array(
			'field' => 'password', 
			'label' => 'Password', 
			'rules' => 'trim|matches[password_confirm]'
		),
		'password_confirm' => array(
			'field' => 'password_confirm', 
			'label' => 'Confirm password', 
			'rules' => 'trim|matches[password]'
		),
	);

	function __construct ()
	{
		parent::__construct();
	}

	public function login ()
	{
		$visitor = $this->get_by(array(
			'email' => $this->input->post('email'),
			'password' => $this->hash($this->input->post('password')),
		), TRUE);
		
		if (count($visitor)) {
			// Log in user
			$data = array(
				'name' => $visitor->name,
				'email' => $visitor->email,
				'id' => $visitor->id,
				'loggeding' => TRUE,
			);
			$this->session->set_userdata($data);
		}
	}

	public function logout ()
	{
		$this->session->sess_destroy();
	}
	public function name ()
	{
		return $this->session->userdata('name');
	}
	public function email ()
	{
		return $this->session->userdata('email');
	}
	public function loggedin ()
	{
		return (bool) $this->session->userdata('loggeding');
	}
	
	public function get_new(){
		$visitor = new stdClass();
		$visitor->name = '';
		$visitor->email = '';
		$visitor->password = '';
		return $visitor;
	}

	public function hash ($string)
	{
		return hash('sha512', $string . config_item('encryption_key'));
	}
	
	function visitor_count(){
		$this->db->select('id')->from('visitor');
		$query=$this->db->get();
		return $query->num_rows();
	}
	
		function get_five(){
		$this->db->select()->from('visitor')->order_by('created','desc')->limit(5,0);
		$query=$this->db->get();
		return $query->result();
	}
	

	function password($email){
		$this->db->select('email,password')->from('visitor')->where('email',$email)->limit(1,0);
		$query=$this->db->get();
		return $query->row();
	}
}