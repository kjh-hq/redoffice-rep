<?php
class Dashboard_m extends MY_Model
{
	protected $_table_name = 'dashboard';
	protected $_order_by = 'id desc';
	protected $_timestamps = FALSE;
	public $rules = array(
		'about' => array(
			'field' => 'about', 
			'label' => 'Payment Type', 
			'rules' => 'trim|xss_clean'
		
		), 
	);
	
	public $rules_search = array(
		'search' => array(
			'field' => 'search', 
			'label' => 'search', 
			'rules' => 'trim|xss_clean'
		
		), 
	);

	public function index(){
	
	}
	
	public function get_new ()
	{
		$dashboard = new stdClass();
		return $dashboard;
	}
	
	function get_about(){
		$this->db->select('about')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
	
		function get_customer(){
		$this->db->select('customer')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
	
		function get_return_policy(){
		$this->db->select('return_policy')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
	
		function get_how(){
		$this->db->select('how')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
	
		function get_faq(){
		$this->db->select('faq')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
}