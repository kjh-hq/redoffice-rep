$(function () {

    // forgotten password //

    $('.forgotten-password').click(function (event) {

        event.preventDefault();

        $(".login-form").hide();
        $(".forgot-form").show();
       
    });

    $('.forgot-button').click(function (event) {

        event.preventDefault();

        var username = $("#forgot-username").val();

        $.ajax({
            url: '/Callback/SendPasswordReset',
            type: 'POST',
            data: {
                username: username
            },
            success: function (data) {

                $(".forgot-form").hide();
                $(".forgot-confirmation").show();
                

            },
            error: function (data) { }
        });

    });

    $('.return-login-button').click(function (event) {

        event.preventDefault();

        $(".forgot-confirmation").hide();
        $(".login-form").show();

    });

    // add to basket //

    $('.btn-add-to-basket').click(function (event) {

        event.preventDefault();

        var optionId = $(this).parent().parent().find(".options").val();
        var quantity = $(this).parent().parent().find(".quantity").val();

        document.location = $(this).parent().parent().find(".addtemplate").val().replace("$1", optionId).replace("$2", quantity)

    });

    // vimeo thumb //
    
    $(".vimeothumb").each(function () {

        console.log("vimeothumb called");

        var $this = this;

        $.ajax({
            type: 'GET',
            url: 'http://vimeo.com/api/v2/video/' + $(this).data("vimeoref") + '.json',
            jsonp: 'callback',
            dataType: 'jsonp',
            success: function (data) {

                console.log("vimeothumb success");
                console.log("vimeothumb data: " + data[0]);

                var video = data[0];

                $this.src = video.thumbnail_medium;

            },
            error: function (e) {
                console.log("vimeothumb error");
                console.log("vimeothumb message: " + JSON.stringify(e, null, 4));
            }
        });

    });

    // show more //

    $('.showmore').click(function (event) {

        event.preventDefault();

        $(this).hide();

        $("." + $(this).data("more")).show();

    });

    // all products downloads //

    $('.btn-download-all').click(function (event) {

        event.preventDefault();

        $.ajax({
            url: '/Callback/ProductDownloadsZip',
            type: 'POST',
            data: {
                producttranslationid: $(this).data("producttranslationid")
            },
            success: function (data) { document.location = data; },
            error: function (data) { alert("There was a problem downloading the file") }
        });

    });
    
    // installer upgrade //

    $('.btn-installer-upgrade').click(function (event) {

        event.preventDefault();

        $(".upgrade-form").hide();
        $(".upgrade-confirmation").show();

        $.ajax({
            url: '/Callback/InstallerUpgradeRequest',
            type: 'POST',
            data: {
                accountid: $(this).data("accountid")
            },
            success: function (data) { },
            error: function (data) { }
        });

    });

    // returns //

    $(".chk-return").change(function () {

        if ($(this).is(":checked")) {

            $(this).parent().parent().next().children().first().prop('disabled', false);
            $(this).parent().parent().next().next().children(":first").prop('disabled', false);

            $(this).parent().parent().next().children().first().data("required", 1);
            $(this).parent().parent().next().next().children(":first").data("required", 1);

        } else {

            $(this).parent().parent().next().children().first().prop('disabled', true);
            $(this).parent().parent().next().next().children(":first").prop('disabled', true);

            $(this).parent().parent().next().children().first().data("required", 0);
            $(this).parent().parent().next().next().children(":first").data("required", 0);

        }

    });

    // favourites //

    $('.btn-addtofavourites a').click(function (event) {

        event.preventDefault();

        $(".btn-addtofavourites").hide();
        $(".btn-removefromfavourites").show();

        $.ajax({
            url: '/Callback/AddToFavourites',
            type: 'POST',
            data: {
                productid: $(this).data("productid"),
                accountid: $(this).data("accountid"),
            },
            success: function (data) { },
            error: function (data) { }
        });

    });

    $('.btn-removefromfavourites a').click(function (event) {

        event.preventDefault();

        $(".btn-addtofavourites").show();
        $(".btn-removefromfavourites").hide();

        $.ajax({
            url: '/Callback/RemoveFromFavourites',
            type: 'POST',
            data: {
                productid: $(this).data("productid"),
                accountid: $(this).data("accountid"),
            },
            success: function (data) { },
            error: function (data) { }
        });

    });

    // contact form //

    $( ".enquirytype" ).change(function()
    {
        $(".enquirytype1").hide();
        $(".enquirytype2").hide();
        $(".enquirytype3").hide();

        switch ($(this).val()) {
            case "Marketing & sales literature":
                $(".enquirytype1").show();
                break;
        
            case "Technical manuals":
                $(".enquirytype2").show();
                break;

            case "Images request":
                $(".enquirytype3").show();
                break;
        }

    });

    // search //

    $('#sitesearch').click(function (event) {

        event.preventDefault();

        $("#sitesearch span").hide();
        $(".searchinput").show();
        $(".searchinput").focus();

    });

    $('.searchinput').focusout(function (event) {

        $(this).hide();
        $("#sitesearch span").show();

    });

    $('.searchinput').keypress(function (e) {
        if (e.which == 13) {

            document.location = "/search?criteria=" + $(".searchinput").val();

        }
    });

    $('.searchinput2').keypress(function (e) {
        if (e.which == 13) {

            document.location = "/search?criteria=" + $(".searchinput2").val();

        }
    });

    $('.searchinput3').keypress(function (e) {
        if (e.which == 13) {

            document.location = "/search?criteria=" + $(".searchinput3").val();

        }
    });

    $('.btn-search').click(function (event) {
        event.preventDefault();
        document.location = "/search?criteria=" + $(this).prev().val();
    });

    // newsletter signup //

    $('.newsletter-signup').click(function (event) {

        event.preventDefault();

        var container = $(this).closest(".newsletter");

        container.find(':input[type=text]').each(function () {

            if ($(this).val().trim().length > 0) {

                document.location = "/register?email=" + $(this).val();

                /*$(".newsletter-form").hide();
                $(".newsletter-thanks").show();

                $.ajax({
                    url: '/Callback/AddToMailingList',
                    type: 'POST',
                    data: {
                        email: $(this).val()
                    },
                    success: function (data) { },
                    error: function (data) { }
                });*/

            }

        });

    });

    // login //

    $('.login-button').click(function (event) {

        event.preventDefault();

        var username = $("#login-username").val();
        var password = $("#login-password").val();

        $.ajax({
            url: '/Callback/CheckLogin',
            type: 'POST',
            data: {
                username: username,
                password: password
            },
            success: function(data) {

                if (data != "Invalid Username or Password.") {

                    //$(".login-form").hide();
                    //$(".login-thanks").show();
                    //$(".login-thanks .btn").attr("href", data);
                    
                    document.location = data;

                } else {

                    $(".login-error-message").text(data);

                }

            },
            error: function (data) { }
        });

    });


    // form validation //

    $('.form-validation input[type=submit]').click(function (event) {

        var passed = true;

        var containingForm = $(this).closest("form");

        containingForm.find(':input').each(function () {

            $(this).removeClass("has-error");

            if ($(this).data("required") == "1" && $(this).val().trim() == "")
            {
                $(this).addClass("has-error");
                passed = false;
            }

        });

        containingForm.find('select').each(function () {

            $(this).removeClass("has-error");

            if ($(this).data("required") == "1" && $(this).val().trim() == "") {
                $(this).addClass("has-error");
                passed = false;
            }

        });

        return passed;

    });

    // simple search //

    $('.simple-search-form').each(function () {

        $(this).find(".btn-search").click(function (e) {

            e.preventDefault();

            var containingForm = $(this).closest(".simple-search-form");

            var querystring = "";
            var baseurl = containingForm.find('#baseurl').first().val();

            containingForm.find('input[type=text]').each(function () {

                querystring += "&" + $(this).data("query") + "=" + $(this).val();

            });

            containingForm.find('select').each(function () {

                querystring += "&" + $(this).data("query") + "=" + $(this).val();

            });

            querystring = baseurl + "?" + querystring.substr(1);

            window.location.replace(querystring);

        });
    });


    // fitvids //

    $(".videocontainer").fitVids();

    // fancybox

    $(".fancybox").fancybox({
        maxWidth: 800,
        maxHeight: 600,
        fitToView: false,
        width: '70%',
        height: '70%',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    $(".fancybox-auto").fancybox({
        fitToView: false,
        autoSize: true,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    // anchor divs //

    var $window = $(window);

    $window.scroll(function () {

        if ($('.sticky-anchor').length > 0) {

            var window_top = $(window).scrollTop();
            var div_top = $('.sticky-anchor').offset().top;
            var stop_top = $('.sticky-anchor-bottom').offset().top;

            stop_top = stop_top - $('.sidenav').height();

            if (div_top > 0) {

                if (window_top > div_top && window_top < stop_top) {
                    $('.sidenav').addClass('sticky');
                } else {
                    $('.sidenav').removeClass('sticky');
                }

                //$("#loginform h2").text($('.sidenav').height());
            }
        }
    });

    // login slide down //

    $("#login, .login").click(function (event) {

        event.preventDefault();

        $('html, body').animate({

            scrollTop: $("#loginform").offset().top
            
        }, 1000);
        
        $("#login-username").focus();

    });
    
    // form file input //
    
    $("[type=file]").on("change", function () {
        // Name of file and placeholder
        var file = this.files[0].name;
        var dflt = $(this).attr("placeholder");
        if ($(this).val() != "") {
            $(this).next().text(file);
        } else {
            $(this).next().text(dflt);
        }
    });
    
    // faqs //
    
    $(".faqs-list a").click(function (event) {

        event.preventDefault();

        $(this).parent().next().toggle();
        
    });

    $(".faqs-list .close").click(function (event) {

        event.preventDefault();

        $(this).parent().parent().toggle();

    });
    
    // collapse + expand //

    $(".reveal-list .expand").click(function (event) {

        event.preventDefault();

        $(this).hide();

        $(this).parent().next().toggle();

    });

    $(".reveal-list .collapse").click(function (event) {

        event.preventDefault();

        $(this).parent().parent().prev().children().show();

        $(this).parent().parent().toggle();

    });

    
    
    // tabs //
    
    $('ul.tabs').each(function () {

        var $active, $content, $links = $(this).find('a');

        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
        $active.addClass('current');

        $content = $($active[0].hash);

        $links.not($active).each(function () {
            $(this.hash).hide();
        });

        $(this).on('click', 'a', function (e) {

            $active.removeClass('current');
            $content.hide();

            $active = $(this);
            $content = $(this.hash);

            $active.addClass('current');
            $content.show();

            e.preventDefault();
        });
    });
    
    $('ul.tabs-no-default').each(function () {

        var $active, $content, $links = $(this).find('a');

        $links.each(function () {
            $(this.hash).hide();
        });

        $(this).on('click', 'a', function (e) {

            if ($active) {
                $active.removeClass('current');
                $content.hide();
            }
            
            $active = $(this);
            $content = $(this.hash);

            $active.addClass('current');
            $content.show();

            e.preventDefault();
        });
    });
    
    // dropdown //
    
    $('.categorydropdown').each(function () {

        $(this).click(function () {

            $(this).next().toggle();
            
        });
    });
    
    // Mobile menu
    $('#menu-button').on('click', function (e) {
        e.preventDefault();
        $('#menu-button, .main-menu').toggleClass('active');
        $('body').toggleClass('pushed-right');
    });

});

// When the window has finished loading create our google map below
// google.maps.event.addDomListener(window, 'load', init);

// function init() {
    
//     LoadContactMap();

//     //LoadSupplierMap();
    
//     //LoadEventsMap();

// }

// function LoadContactMap() {

//     if ($("#map1").length > 0) {

//         var mapOptions = {
//             zoom: 4,
//             mapTypeControl: false,
//             disableDefaultUI: false,
//             draggable: true,
//             scrollwheel: false,

//             center: new google.maps.LatLng(52.2981293, 3.7794798),

//             styles: [
//                 {
//                     "featureType": "water",
//                     "elementType": "geometry",
//                     "stylers": [
//                         {
//                             "color": "#333333"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "featureType": "landscape",
//                     "elementType": "geometry",
//                     "stylers": [
//                         {
//                             "color": "#444444"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "featureType": "road.highway",
//                     "elementType": "geometry.fill",
//                     "stylers": [
//                         {
//                             "color": "#444444"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "featureType": "road.highway",
//                     "elementType": "geometry.stroke",
//                     "stylers": [
//                         {
//                             "color": "#444444"
//                         },
//                         {
//                             "lightness": 0
//                         },
//                         {
//                             "weight": 0.2
//                         }
//                     ]
//                 },
//                 {
//                     "featureType": "road.arterial",
//                     "elementType": "geometry",
//                     "stylers": [
//                         {
//                             "color": "#444444"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "featureType": "road.local",
//                     "elementType": "geometry",
//                     "stylers": [
//                         {
//                             "color": "#444444"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "featureType": "poi",
//                     "elementType": "geometry",
//                     "stylers": [
//                         {
//                             "color": "#444444"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "elementType": "labels.text.stroke",
//                     "stylers": [
//                         {
//                             "visibility": "off"
//                         },
//                         {
//                             "color": "#666666"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "elementType": "labels.text.fill",
//                     "stylers": [
//                         {
//                             "saturation": 0
//                         },
//                         {
//                             "color": "#999999"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "elementType": "labels.icon",
//                     "stylers": [
//                         {
//                             "visibility": "off"
//                         }
//                     ]
//                 },
//                 {
//                     "featureType": "transit",
//                     "elementType": "geometry",
//                     "stylers": [
//                         {
//                             "color": "#444444"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "featureType": "administrative",
//                     "elementType": "geometry.fill",
//                     "stylers": [
//                         {
//                             "color": "#444444"
//                         },
//                         {
//                             "lightness": 0
//                         }
//                     ]
//                 },
//                 {
//                     "featureType": "administrative",
//                     "elementType": "geometry.stroke",
//                     "stylers": [
//                         {
//                             "color": "#333333"
//                         },
//                         {
//                             "lightness": 0
//                         },
//                         {
//                             "weight": 1.2
//                         }
//                     ]
//                 }]
//         };

//         var mapElement = document.getElementById('map1');

//         var map = new google.maps.Map(mapElement, mapOptions);

//         var myLatLng = new google.maps.LatLng(53.4255101, -1.2392551);
//         var marker = new google.maps.Marker({
//             position: myLatLng,
//             map: map
//         });

//         var myLatLng = new google.maps.LatLng(53.4320883, -1.2420101);
//         var marker = new google.maps.Marker({
//             position: myLatLng,
//             map: map
//         });

//         var myLatLng = new google.maps.LatLng(53.4261715, -1.2457447);
//         var marker = new google.maps.Marker({
//             position: myLatLng,
//             map: map
//         });

//         var myLatLng = new google.maps.LatLng(51.4125078, -0.7535888);
//         var marker = new google.maps.Marker({
//             position: myLatLng,
//             map: map
//         });

//         var myLatLng = new google.maps.LatLng(45.501063, 9.1611293);
//         var marker = new google.maps.Marker({
//             position: myLatLng,
//             map: map
//         });
//     }
// }

// function LoadSupplierMap() {

//     if ($("#suppliermap").length > 0) {

//         var mapOptions = {
//             zoom: 13,
//             mapTypeControl: false,
//             disableDefaultUI: false,
//             draggable: true,
//             scrollwheel: false,
//             center: new google.maps.LatLng(53.382715, -1.470274)
//         };

//         var mapElement = document.getElementById('suppliermap');

//         var map = new google.maps.Map(mapElement, mapOptions);

//         new FixedOverlay(map, new google.maps.LatLng(53.382817, -1.450876), '<div class="map-overlay">1</div>');
//         new FixedOverlay(map, new google.maps.LatLng(53.380667, -1.490530), '<div class="map-overlay">2</div>');
//         new FixedOverlay(map, new google.maps.LatLng(53.392850, -1.469759), '<div class="map-overlay">3</div>');
//     }
// }

// function LoadEventsMap() {

//     if ($("#directionsmap").length > 0) {

//         var mapOptions = {
//             zoom: 15,
//             mapTypeControl: false,
//             disableDefaultUI: false,
//             draggable: false,
//             scrollwheel: false,
//             center: new google.maps.LatLng(53.384641, -1.474334)
//         };

//         var mapElement = document.getElementById('directionsmap');

//         var map = new google.maps.Map(mapElement, mapOptions);
        
//         var marker = new google.maps.Marker({
//             position: new google.maps.LatLng(53.384641, -1.474334),
//             map: map
//         });

//     }
// }

// FixedOverlay.prototype = new google.maps.OverlayView();

// FixedOverlay.prototype.onAdd = function () {
//     var container = document.createElement('div'),
//         that = this;

//     if (typeof this.get('content').nodeName !== 'undefined') {
//         container.appendChild(this.get('content'));
//     } else {
//         if (typeof this.get('content') === 'string') {
//             container.innerHTML = this.get('content');
//         } else {
//             return;
//         }
//     }
//     container.style.position = 'absolute';

//     google.maps.event.addDomListener(container, 'mouseup', function () {
//         this.style.cursor = 'default';
//         google.maps.event.removeListener(that.moveHandler);
//     });


//     this.set('container', container)
//     this.getPanes().floatPane.appendChild(container);
// };

function FixedOverlay(map, position, content) {
    if (typeof draw === 'function') {
        this.draw = draw;
    }
    this.setValues({
        position: position,
        container: null,
        content: content,
        map: map
    });
}

FixedOverlay.prototype.draw = function () {
    var pos = this.getProjection().fromLatLngToDivPixel(this.get('position'));
    this.get('container').style.left = pos.x + 'px';
    this.get('container').style.top = pos.y + 'px';
};

FixedOverlay.prototype.onRemove = function () {
    this.get('container').parentNode.removeChild(this.get('container'));
    this.set('container', null)
};