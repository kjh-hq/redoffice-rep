<?php
class Admin_Controller extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->data ['meta_title'] = 'Admin Panel';
		$this->data ['developed_by'] = '';
		$this->load->helper ('form');
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'session' );
		$this->load->model ( 'user_m' );

		$config['upload_path'] = '../public/img_upload/';
		$config['allowed_types'] = 'gif|jpg|png|pdf|zip';


		$this->load->library('upload', $config);
		$this->load->model('admin_page_m');
		$this->load->model('admin_permissions_m');
		$this->load->model('admin_permissions_profile_m');
		if($this->user_m->auth()){
			$sql="SELECT perm.*,ap.* 
			FROM admin_permissions as perm inner join admin_page as ap 
			on perm.perm_page=ap.ap_id 
			WHERE  perm.perm_profile=".$this->user_m->auth()." order by _sort asc";
			$query=$this->db->query($sql);
			$this->data['ap_menu']=$query->result();
			$result = $this->data['ap_menu'];
			$ap_menu = array();
			foreach ($result as $key) {

				if($key->perm_view){
					$ap_menu=$key;
				}
			}
		}

// Login check
		$exception_uris = array(
			'user/login',
			'user/logout'
			);
		if (in_array(uri_string(), $exception_uris) == FALSE) {
			if ($this->user_m->loggedin() == FALSE) {
				redirect('user/login');
			}
		}



	}
}

