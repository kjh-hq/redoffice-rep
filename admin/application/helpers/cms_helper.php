<?php

error_reporting(0);

function btn_view($perm_button, $url, $id = NULL, $get = "", $level = '<i class="fa fa-eye"></i>', $attributes = array('title' => "", 'class' => "btn btn-default dim")) {

    if ($perm_button[$url] && $id != NULL) {

        $url = str_replace('(:num)', $id, $url);

        return anchor($url, $level, $attributes);
    } else if ($perm_button[$url] && strlen($get) > 0) {

        return anchor($url . $get, $level, $attributes);
    } else if ($perm_button[$url]) {

        return anchor($url, $level, $attributes);
    } else {

        return "";
    }
}

function btn_add($perm_button, $url, $id = NULL, $get = "", $level = '<i class="fa fa-plus"></i>', $attributes = array('title' => "", 'class' => "btn btn-primary dim")) {



    if ($perm_button[$url] && $id != NULL) {

        $url = str_replace('(:num)', $id, $url);

        return anchor($url, $level, $attributes);
    } else if ($perm_button[$url] && strlen($get) > 0) {

        return anchor($url . $get, $level, $attributes);
    } else if ($perm_button[$url]) {

        return anchor($url, $level, $attributes);
    } else {

        return "";
    }
}

function btn_edit($perm_button, $url, $id = NULL, $get = "", $level = '<i class="fa fa-pencil"></i>', $attributes = array('title' => "", 'class' => "btn btn-info dim")) {

    if ($perm_button[$url] && strlen($get) > 0 && $id != NULL) {

        $url = str_replace('(:num)', $id, $url);

        return anchor($url . $get, $level, $attributes);
    } else if ($perm_button[$url] && strlen($get) > 0) {

        return anchor($url . $get, $level, $attributes);
    } else if ($perm_button[$url]) {

        $url = str_replace('(:num)', $id, $url);

        return anchor($url, $level, $attributes);
    } else {

        return "";
    }
}

function btn_delete($perm_button, $url, $id = NULL, $get = "", $level = '<i class="fa fa-remove"></i>', $attributes = array('title' => "", 'class' => "btn btn-warning dim")) {

    $attributes['onclick'] = "return confirm('You are about to delete a record. This cannot be undone. Are you sure?');";

    if ($perm_button[$url] && strlen($get) > 0 && $id != NULL) {

        $url = str_replace('(:num)', $id, $url);

        return anchor($url . $get, $level, $attributes);
    } else if ($perm_button[$url] && strlen($get) > 0) {

        return anchor($url . $get, $level, $attributes);
    } else if ($perm_button[$url]) {

        $url = str_replace('(:num)', $id, $url);

        return anchor($url, $level, $attributes);
    } else {

        return "";
    }
}

function article_link($article) {

    return 'article/' . intval($article->id) . '/' . e($article->slug);
}

function article_links($articles) {

    $string = '<ul>';

    foreach ($articles as $article) {

        $url = article_link($article);

        $string .= '<li>';

        $string .= '<h3>' . anchor($url, e($article->title)) . ' â€º</h3>';

        $string .= '<p class="pubdate">' . e($article->pubdate) . '</p>';

        $string .= '</li>';
    }

    $string .= '</ul>';

    return $string;
}

function get_excerpt($article, $numwords = 50) {

    $string = '';

    $url = article_link($article);

    $string .= '<h2>' . anchor($url, e($article->title)) . '</h2>';

    $string .= '<p>' . e(limit_to_numwords(strip_tags($article->body), $numwords)) . '</p>';

    $string .= '<p>...</p>';

    $string .= '<p>' . anchor($url, 'read more ...', array('title' => e($article->title))) . '</p>';

    $string .= '<p class="pubdate">' . e($article->pubdate) . '</p>';

    return $string;
}

function limit_to_numwords($string, $numwords) {

    $excerpt = explode(' ', $string, $numwords + 1);

    if (count($excerpt) >= $numwords) {

        array_pop($excerpt);
    }

    $excerpt = implode(' ', $excerpt);

    return $excerpt;
}

function e($string) {

    return htmlentities($string);
}

function get_client_ip() {

    $ipaddress = '';

    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');

    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');

    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');

    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');

    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');

    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

function uniqueeight() {

    $characters = 'ACEFHJKMNPRTUVWXY4937' . time();

    $string = '';



    for ($i = 0; $i < 8; $i++) {

        $string .= $characters[rand(0, strlen($characters) - 1)];
    }

    return $string;
}

function get_menu($array, $child = FALSE) {

    $CI = & get_instance();

    $str = '';



    if (count($array)) {

        $str .= $child == FALSE ? '' . PHP_EOL : '' . PHP_EOL;



        foreach ($array as $r) {

            if (isset($r['ap_view_link'])) {

                $active = $CI->uri->uri_string() == $r['ap_view_link'] ? TRUE : FALSE;
            } else {

                $active = false;
            }



            if (isset($r['children']) && count($r['children'])) {



                $str .= $active ? '<li class="active"><a class="parent"> '
                        . $r['ap_fa_icon'] . '<span>'
                        . e($r['ap_name'])
                        . '</span></a><ul>' : '<li><a  class="parent"> '
                        . $r['ap_fa_icon']
                        . ' <span>'
                        . e($r['ap_name'])
                        . '</span> </a><ul>';

                $str .= get_menu($r['children'], TRUE);
            } else {



                $str .= $active ? '<li class="active"><a href="' . site_url(e($r['ap_view_link'])) . '"> ' . ($child ? '' : $r['ap_fa_icon'] ) . '<span>' . e($r['ap_name']) . '</span></a>' : ''
                        . '<li><a href="' . site_url(e($r['ap_view_link'])) . '"> ' . ($child ? '' : $r['ap_fa_icon'] ) . '<span>' . e($r['ap_name']) . '</span></a>                

                    ';
            }





            $str .= '</li>' . PHP_EOL;
        }



        $str .= '</ul>' . PHP_EOL;
    }



    return $str;
}

//validation Mobile Number

function validateMobNumber($number) {

    if ($number) {



        $number = str_replace("+", "", $number);

        $number = str_replace(",", "", $number);

        $number = str_replace("-", "", $number);

        $number = str_replace(" ", "", $number);



        $ck = substr($number, 0, 2);



        if ($ck == "88") {

            $count = strlen($number);

            $number = substr($number, 2, $count);
        }

        if (is_numeric($number)) {

            if (strlen($number) < 11 || strlen($number) > 11) {

                return "error";
            }
        } else {

            return "error";
        }

        return $number;
    }
}

// Message Checker



function ordutf8($string, &$offset) {

    $code = ord(substr($string, $offset, 1));

    if ($code >= 128) {        //otherwise 0xxxxxxx
        if ($code < 224)
            $bytesnumber = 2;                //110xxxxx

        else if ($code < 240)
            $bytesnumber = 3;        //1110xxxx

        else if ($code < 248)
            $bytesnumber = 4;    //11110xxx

        $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);

        for ($i = 2; $i <= $bytesnumber; $i++) {

            $offset++;

            $code2 = ord(substr($string, $offset, 1)) - 128;        //10xxxxxx

            $codetemp = $codetemp * 64 + $code2;
        }

        $code = $codetemp;
    }

    $offset += 1;

    if ($offset >= strlen($string))
        $offset = -1;

    return $code;
}

function MessageCheck($fullMessage) {

    $fullMessageVarified = "";

    $maxSmsLimit = 5;

    $singleTextMsgLength = 160;

    $multiTextMsgLength = 150;

    $singleUnicodeMsgLength = 70;

    $multiUnicodeMsgLength = 65;



    $returnData = Array();

    $length = strlen(utf8_decode($fullMessage));

    $msgType = "Text";

    $smsCount = 0;



    $offset = 0;

    while ($offset >= 0) {

        $cod = ordutf8($fullMessage, $offset) . "<br/>";

        if ($cod == 8216 || $cod == 8217) {

            // fullMessage[i] = '\'';
            // fullMessageVarified += "'";

            $fullMessage = str_replace("‘", "'", $fullMessage);

            $fullMessage = str_replace("’", "'", $fullMessage);

            continue;
        }

        if ($cod == 8220 || $cod == 8221) {

            // fullMessage[i] = '\"';
            // fullMessageVarified += "\"";

            $fullMessage = str_replace('“', '"', $fullMessage);

            $fullMessage = str_replace('”', '"', $fullMessage);

            continue;
        }



        if ($cod > 256) {



            $msgType = "Unicode";

            break;
        }
    }

    if ($msgType == "Text") {



        if ($length <= $singleTextMsgLength) {

            $smsCount = 1;
        } else {

            $smsCount = ceil($length / $multiTextMsgLength);
        }
    } else {



        if ($length <= $singleUnicodeMsgLength) {

            $smsCount = 1;
        } else {

            $smsCount = ceil($length / $multiUnicodeMsgLength);
        }
    }

    $array = array(
        "msgType" => $msgType,
        "message" => $fullMessage,
        "length" => $length,
        "smsCount" => $smsCount
    );

    return $array;
}

function notification_msg() {
    $CI = & get_instance();
    $str = '';
    if (validation_errors()) {
        $str .= '<div class="alert alert-danger" style="text-align:Center"><b>'
                . validation_errors()
                . '</b></div>';
    } else {
        
    }



    if ($CI->session->flashdata('error')) {

        $str .= ' <div class="alert alert-danger" style="text-align:Center">'
                . '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
                . '<b>'
                . $CI->session->flashdata('error')
                . $CI->session->unset_userdata('error')
                . '</b></div>';
    } else if ($CI->session->flashdata('success')) {

        $str .= ' <div class="alert alert-success" style="text-align:Center">'
                . '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
                . '<b>'
                . $CI->session->flashdata('success')
                . $CI->session->unset_userdata('success')
                . '</b></div>';
    } else if ($CI->session->flashdata('notify')) {

        $str .= ' <div class="alert alert-info" style="text-align:Center">'
                . '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
                . '<b>'
                . $CI->session->flashdata('notify')
                . $CI->session->unset_userdata('notify')
                . '</b></div>';
    } else {
        
    }

    if (strlen($str) > 0) {

        return $str;
    } else {

        return null;
    }
}

function file_name_modifier($str) {

    $str = strtolower($str);

    $str = trim($str);

    $str = str_replace("&", "_", $str);

    $str = str_replace(" ", "_", $str);

    return $str;
}

function space($var = 1, $spacer = "&nbsp;") {

    $ret = '';



    if ($var != 0) {

//        $var = 1;

        for ($i = 0; $i < $var; $i++)
            $ret .=$spacer;
    }

    return $ret;
}

function buildChild($symtemMenu, $parent_id = '_subid') {

    $child = array();

    foreach ($symtemMenu as $v) {

        $pt = $v[$parent_id];

        $list = @$child[$pt] ? $child[$pt] : array();

        array_push($list, $v);

        $child[$pt] = $list;
    }



    return $child;
}

function generateMenuArray($id, &$output, &$child, $maxlevel = 10, $level = 0, $previous_level = 0, $level_separator = '===', $include_parent = false, $ref_id = 'sys_menu_id', $menu_path = '', $descendant = '', $ref_name = 'sys_menu_name', $menu_name_path = '') {

    $level_str = '';

    for ($i = 0; $i < $level; $i++)
        $level_str .= $level_separator;





    if ($child[$id] && $level <= $maxlevel) {



        foreach ($child[$id] as $key => $v) {



            $id = $v[$ref_id];

            $name = $v[$ref_name];



            $v['level'] = $level;

            $v['level_str'] = $level_str;

            $v['menu_path'] = $menu_path . "->" . $id;

            $v['descendant'] = $menu_path . "->" . $id;

            if ($menu_name_path) {

                $v['menu_name_path'] = $menu_name_path . " / " . $name;
            } else {

                $v['menu_name_path'] = $name;
            }





            if (count($child[$id])) {

                $v['has_child'] = 1;

                $output[] = $v;
            } else {

                $v['has_child'] = 0;

                $output[] = $v;
            }



            generateMenuArray($id, $output, $child, $maxlevel, $level + 1, $level, $level_separator, $include_parent, $ref_id, $v['menu_path'], $v['descendant'], $ref_name, $v['menu_name_path']);
        }
    }

    return $output;
}

function make_array_man($obj) {

    if ($obj) {

        $result = json_encode($obj);

        return json_decode($result, TRUE);
    }

    return 'Object Parameter Need';
}

/**

 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.

 * @author Joost van Veen

 * @version 1.0

 */
if (!function_exists('dump')) {

    function dump($var, $label = 'Dump', $echo = TRUE) {

        // Store dump in variable

        ob_start();

        var_dump($var);

        $output = ob_get_clean();



        // Add formatting

        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);

        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';



        // Output

        if ($echo == TRUE) {

            echo $output;
        } else {

            return $output;
        }
    }

}





if (!function_exists('dump_exit')) {

    function dump_exit($var, $label = 'Dump', $echo = TRUE) {

        dump($var, $label, $echo);

        exit;
    }

}

