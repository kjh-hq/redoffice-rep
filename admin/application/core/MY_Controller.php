<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class MY_Controller extends CI_Controller {



    function __construct() {

        parent::__construct();

        $this->data ['meta_title'] = 'BYSON WEEBO | v 0.1';

        $this->data ['site_name'] = 'BYSON WEEBO';

        $this->data ['developed_by'] = 'BYSON TECHLOGIES';

        $this->load->helper('form');
        $this->load->helper('string');

        $this->load->library('form_validation');

        $this->load->model('admin_users_m');

        $config['upload_path'] = '../public/img_upload/';
        $config['max_size'] = 102400;
        $config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc';

        $this->load->library('upload', $config);

        $this->data['manage_array'] = "manage";



        if ($this->admin_users_m->auth()) {

            $sql = "SELECT perm.*,ap.* 

		FROM admin_permissions as perm inner join admin_page as ap 

		on perm.perm_page=ap.ap_id 

		WHERE  perm.perm_profile=" . $this->admin_users_m->auth() . " AND perm.perm_view=1 AND ap._is_menu = 1 AND _type=1 order by ap._parent_id asc,ap._sort asc";

            //pr($sql);



            $query = $this->db->query($sql);

            $pages = $query->result_array();



            foreach ($pages as $page) {

                if (!$page['_parent_id']) {

                    // This page has no parent

                    $this->data['ap_menu'][$page['ap_id']] = $page;

                } else {

                    // This is a child page

                    $this->data['ap_menu'][$page['_parent_id']]['children'][$page['ap_id']] = $page;

                }

            }



            $sql = "SELECT perm.*,ap.* 

		FROM admin_permissions as perm inner join admin_page as ap 

		on perm.perm_page=ap.ap_id 

		WHERE  perm.perm_profile=" . $this->admin_users_m->auth() . " order by _sort asc";

            //pr($sql);



            $query = $this->db->query($sql);

            $button = $query->result_array();



            //pr($button);

            $this->data['perm_button'] = array();

            foreach ($button as $data) {

                $this->data['perm_button'][$data['ap_view_slug']] = $data['perm_view'];

                $this->data['perm_button'][$data['ap_add_slug']] = $data['perm_add'];

                $this->data['perm_button'][$data['ap_edit_slug']] = $data['perm_edit'];

                $this->data['perm_button'][$data['ap_delete_slug']] = $data['perm_delete'];

            }

            $this->data['page_title'] = array();

            foreach ($button as $data) {

                if ($data['_tag'] == 'dashboard') {

                    $this->data['page_title']['home'] = $data['ap_view_title'];

                    $this->data['page_title']['home_slug'] = $data['ap_view_slug'];

                }

                $this->data['page_title'][$data['ap_view_slug']] = $data['ap_view_title'];

                $this->data['page_title'][$data['ap_add_slug']] = $data['ap_add_title'];

                $this->data['page_title'][$data['ap_edit_slug']] = $data['ap_edit_title'];

                $this->data['page_title'][$data['ap_delete_slug']] = $data['ap_delete_title'];

            }



     

            //pr($this->data['perm_button']);

        }





// Login check

        $exception_uris = array(

            'user/login',

            'user/logout'

        );

        if (in_array(uri_string(), $exception_uris) == FALSE) {

            if ($this->admin_users_m->loggedin() == FALSE) {

                redirect('user/login');

            }

        }

    }



}

