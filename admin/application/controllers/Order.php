<?php
class Order extends MY_Controller
{
	public function __construct ()
	{
		parent::__construct();
		error_reporting(E_ALL);
		$this->load->model('page_m');
		
		$this->load->model('order_n_m');
		$this->load->model('order_l_m');
		$this->load->model('product_m');
		$this->load->model('product_image_m');
		//$this->load->model('ar_m');
		$this->load->helper(array('form', 'url'));
		//$this->load->library('upload');
		
	}
	public function index ($start=0)
	{
		// Fetch all articles
		$this->data['articles'] = $this->order_n_m->get_post(10,$start);
		$this->load->library('pagination');
		$config['base_url']=site_url().'/order/index/';
		$config['total_rows']=$this->order_n_m->get_post_count();
		$config['per_page']=10;
		$config['full_tag_open'] = '<div><ul class="pagination pagination-small pagination-centered">';
		$config['full_tag_close'] = '</ul></div>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$config['uri_segment'] = 3;
		$config['num_links'] = 10;
	
		$this->pagination->initialize($config);
		$this->data['pages_p']=$this->pagination->create_links();
		$this->data['pages'] = $this->page_m->get();
	
		//$this->data['pages'] = $this->page_m->get_nested();
		// Load view
		$this->data['subview'] = 'order/index';
		$this->load->view('_layout_main', $this->data);
	
	
	}
	public function details ($id = NULL)
	{
		
		$this->data['id']=$id;
		
		// Fetch a article or set a new one
		if ($id) {
			$this->data['article'] = $this->order_n_m->get($id);
			count($this->data['article']) || $this->data['errors'][] = 'article could not be found';
		}
		
		// Load the view
		$this->data['subview'] = 'order/edit';
		$this->load->view('_layout_main', $this->data);
	}
	public function print_order ($id = NULL)
	{
		
		$this->data['id']=$id;
		if($id>100){
			redirect('dashboard/upgrade');
		}
		// Fetch a article or set a new one
		if ($id) {
			$this->data['article'] = $this->order_n_m->get($id);
			count($this->data['article']) || $this->data['errors'][] = 'article could not be found';
		}
		
		// Load the view
		$this->load->view('order/print', $this->data);
	}
	public function status($id)
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');
		$data['status']=$status;
		
		$this->order_n_m->save($data, $id);
		redirect('order/details/'.$id);
	}
	
	
	
	public function delete ($id)
	{
		$order = $this->order_n_m->get($id);
		$order_list = $this->order_l_m->get_order($order->order_id);
		
		foreach($order_list as $order_list){
			$this->order_l_m->delete($order_list->id);
		}
		
		$this->order_n_m->delete($id);
		redirect('order');
	}
}