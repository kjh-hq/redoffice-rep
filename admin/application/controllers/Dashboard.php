<?php
class Dashboard extends MY_Controller {
        function __construct() {
            parent:: __construct();
       error_reporting(E_ALL);
            $this->load->library('upload');
            $this->load->helper(array('form', 'url'));
			$this->load->helper('url');
        	$this->load->model('page_m');
			$this->load->model('slider_m');
			$this->load->model('visitor_m');
			$this->load->model('order_l_m');
			$this->load->model('order_n_m');
			$this->load->model('dashboard_m');
			
        }
        
        public function index(){
        	
			$this->data['order'] = $this->order_n_m->get_post_count(); 
			$this->data['visitor'] = $this->visitor_m->visitor_count(); 
			$this->data['visitor_created'] = $this->visitor_m->get_five(); 
			$this->data['order_created'] = $this->order_n_m->get_five();
			$this->data['total_sale'] = $this->order_n_m->get_total_sale();
        	$this->data['subview']='dashboard/index';
        	
        	$this->load->view('_layout_main', $this->data);
        
        
        }
        public function status(){
				$data = array();
        	switch ($this->input->get("range")) {
        		case 'day':
					$data[0][0]="Day";
					$data[0][1]="Sales";
					$data[0][2]="Customer";
        		  for ($i=1;$i<24;$i++) {
        		 	$data[$i][0] = "".$i;
        		 	$data[$i][1] = $this->dashboard_m->get_day_order($i);
        		 	$data[$i][2] = $this->dashboard_m->get_day_visitor($i);
        		  }
        			break;
        		case 'week':
					$data[0][0]="Week";
					$data[0][1]="Sales";
					$data[0][2]="Customer";
        		  for ($i=1;$i<8;$i++) {
        		 	$data[$i][0] = jddayofweek ($i-2, 2);
        		 	$data[$i][1] = $this->dashboard_m->get_week_order($i);
        		 	$data[$i][2] = $this->dashboard_m->get_week_visitor($i);
        		  }
        			
        			break;
        		case 'month':
        			$data[0][0]="Month";
					$data[0][1]="Sales";
					$data[0][2]="Customer";
        		  for ($i=1;$i<=31;$i++) {
        		 	$data[$i][0] = "".$i;
        		 	$data[$i][1] = $this->dashboard_m->get_month_order($i);
        		 	$data[$i][2] = $this->dashboard_m->get_month_visitor($i);
        		  }
        			break;
        		case 'year':
        			$data[0][0]="Year";
					$data[0][1]="Sales";
					$data[0][2]="Customer";
        		  for ($i=1;$i<13;$i++) {
        		  	$jd=gregoriantojd($i,date('d'),day('y'));
        		 	$data[$i][0] = jdmonthname($jd,2);
        		 	$data[$i][1] = $this->dashboard_m->get_year_order($i);
        		 	$data[$i][2] = $this->dashboard_m->get_year_visitor($i);
        		  }
        			break;
        		
        		default:
        			# code...
        			break;
        	}
				
        	echo json_encode($data);
        	//$this->load->view('cnb_modal',$this->data);
        }
        
        
   public function brand(){
   $this->load->model('brand_m');
        	$this->data['brand'] = $this->brand_m->get();
        	$this->data['subview']='brand';
        	
        	$this->load->view('_layout_main', $this->data);
        }  
        
     public function info(){
		$this->load->model('dashboard_m');
        	$this->data['dashboard'] = $this->dashboard_m->get(1);
		
        	$this->data['subview']='info/index';
        	$this->load->view('_layout_main', $this->data);
        }
        
		
		  public function info_edit(){
		$this->load->model('dashboard_m');
        	$this->data['dashboard'] = $this->dashboard_m->get(1);
			
			$rules = $this->dashboard_m->rules;
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->dashboard_m->array_from_post(array(
				'phone',  
				'email',  
				
			));
			
			$this->dashboard_m->save($data, 1);
			
			redirect('dashboard/info');
		}
		
        	$this->data['subview']='info/info';
        	$this->load->view('_layout_main', $this->data);
        }
		
		
			  public function design(){
			$this->load->model('dashboard_m');
        	$this->data['dashboard'] = $this->dashboard_m->get(1);
			
			$rules = $this->dashboard_m->rules;
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->dashboard_m->array_from_post(array(
				'background',  
				'header',  
				'buy_now',  
				'buy_now_hover',  
				'buy_now_hover_color',  
				
			));
			
			$this->dashboard_m->save($data, 1);
			
			redirect('dashboard/design');
		}
		
        	$this->data['subview']='color/color';
        	$this->load->view('_layout_main', $this->data);
        }
		
		
		 public function customer(){
        	
			$this->data['customer'] = $this->visitor_m->get(); 
			
        	$this->data['subview']='customer/index';
        	
        	$this->load->view('_layout_main', $this->data);
        
        
        }
		
		public function details($id){
        	
			$this->data['customer'] = $this->visitor_m->get($id); 
			
        	$this->data['subview']='customer/details';
        	
        	$this->load->view('_layout_main', $this->data);
        
        
        }
		
			public function lo($id){
        	
			$this->data['dashboard'] = $this->dashboard_m->get(1);
			$rules = $this->dashboard_m->rules;
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->dashboard_m->array_from_post(array(
				'add1_link',  
				'add2_link',  
				'add3_link',  				
			));
			
			$this->dashboard_m->save($data, 1);
			
			redirect('dashboard/lo');
		}
        	$this->data['subview']='dashboard/lo';
        	
        	$this->load->view('_layout_main', $this->data);
        
        
        }
        public function upgrade(){
		$this->load->view('upgrade');
	}
       
    }
