<?php
class Slider extends MY_Controller
{
	public function __construct ()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->model('slider_m');
		$this->data['_primary_key'] = $this->slider_m->get_primary_key();
        	$this->data['_primary_name'] = $this->slider_m->get_primary_name();
	}
	public function index ()
	{
		
		$this->data[$this->data['manage_array']][strtolower(get_class($this))] = $this->slider_m->get(null,false,true);
			if(count($_POST['_sort'])){
				foreach ($_POST['_sort'] as $key => $value) {
					$image_sort['_sort']=$value;
					$this->slider_m->save($image_sort,$key);
				}
			}
			$this->data['subview'] = strtolower(get_class($this)) .'/index';
			$this->load->view('_layout_crud', $this->data);
	}
	public function edit($id=NULL){
	// Fetch a slider or set a new one
		if ($id) {
			$this->data[$this->data['manage_array']][strtolower(get_class($this))] = $this->slider_m->get($id, FALSE, TRUE);
			count($this->data[$this->data['manage_array']][strtolower(get_class($this))]) || $this->data['errors'][] = ucwords(get_class($this)) . ' could not be found';
		}
		else {
			$this->data[$this->data['manage_array']][strtolower(get_class($this))] = $this->slider_m->get_new(TRUE);
		}
// sliders for dropdown
// Set up the form
		$rules = $this->slider_m->rules;
		$this->form_validation->set_rules($rules);
// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->slider_m->array_from_post(array(
				'title1',
				'title2',
				));
			if (!$this->upload->do_upload())
			{
				//$this->upload->display_errors();
			}
			else
			{
				$upload_data = $this->upload->data();
				$data['image'] = $upload_data['file_name'];
			}
			$this->slider_m->save($data, $id);
			    if ($this->input->post('submit') == "Save and Continue") {
                redirect('' . strtolower(get_class($this)) . '/edit');
            } else if ($this->input->post('submit') == "Update") {
                redirect('' . strtolower(get_class($this)) . '/edit/' . $id);
            } else if ($this->input->post('submit') == "Save") {
                redirect('' . strtolower(get_class($this)) );
            } else {
                redirect('' . strtolower(get_class($this)) );
            }
		}
		$this->data['subview'] = strtolower(get_class($this)) . '/edit';
		$this->load->view('_layout_crud', $this->data);
	}
		public function delete ()
		{
			$id = $this->input->get('id');
			$image = $this->input->get('image');
			$this->load->model('slider_m');
			unlink('public/img_upload/'.$image);
			$this->slider_m->delete($id);
			redirect('slider');
		}
	}