<?php
class Product extends MY_Controller
{
	public function __construct ()
	{
		parent::__construct();
		$this->load->helper ('form');
		$this->load->library ('form_validation' );
		$this->load->model('page_m');
		$this->load->model('product_m');
		$this->load->model('product_image_m');
		$this->load->model('product_file_m');
		$this->load->model('page_layout_m');
		$this->load->helper(array('form', 'url'));
		$config['upload_path'] = '../public/img_upload/';
	        $config['max_size'] = 102400;
	        $config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc';
	        $this->load->library('upload', $config);
		$this->data['_primary_key'] = $this->product_m->get_primary_key();
        	$this->data['_primary_name'] = $this->product_m->get_primary_name();
	}
	public function index ()
	{
		$start = $this->input->get('per_page');
		$filter = "?target=filter";
		$where = array();
		$like = array();
		if($this->input->get('id')){
			if($this->input->get('id')=="desc"){
				$filter .="&id=desc";
			}else{
				$filter .="&id=asc";
			}
		}
		if($this->input->get('filter_name')){
			if(is_string($this->input->get('filter_name'))){
				$filter .="&filter_name=".$this->input->get('filter_name');
				$like['name']  = $this->input->get('filter_name'); 
			}else{
				$filter .="&filter_name=";
			}
		}
		if($this->input->get('filter_category')){
			if(is_numeric($this->input->get('filter_category'))){
				$filter .="&filter_category=".$this->input->get('filter_category');
				$where['_parent_id']  = $this->input->get('filter_category'); 
			}else{
				$filter .="&filter_category=";
			}
		}
		if($this->input->get('filter_status')!=NULL){
			if(is_numeric($this->input->get('filter_status')) && $this->input->get('filter_status')<2 && $this->input->get('filter_status') <=0){
				$filter .="&filter_status=".$this->input->get('filter_status');
				$where['_status']  = $this->input->get('filter_status'); 
			}else{
				$filter .="&filter_status=2";
			}
		}
		$this->data['pages_with_parent'] = $this->page_m->get_all_selectable();
		$total_rows = $this->product_m->get_post_count($where,$like);
		$this->session->set_flashdata('notify', 'Total '.$total_rows.' products filtered');
		// Fetch all products
		$this->data['products'] = $this->product_m->get_post(10,$start,$where,$like);
		$this->load->library('pagination');
		$config['base_url']=site_url().'/product/index'.$filter;
		$config['total_rows']=$total_rows;
		$config['page_query_string'] = TRUE;
		$config['per_page']=10;
		$config['full_tag_open'] = '<div><ul class="pagination pagination-small pagination-centered">';
		$config['full_tag_close'] = '</ul></div>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;
		$this->pagination->initialize($config);
		$this->data['pages_p']=$this->pagination->create_links();
		$this->data['filter']=$filter;
		
// Load view
		$this->data['subview'] = 'product/index';
		$this->load->view('_layout_main', $this->data);
	}
	public function edit ($id = NULL)
	{
// Fetch a product or set a new one
		if ($id) {
			$this->data[$this->data['manage_array']][strtolower(get_class($this))] = $this->product_m->get($id, FALSE, TRUE);
			count($this->data[$this->data['manage_array']][strtolower(get_class($this))]) || $this->data['errors'][] = ucwords(get_class($this)) . ' could not be found';
		}else{
			$this->data[$this->data['manage_array']][strtolower(get_class($this))] = make_array_man($this->product_m->get_new());
		}
		$this ->data['no_parents'] = $this->page_m->get_no_parents();
		$this ->data['all_products'] = $this->product_m->get();
		$this->data['pages_with_parent'] = $this->page_m->get_all_selectable();
// Set up the form
		$rules = $this->product_m->rules;
		$this->form_validation->set_rules($rules);
// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->product_m->array_from_post(array( 
				'_parent_id',  
				'name',  	
				'sales_type',
				'sale',
				'product_features',
				'technical_specifications',
				'shortdesc',
				'product_feature_short',
				'menu_featured',
				'menu_sales',
				));
			if($id==NULL){
			$data['publisher']=$this->session->userdata('id');
			}
			$this->data['category'] = $this->page_m->get($data['_parent_id']);
			if($this->data['category']->_parent_id==0){
			$data['_manufacturer'] = $this->data['category']->id;
			}else{
				$parent = $this->page_m->get($this->data['category']->_parent_id);
				$data['_manufacturer'] =$parent->id;
			}
			if(empty($this->data[$this->data['manage_array']][strtolower(get_class($this))]['_link'])){
			$data['_link'] = url_title(strtolower($this->_unique_link (((!empty($data['_link']))?$data['_link']:url_title($data['name'])),$id)));
			}
			$data['_related_to'] = json_encode($this->input->post('_related_to'));
			$id=$this->product_m->save($data, $id);
			$slug['_slug'] = $this->page_layout_m->get_slug($this->page_layout_m->get_id_by_tag('product_details'),$id);
			$this->product_m->save($slug, $id);
			if(!empty($_POST['_sort'])){
				foreach ($_POST['_sort'] as $key => $value) {
					$image_sort['_sort']=$value;
					$this->product_image_m->save($image_sort,$key);
				}
			}
			if(!empty($_POST['_dsort'])){
				foreach ($_POST['_dsort'] as $key => $value) {
					$file_data['_sort']=$value;
					$file_data['pfile_name'] = $_POST['_dpfile_name'][$key];
					$this->product_file_m->save($file_data,$key);
				}
			}
			$_dnsort = empty($_POST['_dnsort'])==FALSE?$_POST['_dnsort']:[];
			$_dnpfile_name = empty($_POST['_dnpfile_name'])==FALSE?$_POST['_dnpfile_name']:[];
			$d_files = empty($_FILES['downloadfile'])==FALSE?$_FILES['downloadfile']:[];
			$img_files = empty($_FILES['userfile'])==FALSE?$_FILES['userfile']:[];
			if(!empty($img_files)){
			$cpt = count($img_files['name']);
			}else{
			$cpt=0;
			}
			for($i=0; $i<$cpt; $i++)
			{           
				$_FILES['userfile']['name']= $img_files['name'][$i];
				$_FILES['userfile']['type']= $img_files['type'][$i];
				$_FILES['userfile']['tmp_name']= $img_files['tmp_name'][$i];
				$_FILES['userfile']['error']= $img_files['error'][$i];
				$_FILES['userfile']['size']= $img_files['size'][$i];    
				if (!$this->upload->do_upload())
				{
					//pr($this->upload->display_errors());
				}
				else
				{
					$upload_data = $this->upload->data();
					$image_data = array();
					$image_data['image'] = $upload_data['file_name'];
					$image_data['product_id'] = $id;
					$this->product_image_m->save($image_data);
					$this->load->library('image_lib');
					$this->resize600($upload_data['full_path'],$upload_data['file_name']);
					$this->resize400($upload_data['full_path'],$upload_data['file_name']);
					$this->resize200($upload_data['full_path'],$upload_data['file_name']);
					//die($upload_data['full_path'].$upload_data['file_name']);
				}
			}
			if(!empty($d_files)){
			$cpt = count($d_files['name']);
			}else{
			$cpt=0;
			}
			for($i=0; $i<$cpt; $i++)
			{           
				$_FILES['userfile']['name']= $d_files['name'][$i];
				$_FILES['userfile']['type']= $d_files['type'][$i];
				$_FILES['userfile']['tmp_name']= $d_files['tmp_name'][$i];
				$_FILES['userfile']['error']= $d_files['error'][$i];
				$_FILES['userfile']['size']= $d_files['size'][$i];    
				if (!$this->upload->do_upload())
				{
					//pr($this->upload->display_errors());
				}
				else
				{
					$upload_data = $this->upload->data();
					$image_data = array();
					$image_data['pfile_image'] = 'file_'.str_replace('.', '', $upload_data['file_ext']).'.png';
					$image_data['pfile_filename'] = $upload_data['file_name'];
					$image_data['pfile_size'] = $upload_data['file_size'];
					$image_data['pfile_ext'] = $upload_data['file_ext'];
					$image_data['pfile_type'] = 'downloadfile';
					$image_data['product_id'] = $id;
					$image_data['_sort'] = $_dnsort[$i];
					$image_data['pfile_name'] = $_dnpfile_name[$i];
					$this->product_file_m->save($image_data);
				}
			}
			if($this->input->get('rdirBack')){
				$rdirBack = str_replace("&", ".and.", $this->input->get('rdirBack'));
				 }else if($this->input->post('rdirBack')){
				 	$rdirBack = str_replace("&", ".and.", $this->input->post('rdirBack'));
				 }else{
				 	$rdirBack ="";
				 }
			$this->session->set_flashdata('success', 'Data Saved Succesfully');
			redirect('product/edit/'.$id.'?rdirBack='.$rdirBack);
		}
// Load the view
		$this->data['subview'] = 'product/edit';
		$this->load->view('_layout_main', $this->data);
	}
	public function delete ($id)
	{
		$id = $this->input->get('id');
		$image = $this->input->get('image');
		$image_s = $this->input->get('image_s');
		$image_m = $this->input->get('image_m');
		$image_f = $this->input->get('image_f');
		//$this->load->model('slider_m');
		// unlink('../public/img_upload/'.$image);
		// unlink('../public/img_upload/'.$image_s);
		// unlink('../public/img_upload/'.$image_m);
		// unlink('../public/img_upload/'.$image_f);
		$this->session->set_flashdata('success', 'Record Deleted Succesfully');
		$this->product_m->delete($id);
		redirect('product');
	}
	public function deleteImg ($id)
	{
		$img = $this->product_image_m->get($id);
		unlink('../public/img_upload/'.$img->image);
		unlink('../public/img_upload/large/'.$img->image);
		unlink('../public/img_upload/medium/'.$img->image);
		unlink('../public/img_upload/small/'.$img->image);
		$this->product_image_m->delete($id);
		redirect($this->input->get('rdirBack'));
	}
		public function deletefile ($id)
	{
		$img = $this->product_file_m->get($id);
		unlink('../public/img_upload/'.$img->pfile_filename);
		$this->product_file_m->delete($id);
		redirect($this->input->get('rdirBack'));
	}
	public function status($id){
		$this->product_m->status($id);
		redirect($this->input->get('rdirBack'));
	}
	public function converter()
	{
		$this->load->view('converter', $this->data);
	}
	function resize600($path,$file){
		$config['image_library']   = 'gd2';
		$config['source_image']    = $path;
		$config['create_thumb']    = FALSE;
		$config['maintain_ratio']  = TRUE;
		$config['width']           = 650;
		$config['height']          = 480;
		$config['new_image']   = '../public/img_upload/large/'.$file;
		
		$this->image_lib->initialize($config);
		if ( ! $this->image_lib->resize())
		{
			//var_dump($config);
		    //die($this->image_lib->display_errors());
		}
		$this->image_lib->clear();
	}
		function resize400($path,$file){
		$config['image_library']   = 'gd2';
		$config['source_image']    = $path;
		$config['create_thumb']    = FALSE;
		$config['maintain_ratio']  = TRUE;
		$config['width']           = 500;
		$config['height']          = 500;
		$config['new_image']   = '../public/img_upload/medium/'.$file;
		
		$this->image_lib->initialize($config);
		if ( ! $this->image_lib->resize())
		{
			//var_dump($config);
		    //die($this->image_lib->display_errors());
		}
		$this->image_lib->clear();
	}
		function resize200($path,$file){
		$config['image_library']   = 'gd2';
		$config['source_image']    = $path;
		$config['create_thumb']    = FALSE;
		$config['maintain_ratio']  = TRUE;
		$config['width']           = 400;
		$config['height']          = 400;
		$config['new_image']   = '../public/img_upload/small/'.$file;
		
		$this->image_lib->initialize($config);
		if ( ! $this->image_lib->resize())
		{
			//var_dump($config);
		    //die($this->image_lib->display_errors());
		}
		$this->image_lib->clear();
	}
		public function _unique_link ($str,$id=NULL)
	{
		$this->db->where('_link', $str);
		if(!empty($id)){
			$this->db->where('id !=', $id);
		}
		$page = $this->product_m->get();
		if (count($page)) {
			$str=$this->_unique_link(increment_string($str));
		}
		return $str;
	}
}