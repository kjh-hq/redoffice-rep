<?php
class News extends MY_Controller
{
	public function __construct ()
	{
		parent::__construct();
		$this->load->model('slider_m');
		$this->load->helper(array('form', 'url'));
		$this->load->library('upload');
		
	}
	public function index ($start=0)
	{
		// Fetch all articles
		$this->data['slider4'] = $this->slider_m->get_post(10,$start,4);
		$this->load->library('pagination');
		$config['base_url']=site_url().'/admin/news/index/';
		$config['total_rows']=$this->slider_m->get_post_count();
		$config['per_page']=10;
		$config['full_tag_open'] = '<div><ul class="pagination pagination-small pagination-centered">';
		$config['full_tag_close'] = '</ul></div>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;
		$this->data['slider5'] = $this->slider_m->get();
		$this->pagination->initialize($config);
		$this->data['pages_p']=$this->pagination->create_links();
		// Load view
		$this->data['subview'] = 'admin/news/index';
		$this->load->view('admin/_layout_main', $this->data);
	
	
	}
	public function edit ($id = NULL)
	{
		
		
		
		// Fetch a article or set a new one
		if ($id) {
			$this->data['slider4'] = $this->slider_m->get($id);
			count($this->data['slider4']) || $this->data['errors'][] = 'news could not be found';
		}else{
			$this->data['slider4'] = $this->slider_m->get_new();
			
		}
		
		// Set up the form
		$rules = $this->slider_m->rules;
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->slider_m->array_from_post(array(
				'title',
				'body', 
				
			));
			session_start();
			$_SESSION['myValue']=$data['title'];
			
			$this->slider_m->save($data, $id);
			$_SESSION['id']=$id;
			redirect('upload3/upload');
		}
		
		// Load the view
		$this->data['subview'] = 'admin/news/edit';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	
	
	public function delete ($id)
	{
		$id = $this->input->get('id');
		$image = $this->input->get('image');
		
		$this->load->model('slider_m');
		unlink('public/img_upload/'.$image);
		$this->slider_m->delete($id);
		redirect('admin/news');
	}
}