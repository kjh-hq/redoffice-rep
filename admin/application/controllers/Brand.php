<?php
class Brand extends MY_Controller
{
	public function __construct ()
	{
		parent::__construct();
		$this->load->library('upload');
		
	}
	public function index ()
	{
	
	}
	
	public function delete ($id)
	{
		$id = $this->input->get('id');
		$image = $this->input->get('image');
		
		$this->load->model('brand_m');
		unlink('public/img_upload/'.$image);
		$this->brand_m->delete($id);
		redirect('admin/dashboard/brand');
	}
	
}