<?php
class Permission extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('admin_page_m');
        $this->load->model('admin_permissions_m');
        $this->load->model('admin_permissions_profile_m');
        $this->load->library('form_validation');
    }
    public function index() {
        $this->data['admin_page'] = $this->admin_page_m->get();
        $this->data['admin_permissions'] = $this->admin_permissions_m->get();
        $this->data['total_perm'] = $this->admin_permissions_m->get_perm_count();
        $this->data['admin_permissions_profile'] = $this->admin_permissions_profile_m->get();
        $this->data['subview'] = 'permission/index';
        $this->load->view('_layout_main', $this->data);
    }
    public function permission_profile() {
        $this->data['admin_page'] = $this->admin_page_m->get();
        $this->data['admin_permissions'] = $this->admin_permissions_m->get();
        $this->data['total_perm'] = $this->admin_permissions_m->get_perm_count();
        $this->data['admin_permissions_profile'] = $this->admin_permissions_profile_m->get();
        $this->data['subview'] = 'permission/permission_profile';
        $this->load->view('_layout_main', $this->data);
    }
    public function save() {
        //$controller = $this->input->post('controller');
        $ajaxMode = $this->input->get('ajaxMode');
        $id_tab = $this->input->get('id_tab');
        $id_profile = $this->input->get('id_profile');
        $perm = $this->input->get('perm');
        $enabled = $this->input->get('enabled');
        $submitAddAccess = $this->input->get('submitAddAccess');
        $action = $this->input->get('action');
        $ajax = $this->input->get('ajax');
      
        $where = "";
        if ($perm == "all") {
            if ($id_tab == -1) {
                $this->db->query("UPDATE `admin_permissions` SET 
					`perm_view` = '$enabled' ,
					`perm_add` = '$enabled' ,
					`perm_edit` = '$enabled' ,
					`perm_delete` = '$enabled', 
					`perm_all` = '$enabled' 
					WHERE `perm_profile` =$id_profile;");
                echo 'ok';
                die();
            }
            $data['perm_view'] = $enabled;
            $data['perm_add'] = $enabled;
            $data['perm_edit'] = $enabled;
            $data['perm_delete'] = $enabled;
        } elseif ($id_tab == -1) {
            $this->db->query("UPDATE `admin_permissions` SET 
				`perm_$perm` = '$enabled' 
				WHERE `perm_profile` =$id_profile;");
            echo 'ok';
            die();
        }
        $data['perm_' . $perm] = $enabled;
        if ($this->admin_permissions_m->save($data, $id_tab, $where)) {
            echo 'ok';
            die();
        } else {
            echo "error";
            die();
        }
    }
    public function edit($id = NULL) {
        // Fetch a article or set a new one
        if ($id) {
            $this->data['admin_permissions_profile'] = $this->admin_permissions_profile_m->get($id);
            count($this->data['admin_permissions_profile']) || $this->data['errors'][] = 'article could not be found';
        } else {
            $this->data['admin_permissions_profile'] = $this->admin_permissions_profile_m->get_new();
        }
        // Set up the form
        $rules = $this->admin_permissions_profile_m->rules;
        $this->form_validation->set_rules($rules);
        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->admin_permissions_profile_m->array_from_post(array(
                'prof_name',
                '_sort',
            ));
            if ($id) {
                $this->admin_permissions_profile_m->save($data, $id);
            } else {
                $id = $this->admin_permissions_profile_m->save($data);
                $perm_page = $this->admin_page_m->get();
                foreach ($perm_page as $perm_page) {
                    $permission['perm_page'] = $perm_page->ap_id;
                    $permission['perm_profile'] = $id;
                    $this->admin_permissions_m->save($permission);
                }
            }
            redirect('permission');
        }
        // Load the view
        $this->data['subview'] = 'permission/edit';
        $this->load->view('_layout_main', $this->data);
    }
    public function sync($id) {
        // Fetch a article or set a new one
        if ($id) {
            $this->data['admin_permissions_profile'] = $this->admin_permissions_profile_m->get($id);
        } else {
            die("Error");
        }
        $perm_missed_page = $this->admin_page_m->perm_missed_page($id);
        foreach ($perm_missed_page as $perm_missed_page) {
            $permission['perm_page'] = $perm_missed_page->ap_id;
            $permission['perm_profile'] = $id;
            $this->admin_permissions_m->save($permission);
        }
        
          $perm_extra_page = $this->admin_page_m->perm_extra_page($id);
        foreach ($perm_extra_page as $perm_extra_page) {
            $this->admin_permissions_m->delete($perm_extra_page->perm_id);
        }
        
        
        redirect('permission/permission_profile');
    }
    public function delete($id) {
        $this->admin_permissions_profile_m->delete($id);
        $this->admin_permissions_m->delete_perm($id);
        redirect('permission');
    }
    public function error() {
        $this->load->view('permission/error_permission', $this->data);
    }
}
