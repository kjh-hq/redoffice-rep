<?php
class Banner extends MY_Controller
{
public function __construct ()
{
parent::__construct();
$this->load->library('upload');
$this->load->model('banner_m');
}
public function index ()
{
$this->data['banner'] = $this->banner_m->get();
$rules = $this->banner_m->rules;
$this->form_validation->set_rules($rules);
if ($this->form_validation->run() == TRUE) {
$data = $this->banner_m->array_from_post(array(
'name',
'link',
'id',
));
if (!$this->upload->do_upload())
{
//$this->upload->display_errors();
}
else
{
$upload_data = $this->upload->data();
$data['image'] = $upload_data['file_name'];
$this->load->library('image_lib');
$this->resize600($upload_data['full_path'],$upload_data['file_name']);
$this->resize400($upload_data['full_path'],$upload_data['file_name']);
$this->resize200($upload_data['full_path'],$upload_data['file_name']);
}
$this->banner_m->save($data, $data['id']);
redirect('banner');
}
$this->data['subview']='banner/index';
$this->load->view('_layout_main', $this->data);
}
function resize600($path,$file){
		$config['image_library']   = 'gd2';
		$config['source_image']    = $path;
		$config['create_thumb']    = FALSE;
		$config['maintain_ratio']  = TRUE;
		$config['width']           = 600;
		$config['height']          = 600;
		$config['new_image']   = '../public/img_upload/large/'.$file;
		
		$this->image_lib->initialize($config);
		if ( ! $this->image_lib->resize())
		{
			//var_dump($config);
		    //die($this->image_lib->display_errors());
		}
		$this->image_lib->clear();
	}
		function resize400($path,$file){
		$config['image_library']   = 'gd2';
		$config['source_image']    = $path;
		$config['create_thumb']    = FALSE;
		$config['maintain_ratio']  = TRUE;
		$config['width']           = 400;
		$config['height']          = 400;
		$config['new_image']   = '../public/img_upload/medium/'.$file;
		
		$this->image_lib->initialize($config);
		if ( ! $this->image_lib->resize())
		{
			//var_dump($config);
		    //die($this->image_lib->display_errors());
		}
		$this->image_lib->clear();
	}
		function resize200($path,$file){
		$config['image_library']   = 'gd2';
		$config['source_image']    = $path;
		$config['create_thumb']    = FALSE;
		$config['maintain_ratio']  = TRUE;
		$config['width']           = 200;
		$config['height']          = 200;
		$config['new_image']   = '../public/img_upload/small/'.$file;
		
		$this->image_lib->initialize($config);
		if ( ! $this->image_lib->resize())
		{
			//var_dump($config);
		    //die($this->image_lib->display_errors());
		}
		$this->image_lib->clear();
	}
}