<?php 
class Other extends MY_Controller{
	public function __construct ()
	{
		parent::__construct();
	$this->load->model('dashboard_m');
	
	}
	
	
	function index()
	{
		$this->data['subview'] = 'admin/other_index';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	
	function about()
	{
		$dashboard = $this->dashboard_m->get_about();
		$this->data['dashboard']=$dashboard->about;
		$this->data['table']='about';
		$rules = $this->dashboard_m->rules;
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->dashboard_m->array_from_post(array(
				'about',  
			
			));
			$this->dashboard_m->save($data, 1);
			redirect('admin/other');
			
		}
		
		
		$this->data['subview'] = 'admin/other';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	function customer()
	{
		$dashboard = $this->dashboard_m->get_customer();
		$this->data['dashboard']=$dashboard->customer;
		$this->data['table']='customer';
		
		$rules = $this->dashboard_m->rules;
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->dashboard_m->array_from_post(array(
				'customer',  
			
			));
			$this->dashboard_m->save($data, 1);
			redirect('admin/other');
			
		}
		
		$this->data['subview'] = 'admin/other';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	function return_policy()
	{
		$dashboard = $this->dashboard_m->get_return_policy();
		$this->data['dashboard']=$dashboard->return_policy;
		$this->data['table']='return_policy';
		
		$rules = $this->dashboard_m->rules;
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->dashboard_m->array_from_post(array(
				'return_policy',  
			
			));
			$this->dashboard_m->save($data, 1);
			redirect('admin/other');
			
		}
		
		$this->data['subview'] = 'admin/other';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	function how()
	{
		$dashboard = $this->dashboard_m->get_how();
		$this->data['dashboard']=$dashboard->how;
		$this->data['table']='how';
		
		
		$rules = $this->dashboard_m->rules;
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->dashboard_m->array_from_post(array(
				'how',  
			
			));
			$this->dashboard_m->save($data,1);
			redirect('admin/other');
			
		}
		
		
		$this->data['subview'] = 'admin/other';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	function faq($id)
	{
		$dashboard = $this->dashboard_m->get_faq();
		$this->data['dashboard']=$dashboard->faq;
		$this->data['table']='faq';
		
		$rules = $this->dashboard_m->rules;
		$this->form_validation->set_rules($rules);
	
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->dashboard_m->array_from_post(array(
				'faq',  
			
			));
			$this->dashboard_m->save($data,1);
			redirect('admin/other');
		}
		
		$this->data['subview'] = 'admin/other';
		$this->load->view('admin/_layout_main', $this->data);
	}
	
	
}
?>