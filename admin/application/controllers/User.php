<?php
class User extends MY_Controller {
    public function __construct() {
        parent::__construct();
        //error_reporting(E_ALL);
        $this->load->helper('form');
        $this->load->helper('captcha');
        $this->load->library('form_validation');
        $this->load->model('admin_users_m');
    }
    public function index() {
        // Fetch all users
        $this->data['users'] = $this->admin_users_m->get();
        // Load view
        $this->data['subview'] = 'user/index';
        $this->load->view('_layout_main', $this->data);
    }
    public function edit() {
        $id = $this->session->userdata('id');
        // Fetch a user or set a new one
        if ($id) {
            $this->data['user'] = $this->admin_users_m->get($id);
            count($this->data['user']) || $this->data['errors'][] = 'User could not be found';
        } else {
            $this->data['user'] = $this->user_m->get_new();
        }
        // Set up the form
        $rules = $this->admin_users_m->rules_profile;
        $id || $rules['email']['rules'] .= '|callback__unique_email';
        $this->form_validation->set_rules($rules);
        // Process the form
        if ($this->form_validation->run() == TRUE) {
            $data = $this->admin_users_m->array_from_post(array('admin_name', 'admin_email', 'admin_recover_mobile_no', 'admin_address', 'admin_country_id'));
            if (strlen($data['admin_name']) > 1) {
                $this->session->set_userdata('name', $data['admin_name']);
            } else {
                $this->session->set_userdata('name', '');
            }
            $this->admin_users_m->save($data, $id);
            redirect('user/edit');
        }
        // Load the view
        $this->data['subview'] = 'user/edit';
        $this->load->view('_layout_main', $this->data);
    }
    public function delete($id) {
        $this->user_m->delete($id);
        redirect('user');
    }
    public function login() {
        // Redirect a user if he's already logged in
        $dashboard = site_url();
        $this->admin_users_m->loggedin() == FALSE || redirect($dashboard);
        // Set form
        $rules = $this->admin_users_m->rules;
        $this->form_validation->set_rules($rules);
        // Process form
        if ($this->form_validation->run() == TRUE) {
            // We can login and redirect
            if ($this->admin_users_m->login() == TRUE) {
                redirect($dashboard);
            } else {
                $this->session->set_flashdata('error', 'That email/password combination does not exist');
                redirect('user/login', 'refresh');
            }
        }
        // Load captcha
        $str = uniqueeight();
        $this->session->unset_userdata("captcha_data");
        $this->session->set_userdata("captcha_data", $str);
        $vals = array(
            'word' => $str,
            'img_path' => './captcha/',
            'img_url' => base_urL() . 'captcha/',
            'font_path' => base_urL() . 'assets/fonts/open-sans/OpenSans-Bold.ttf',
            'img_width' => 200,
            'img_height' => 50,
            'expiration' => 5,
            'word_length' => 8,
            'font_size' => 14,
            // White background and border, black text and red grid
            'colors' => array(
                'background' => array(200, 200, 200),
                'border' => array(235, 235, 235),
                'text' => array(2, 2, 2),
                'grid' => array(225, 100, 40)
            )
        );
        $this->data['cap'] = create_captcha($vals);
        // Load view
        $this->load->view('user/login', $this->data);
    }
    public function logout() {
        $this->admin_users_m->logout();
        redirect('user/login');
    }
    public function _unique_email($str) {
        // Do NOT validate if email already exists
        // UNLESS it's the email for the current user
        $id = $this->uri->segment(4);
        $this->db->where('email', $str);
        !$id || $this->db->where('id !=', $id);
        $user = $this->user_m->get();
        if (count($user)) {
            $this->form_validation->set_message('_unique_email', '%s should be unique');
            return FALSE;
        }
        return TRUE;
    }
    public function _unique_mobile($str) {
        // Do NOT validate if email already exists
        // UNLESS it's the email for the current user
        if ($this->session->userdata('id')) {
            $this->db->where('admin_id !=', $this->session->userdata('id'));
        }
        $where = "( admin_mobile_no = " . $str . " OR admin_recover_mobile_no = " . $str . " )";
        $this->db->where($where);
        $user = $this->admin_users_m->get();
        if (count($user)) {
            $this->form_validation->set_message('_unique_mobile', '%s is already registered');
            return FALSE;
        }
        return TRUE;
    }
    public function _capthca_matches($str) {
        if ($this->session->userdata("captcha_data") != NULL) {
            if ($str == $this->session->userdata("captcha_data")) {
                return true;
            }
        } else {
            $this->form_validation->set_message('_capthca_matches', '%s is required');
            return FALSE;
        }
        $this->form_validation->set_message('_capthca_matches', '%s doesn\'t matches.Try Again');
        return false;
    }
}
