<?php
class Page extends MY_Controller
{
	public function __construct ()
	{
		parent::__construct();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'session' );
		$this->load->model ( 'user_m' );
		$this->load->model ('product_m');
		$this->load->helper('string');
		$this->load->model('page_layout_m');
		$this->load->model('page_m');
		$this->data['_primary_key'] = $this->page_m->get_primary_key();
        $this->data['_primary_name'] = $this->page_m->get_primary_name();
		
	}
	public function index ()
	{
// Fetch all pages
		$this->data[$this->data['manage_array']][strtolower(get_class($this))] = $this->page_m->get_menus_by_pos();
// Load view
		$this->data['subview'] = strtolower(get_class($this)) .'/index';
		$this->load->view('_layout_crud', $this->data);
	}
	public function edit ($id = NULL)
	{
// Fetch a page or set a new one
		 if ($id) {
            $this->data[$this->data['manage_array']][strtolower(get_class($this))] = $this->page_m->get($id, FALSE, TRUE);
            count($this->data[$this->data['manage_array']][strtolower(get_class($this))]) || $this->data['errors'][] = ucwords(get_class($this)) . ' could not be found';
        } else {
            $this->data[$this->data['manage_array']][strtolower(get_class($this))] = $this->page_m->get_new(TRUE);
        }
// Pages for dropdown
		$this->data['all_menu'] = $this->page_m->get_dropdown($id);
		$this->data['_level'] = $this->page_m->get_level($id);
		$this->data['layout'] = $this->page_layout_m->get_dropdown();
// Set up the form
		$rules = $this->page_m->rules;
		$this->form_validation->set_rules($rules);
// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->page_m->array_from_post(array(
				'_parent_id',  
				'title',  
				'short_description',  
				'description',  
				'_link',  
				'_layout',
				'topic',
				'fullwidthclass',
				));
			
			$data['_link'] = url_title(strtolower($this->_unique_link (((!empty($data['_link']))?$data['_link']:$data['title']),$id)));
			$data['_level'] = $this->page_m->get_level($data['_parent_id']); 
			if (!$this->upload->do_upload())
			{
// $this->upload->display_errors();
			}
			else
			{
				$upload_data = $this->upload->data();
				$data['image'] = $upload_data['file_name'];
			}
			$id = $this->page_m->save($data, $id);
			$slug['_slug'] = $this->page_layout_m->get_slug($data['_layout'],$id);
			$this->page_m->save($slug, $id);
			    if ($this->input->post('submit') == "Save and Continue") {
                redirect('' . strtolower(get_class($this)) . '/edit');
            } else if ($this->input->post('submit') == "Update") {
                redirect('' . strtolower(get_class($this)) . '/edit/' . $id);
            } else if ($this->input->post('submit') == "Save") {
                redirect('' . strtolower(get_class($this)) );
            } else {
                redirect('' . strtolower(get_class($this)) );
            }
		}
// Load the view
		$this->data['subview'] = strtolower(get_class($this)) . '/edit';
		$this->load->view('_layout_crud', $this->data);
	}
	public function order ()
	{
		$this->data['sortable'] = TRUE;
		$this->data['subview'] = 'page/order';
		$this->load->view('_layout_main', $this->data);
	}
	public function order_ajax ()
	{
// Save order from ajax call
		if (isset($_POST['sortable'])) {
			$this->page_m->save_order($_POST['sortable']);
		}
// Fetch all pages
		$this->data['pages'] = $this->page_m->get_nested();
// Load view
		$this->load->view('page/order_ajax', $this->data);
	}
	
	
	public function delete ($id)
	{
		$this->page_m->delete($id);
		redirect('page');
	}
	public function _unique_slug ($str)
	{
// Do NOT validate if slug already exists
// UNLESS it's the slug for the current page
		$id = $this->uri->segment(4);
		$this->db->where('slug', $this->input->post('slug'));
		! $id || $this->db->where('id !=', $id);
		$page = $this->page_m->get();
		if (count($page)) {
			$this->form_validation->set_message('_unique_slug', '%s should be unique');
			return FALSE;
		}
		return TRUE;
	}
		public function _unique_link ($str,$id=NULL)
	{
// Do NOT validate if slug already exists
// UNLESS it's the slug for the current page
		$this->db->where('_link', $str);
		if(!empty($id)){
			$this->db->where('id !=', $id);
		}
		$page = $this->page_m->get();
		if (count($page)) {
			pr(increment_string($str));
			$str=$this->_unique_link(increment_string($str));
		}
		return $str;
	}
}