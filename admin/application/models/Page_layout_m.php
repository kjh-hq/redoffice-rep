<?php
class Page_layout_m extends MY_Model
{
	protected $_table_name = 'page_layout';
	protected $_order_by = 'layout_id';
	 protected $_primary_key = 'layout_id';
	 protected $_primary_name = 'layout_name';
	public $rules = array();
	 public function get_primary_key() {
        return $this->_primary_key;
    }
    public function get_primary_name() {
        return $this->_primary_name;
    }
	public function get_new ($array=NULL)
	{
		$obj = new stdClass();
		$obj->layout_name = '';
		if($array){
            $result = json_encode($obj);
            return json_decode($result,TRUE);
        }
        return $obj;
	}
	public function get_dropdown()
	{
// Fetch pages without parents
		$this->db->select('layout_id, layout_name');
		$this->db->where('_active', 1);
		$obj = parent::get();
// Return key => value pair array
		$array = array();
		if (count($obj)) {
			foreach ($obj as $data) {
				$array[$data->layout_id] = $data->layout_name;
			}
		}
		return $array;
	}
	public function get_slug($layout_id,$id){
		$obj = parent::get($layout_id,true);
		return str_replace('(:num)',$id, $obj->layout_slug);
	}
		public function get_id_by_tag($str){
		$this->db->where('_tag',$str);
		$obj = parent::get(null,true);
		return $obj->layout_id;
	}
}