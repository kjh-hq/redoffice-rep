<?php
class Admin_users_M extends MY_Model {
    protected $_table_name = 'admin_users';
    protected $_order_by = 'admin_id';
    protected $_primary_key = 'admin_id';
    public $rules = array(
        'username' => array(
            'field' => 'username',
            'label' => 'username',
            'rules' => 'trim|required|min_length[4]|max_length[25]'
        ),
        'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
        )
    );
    public $registration_rule = array(
        'username' => array(
            'field' => 'username',
            'label' => 'username',
            'rules' => 'trim|min_length[4]|max_length[25]|required|callback__unique_mobile'
        ),
        'cap' => array(
            'field' => 'cap',
            'label' => 'Captcha',
            'rules' => 'trim|required|callback__capthca_matches',
        )
    );
    public $rules_admin = array(
        'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required|min_length[6]|max_length[8]|matches[password_confirm]'
        ),
        'password_confirm' => array(
            'field' => 'password_confirm',
            'label' => 'Confirm password',
            'rules' => 'trim|required|matches[password]'
        ),
    );
    public $rules_profile = array(
        'admin_recover_mobile_no' => array(
            'field' => 'admin_recover_mobile_no',
            'label' => 'Alternative Mobile Number',
            'rules' => 'trim|required|min_length[11]|max_length[11]|callback__unique_mobile'
        ),
        'admin_email' => array(
            'field' => 'admin_email',
            'label' => 'Email',
            'rules' => 'trim|required'
        )
    );
    function __construct() {
        parent::__construct();
    }
    public function get_new() {
        $auser = new stdClass();
        $auser->admin_id = null;
        $auser->admin_name = "";
        $auser->admin_email = "";
        $auser->admin_recover_mobile_no = "";
        $auser->admin_address = "";
        $auser->admin_country_id = "";
        return $auser;
    }
    public function admincisession() {
        $cookie_name = "admincisession";
        if (!isset($_COOKIE[$cookie_name])) {
            return "Cookie named '" . $cookie_name . "' is not set!";
        } else {
            return $_COOKIE[$cookie_name];
        }
    }
    public function login() {
        $user = $this->get_by(array(
            'admin_name' => $this->input->post('username'),
            'admin_password' => $this->hash($this->input->post('password') . $this->input->post('username')),
                ), TRUE);
        if (count($user)) {
// Log in user
            $data = array(
                'name' => $user->admin_name,
                'email' => $user->admin_email,
                'auth_level' => $user->admin_permission_id,
                'id' => $user->admin_id,
                'loggedin' => TRUE,
                'admincisession' => $this->admincisession(),
            );
            $data_auth['admin_sess_id'] = $this->admincisession();
            $this->save($data_auth, $user->admin_id);
            $this->session->set_userdata($data);
        } else {
            $data = array('invalid_try' => ($this->session->userdata('invalid_try') == NULL ? 0 : $this->session->userdata('invalid_try') + 1));
            $this->session->set_userdata($data);
        }
    }
    public function login_by_id($id) {
        $user = $this->get($id);
        if (count($user)) {
// Log in user
            $data = array(
                'name' => $user->admin_name,
                'email' => $user->admin_email,
                'auth_level' => $user->admin_permission_id,
                'id' => $user->admin_id,
                'loggedin' => TRUE,
                'admincisession' => $this->admincisession(),
            );
            $data_auth['admin_sess_id'] = $this->admincisession();
            $this->save($data_auth, $user->admin_id);
            $this->session->set_userdata($data);
        } else {
            $data = array('invalid_try' => ($this->session->userdata('invalid_try') == NULL ? 0 : $this->session->userdata('invalid_try') + 1));
            $this->session->set_userdata($data);
        }
    }
    public function logout() {
        if ($this->session->userdata('id')) {
            $data_auth['admin_sess_id'] = "";
            $this->save($data_auth, $this->session->userdata('id'));
        }
        $this->session->sess_destroy();
    }
    public function loggedin() {
        return (bool) $this->session->userdata('loggedin');
    }
    public function auth() {
        return $this->session->userdata('auth_level');
    }
    public function get_name($id) {
        $this->db->select('admin_id,admin_name');
        $this->db->where('admin_id', $id);
        $result = parent::get(null, true);
        return $result->admin_name;
    }
    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }
    public function get_affiliate() {
        // Fetch pages without parents
        $auser = $this->get_by(array('admin_permission_id' => $this->admin_permissions_profile_m->get_affiliate_id()));
        // Return key => value pair array
        $array = array();
        if (count($auser)) {
            foreach ($auser as $data) {
                $array[$data->admin_id] = $data->admin_name;
            }
        }
        return $array;
    }
}
