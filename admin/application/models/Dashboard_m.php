<?php
class Dashboard_m extends MY_Model
{
	protected $_table_name = 'dashboard';
	protected $_order_by = 'id desc';
	protected $_timestamps = FALSE;
	public $rules = array(
		'about' => array(
			'field' => 'about', 
			'label' => 'Payment Type', 
			'rules' => 'trim'
		
		), 
	);
	
	public $rules_search = array(
		'search' => array(
			'field' => 'search', 
			'label' => 'search', 
			'rules' => 'trim'
		
		), 
	);
	public function index(){
	
	}
	
	public function get_new ()
	{
		$dashboard = new stdClass();
		return $dashboard;
	}
	
	function get_about(){
		$this->db->select('about')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
	
		function get_customer(){
		$this->db->select('customer')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
	
		function get_return_policy(){
		$this->db->select('return_policy')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
	
		function get_how(){
		$this->db->select('how')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
	
		function get_faq(){
		$this->db->select('faq')->from('dashboard')->where('id',1);
		$query=$this->db->get();
		return $query->row();
	}
		function get_day_order($id){
		$this->db->select('id')->from('order_num')->where('HOUR(created)',$id);
		$query=$this->db->get();
		return $query->num_rows();
	}
	function get_day_visitor($id){
		$this->db->select('id')->from('visitor')->where('HOUR(created)',$id);
		$query=$this->db->get();
		return $query->num_rows();
	}
		function get_week_order($id){
		$this->db->select('id')->from('order_num')->where('WEEKDAY(created)',$id);
		$query=$this->db->get();
		return $query->num_rows();
	}
	function get_week_visitor($id){
		$this->db->select('id')->from('visitor')->where('WEEKDAY(created)',$id);
		$query=$this->db->get();
		return $query->num_rows();
	}
			function get_month_order($id){
		$this->db->select('id')->from('order_num')->where('DAY(created)',$id);
		$query=$this->db->get();
		return $query->num_rows();
	}
	function get_month_visitor($id){
		$this->db->select('id')->from('visitor')->where('DAY(created)',$id);
		$query=$this->db->get();
		return $query->num_rows();
	}
			function get_year_order($id){
		$this->db->select('id')->from('order_num')->where('MONTH(created)',$id);
		$query=$this->db->get();
		return $query->num_rows();
	}
	function get_year_visitor($id){
		$this->db->select('id')->from('visitor')->where('MONTH(created)',$id);
		$query=$this->db->get();
		return $query->num_rows();
	}
}