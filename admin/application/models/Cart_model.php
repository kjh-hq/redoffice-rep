<?php
class Cart_model extends MY_Model {
	protected $_table_name = 'products';
	protected $_order_by = 'id desc';
	protected $_timestamps = FALSE;
	public $rules = array ()
	;
	public function index() {
	}
	public function get_new() {
		$cart = new stdClass ();
		
		return $cart;
	}
	function retrieve_products() {
		$query = $this->db->get ( 'products' );
		return $query->result_array ();
	}
	
	// Updated the shopping cart
	function validate_update_cart() {
		
		// Get the total number of items in cart
		$total = $this->cart->total_items ();
		
		// Retrieve the posted information
		$item = $this->input->post ( 'rowid' );
		$qty = $this->input->post ( 'qty' );
		
		// Cycle true all items and update them
		for($i = 0; $i < $total; $i ++) {
			// Create an array with the products rowid's and quantities.
			$data = array (
					'rowid' => $item [$i],
					'qty' => $qty [$i] 
			);
			
			// Update the cart with the new information
			$this->cart->update ( $data );
		}
	}
	
	// Add an item to the cart
	function validate_add_cart_item() {
		$id = $this->input->post ( 'product_id' ); // Assign posted product_id to $id
		$cty = $this->input->post ( 'quantity' ); // Assign posted quantity to $cty
		$this->db->where ( 'id', $id ); // Select where id matches the posted id
		$query = $this->db->get ( 'products', 1 ); // Select the products where a match is found and limit the query by 1
		 $r=$query->row();
		if($this->input->post ( 'type' )){
			
			$type = $this->input->post ( 'type' );
			}else{
			if($r->price){
			$type = 'Regular';
			}elseif ($r->sprice) {
				$type = 'Standard';
			}elseif ($r->oprice) {
				$type = 'Original';
			}elseif ($r->cprice) {
				$type = 'Color';
			}
		}
		// Check if a row has been found
		
			
				$data = array (
						'id' => $id,
						'qty' => $cty,
						'name' => $r->name ,
						'img' => $r->small_image_name ,
						'type' => $type ,
				);
				if($type=='Regular'){
					$data['price']=$r->price-$r->discount;
				}elseif ($type=='Standard') {
					$data['price']=$r->sprice-$r->discount;
				}elseif ($type=='Original') {
					$data['price']=$r->oprice-$r->discount;
				}elseif ($type=='Color') {
					$data['price']=$r->cprice-$r->discount;
				}
			if ($this->cart->insert ( $data )) {	
				
				
				return TRUE;
		
			
			// Nothing found! Return FALSE!
		} else {
			return FALSE;
		}
	}
}