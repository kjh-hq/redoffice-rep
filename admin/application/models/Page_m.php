<?php

class Page_m extends MY_Model {
    protected $_table_name = 'pages';
    protected $_order_by = '_parent_id, _sort';
    protected $_primary_key = 'id';
    protected $_primary_name = 'title';
    public $rules = array(
        'title' => array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required|max_length[100]'
        )
    );
    public function get_primary_key() {
        return $this->_primary_key;
    }
    public function get_primary_name() {
        return $this->_primary_name;
    }
    public function get_new($array = NULL) {
        $obj = new stdClass();
        $obj->title = '';
        $obj->slug = '';
        $obj->background_color = '';
        $obj->_layout = '';
        $obj->description = '';
        $obj->_parent_id = 0;
        if ($array) {
            $result = json_encode($obj);
            return json_decode($result, TRUE);
        }
        return $obj;
    }
    public function delete($id) {
// Delete a page
        parent::delete($id);
// Reset parent ID for its children
        $this->db->set(array(
            '_parent_id' => 0
        ))->where('_parent_id', $id)->update($this->_table_name);
    }
    public function get_dropdown($id = null) {
        // Fetch pages without parents
        $this->db->select('id, title,_level');
        if (!empty($id)) {
            $this->db->where('id!=', $id);
        }
        $data_array = parent::get();
        // Return key => value pair array
        $array = array('0' => 'Not Parent');
        if (count($data_array)) {
            foreach ($data_array as $data) {
                $array[$data->id] = $data->title;
            }
        }
        return $array;
    }
    public function get_business_dropdown($page_layout_id) {
        // Fetch pages without parents
        $this->db->select('id, title,_level');
        $this->db->where('_layout', $page_layout_id);
        $data_array = parent::get();
        // Return key => value pair array
        $array = array();
        if (count($data_array)) {
            foreach ($data_array as $data) {
                $array[$data->id] = $data->title;
            }
        }
        return $array;
    }
    public function get_level($id) {
        $obj = parent::get($id, true);
        return (!empty($obj)) ? $obj->_level + 1 : 1;
    }
    public function save_order($pages) {
        if (count($pages)) {
            foreach ($pages as $order => $page) {
                if ($page['item_id'] != '') {
                    $data = array('_parent_id' => (int) $page['parent_id'], '_sort' => $order);
                    $this->db->set($data)->where($this->_primary_key, $page['item_id'])->update($this->_table_name);
                }
            }
        }
    }
    public function get_nested() {
        $this->db->order_by($this->_order_by);
        $pages = $this->db->get('pages')->result_array();
        $array = array();
        foreach ($pages as $page) {
            if (!$page['_parent_id']) {
// This page has no parent
                $array[$page['id']] = $page;
                $array[$page['id']]['q'] = $this->product_m->get_product_count($page['id']) . "parent";
            } else {
// This is a child page
                $array[$page['_parent_id']]['children'][] = $page;
                $array[$page['_parent_id']]['q'] = $this->product_m->get_product_count($page['_parent_id']) . "child";
            }
        }
        return $array;
    }
    public function get_with_parent($id = NULL, $single = FALSE) {
        $this->db->select('pages.*, p.title as parent_title');
        $this->db->join('pages as p', 'pages._parent_id=p.id', 'left');
        return parent::get($id, $single);
    }
    public function get_no_parents() {
// Fetch pages without parents
        $this->db->select('id, title');
        $this->db->where('_parent_id', 0);
        $pages = parent::get();
// Return key => value pair array
        $array = array(
            0 => 'No parent'
        );
        if (count($pages)) {
            foreach ($pages as $page) {
                $array[$page->id] = $page->title;
            }
        }
        return $array;
    }
    public function get_category() {
        $this->db->select('id, title');
        $this->db->where('_parent_id', 0);
        $category = parent::get();
// Return key => value pair array
        $array = array(
            0 => 'No parent'
        );
        if (count($category)) {
            foreach ($category as $category) {
                $this->db->select('id, title');
                $this->db->where('_parent_id', $category->id);
                $sub_category = parent::get();
                foreach ($sub_category as $sub_category) {
                    $array[$category->title][$sub_category->id] = $sub_category->title;
                }
            }
        }
        return $array;
    }
    public function get_all_selectable() {
        $this->db->select('id, title');
        $this->db->where('_parent_id', 0);
        $category = parent::get();
// Return key => value pair array
        $array = array();
        $array[0]['title'] = 'No parent';
        if (count($category)) {
            foreach ($category as $category) {
                $array[$category->id]['title'] = $category->title;
                $this->db->select('id, title,topic');
                $this->db->where('_parent_id', $category->id);
                 $this->db->order_by('topic', 'asc');
                $sub_category = parent::get();
                foreach ($sub_category as $sub_category) {
                    $data['title'] = $sub_category->title;
                    $data['topic'] = $sub_category->topic;
                    $children[$sub_category->id] = $data;
                }
                $array[$category->id]['children'] = $children;
                $children = null;
            }
        }
        return $array;
    }
    function get_cat_name($id) {
        $this->db->select('id,title')->from('pages')->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result->title;
    }
    public function get_menus_by_pos() {
        $this->db->select('*,
                        id as sys_menu_id,
                        title as sys_menu_name
                        ')->from('pages');
        $query = $this->db->get();
        $result = $query->result_array();
        $v = buildChild($result, $parent_id = '_parent_id');
        $output = array();
        $output = generateMenuArray(0, $output, $v, 10, 0, null, "--");
        return $output;
    }
}
