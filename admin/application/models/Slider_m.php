<?php
class Slider_m extends MY_Model {
    protected $_table_name = 'slide_image';
    protected $_order_by = 'id asc';
    protected $_timestamps = FALSE;
    protected $_primary_key = 'id';
    protected $_primary_name = 'title1';
    public $rules = array(
        'title1' => array(
            'field' => 'title1',
            'label' => 'Title1',
            'rules' => 'trim|max_length[100]'
        ),
        'title2' => array(
            'field' => 'title2',
            'label' => 'Title2',
            'rules' => 'trim|max_length[100]'
        )
    );
    public function index() {
        
    }
    public function get_primary_key() {
        return $this->_primary_key;
    }
    public function get_primary_name() {
        return $this->_primary_name;
    }
    public function get_new($array = NULL) {
        $obj = new stdClass();
        $obj->title1= '';
        $obj->title2= '';
        if ($array) {
            $result = json_encode($obj);
            return json_decode($result, TRUE);
        }
        return $obj;
    }
}
