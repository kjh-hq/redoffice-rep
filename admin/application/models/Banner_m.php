<?php
class Banner_m extends MY_Model
{
	protected $_table_name = 'banner';
	protected $_order_by = 'id asc';
	protected $_timestamps = FALSE;
	public $rules = array(
	
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|max_length[100]|xss_clean'
		
		),
		'link' => array(
			'field' => 'link', 
			'label' => 'Link', 
			'rules' => 'trim|prep_url|max_length[100]|xss_clean'
		
		),
	);
	public function index(){
	
	}
	
	public function get_new ()
	{
		$banner = new stdClass();
		return $banner;
	}
	
}