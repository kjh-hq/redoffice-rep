<?php

class Product_m extends MY_Model {

    protected $_table_name = 'products';

    protected $_order_by = '_parent_id desc, id desc';

    protected $_timestamps = TRUE;
    protected $_primary_key = 'id';
    protected $_primary_name = 'name';
    public $rules = array(

        '_parent_id' => array(

            'field' => '_parent_id',

            'label' => 'Category',

            'rules' => 'trim|required'

        ),

        'name' => array(

            'field' => 'name',

            'label' => 'Name',

            'rules' => 'trim|required|max_length[100]'

        )

    );

    public function index() {

        

    }
public function get_primary_key() {
        return $this->_primary_key;
    }
    public function get_primary_name() {
        return $this->_primary_name;
    }
    public function get_new() {

        $article = new stdClass();

        return $article;

    }

    public function get_with_parent($id = NULL, $single = FALSE) {

        $this->db->select('pages.*, p.title as parent_title');

        $this->db->join('pages as p', 'pages._parent_id=p.id', 'left');

        $pages = parent::get();

        // Return key => value pair array

        $array = array(

        );

        if (count($pages)) {

            foreach ($pages as $page) {

                $array[$page->id] = $page->title;

            }

        }

        return $array;

    }

    function get_post_count($where, $like) {

        $this->db->select('id')->from('products')->where($where)->like($like);

        $query = $this->db->get();

        return $query->num_rows();

    }

    function get_post($num = 20, $start = 0, $where, $like) {

        $this->db->select()->from('products')->where($where)->like($like)->order_by('created_at', 'desc')->limit($num, $start);

        $query = $this->db->get();

        return $query->result();

    }

    function get_product_count($id) {

        $this->db->select('id')->from('products')->where('_parent_id', $id);

        $query = $this->db->get();

        return $query->num_rows();

    }

    function get_product($id, $num = 20, $start = 0) {

        $this->db->select()->from('products')->where('_parent_id', $id)->order_by('name', 'asc')->limit($num, $start);

        $query = $this->db->get();

        return $query->result();

    }

    function get_child_of($id) {

        $this->db->select()->from('products')->where('_parent_id', $id);

        $query = $this->db->get();

        return $query->result();

    }

    function get_related($id) {

        $this->db->select('_parent_id')->from('products')->where('id', $id);

        $query = $this->db->get();

        $parent = $query->row();

        $this->db->select()->from('products')->where('_parent_id', $parent->_parent_id);

        $this->db->where('id !=', $id);

        $query2 = $this->db->get();

        return $query2->result();

    }

    function get_product_sort($id, $num = 20, $start = 0, $name_type, $name) {

        $this->db->select()->from('products')->where('_parent_id', $id)->order_by($name_type, $name)->limit($num, $start);

        $query = $this->db->get();

        return $query->result();

    }

    function status($id) {

        $sql = "UPDATE products

			SET _status = IF(_status = 0 , 1, IF(_status = 1 , 0, _status))

			WHERE id=$id;";

        $this->db->query($sql);

        return $id;

    }

}

