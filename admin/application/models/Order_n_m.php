<?php
class Order_n_m extends MY_Model
{
	protected $_table_name = 'order_num';
	protected $_order_by = 'created desc, id desc';
	protected $_timestamps = FALSE;
	public $rules = array(
	
		'ptype' => array(
			'field' => 'ptype', 
			'label' => 'Payment Type', 
			'rules' => 'trim|required|max_length[100]'
		
		), 
		'isterms1' => array(
			'field' => 'isterms1', 
			'label' => 'Accept Button', 
			'rules' => 'trim|required|max_length[100]'
		
		), 
	);
	public $admin_rules = array(
	
		'status' => array(
			'field' => 'status', 
			'label' => 'Status', 
			'rules' => 'trim|required|max_length[100]'
		
		)
	);
	
	public function index(){
	
	}
	
	public function get_new ()
	{
		$article = new stdClass();
		$article->visitor_id = '';
		$article->status = '';
		
		//$article->pubdate = date('Y-m-d');
		return $article;
	}
	
	function get_post_count(){
		$this->db->select('id')->from('order_num');
		$query=$this->db->get();
		return $query->num_rows();
	}
	
	function get_post($num=20,$start=0){
		$this->db->select()->from('order_num')->order_by('id','desc')->limit($num,$start);
		$query=$this->db->get();
		return $query->result();
	}
		function get_five(){
			$sql="SELECT order_num.id AS order_id, visitor.name AS visitor_name,order_num.status as status,order_num.created as created,order_num.total as total FROM visitor INNER JOIN  order_num ON visitor.id=order_num.visitor_id
ORDER BY `order_num`.`id`  DESC limit 5";
	
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_total_sale(){
		$sql="SELECT SUM(total) as total FROM order_num";
		$query=$this->db->query($sql);
		return $query->row();
	}
}
