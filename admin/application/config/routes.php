<?php



defined('BASEPATH') OR exit('No direct script access allowed');



/*

  | -------------------------------------------------------------------------

  | URI ROUTING

  | -------------------------------------------------------------------------

  | This file lets you re-map URI requests to specific controller functions.

  |

  | Typically there is a one-to-one relationship between a URL string

  | and its corresponding controller class/method. The segments in a

  | URL normally follow this pattern:

  |

  |	example.com/class/method/id/

  |

  | In some instances, however, you may want to remap this relationship

  | so that a different class/function is called than the one

  | corresponding to the URL.

  |

  | Please see the user guide for complete details:

  |

  |	https://codeigniter.com/user_guide/general/routing.html

  |

  | -------------------------------------------------------------------------

  | RESERVED ROUTES

  | -------------------------------------------------------------------------

  |

  | There are three reserved routes:

  |

  |	$route['default_controller'] = 'welcome';

  |

  | This route indicates which controller class should be loaded if the

  | URI contains no data. In the above example, the "welcome" class

  | would be loaded.

  |

  |	$route['404_override'] = 'errors/page_missing';

  |

  | This route will tell the Router which controller/method to use if those

  | provided in the URL cannot be matched to a valid route.

  |

  |	$route['translate_uri_dashes'] = FALSE;

  |

  | This is not exactly a route, but allows you to automatically route

  | controller and method names that contain dashes. '-' isn't a valid

  | class or method name character, so it requires translation.

  | When you set this option to TRUE, it will replace ALL dashes in the

  | controller and method URI segments.

  |

  | Examples:	my-controller/index	-> my_controller/index

  |		my-controller/my-method	-> my_controller/my_method

 */

$cisession = "";



$cookie_name = "cisession";

if (!isset($_COOKIE[$cookie_name])) {

    

} else {



    $cisession = $_COOKIE[$cookie_name];

}

//pr($cisession);

$route['default_controller'] = "dashboard";

$route['404_override'] = 'error404';

$route['user/logout'] = 'user/logout';

$route['user/login'] = 'user/login';

//$route['(.*)'] = "dashboard";

require_once( BASEPATH . 'database/DB.php');



$db = &DB();



$sql = "SELECT admin_permission_id 

		FROM admin_users

		WHERE  admin_sess_id='" . $cisession . "'";

$query = $db->query($sql);

$result = $query->row();

//pr($result);

if (count($result) != 0) {

    if ($result->admin_permission_id) {

        //All primary link routing permission

        $admin_permission_id = $result->admin_permission_id;

        $sql = "SELECT perm.*,ap.* 

		FROM admin_permissions as perm inner join admin_page as ap 

		on perm.perm_page=ap.ap_id 

		WHERE  perm.perm_profile=$result->admin_permission_id";

        $query = $db->query($sql);

        $result = $query->result();



        foreach ($result as $key) {



            if ($key->perm_view) {

                $route[$key->ap_view_slug] = $key->ap_view_link;

            } else {

                $route[$key->ap_view_slug] = "permission/error";

            }

            if ($key->perm_add) {

                $route[$key->ap_add_slug] = $key->ap_add_link;

            } else {

                $route[$key->ap_add_slug] = "permission/error";

            }



            if ($key->perm_edit) {

                $route[$key->ap_edit_slug] = $key->ap_edit_link;

            } else {

                $route[$key->ap_edit_slug] = "permission/error";

            }



            if ($key->perm_delete) {

                $route[$key->ap_delete_slug] = $key->ap_delete_link;

            } else {

                $route[$key->ap_delete_slug] = "permission/error";

            }

        }





  

        $roots = $route;

    }

} else {

    

}



//pr($route);