<section>
	<h2>Vedios</h2>
	
	<?php echo anchor('admin/vedio/edit', '<i class="fa fa-plus"></i> Add an vedio'); ?>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Created</th>				
				<th>Link</th>				
				<th>Edit</th>
			</tr>
		</thead>
		<tbody>
<?php if(count($vedio)): foreach($vedio as $vedio): ?>	
		<tr>
			<td><?php echo anchor('admin/vedio/edit/' . $vedio->id, $vedio->title); ?></td>
			<td><?php echo $vedio->created; ?></td>
			<td><?php echo $vedio->link; ?></td>
			<td><?php echo btn_edit('admin/vedio/edit/' . $vedio->id); ?></td>
			
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">We could not find any vedio.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
</section>