<div class="panel panel-default">
    <div class="page-header">
        <div class="container-fluid">
        
            <div class="pull-right">
                <?php echo btn_add($perm_button, $this->uri->segment(1), null, '', '<i class="fa fa-step-backward" aria-hidden="true"></i> Back to '.$page_title[$this->uri->segment(1)]); ?>
            </div>
            <h1><?php echo $page_title[$this->uri->segment(1)] ?></h1>
        </div>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo empty($manage[$this->router->fetch_class()][$_primary_key]) ? '<i class="fa fa-plus"></i> Add ' . $page_title[$this->uri->segment(1) . '/' . $this->uri->segment(2)] : '<i class="fa fa-edit"></i> Edit ' . $page_title[$this->uri->segment(1)] . ' <strong>' . $manage[$this->router->fetch_class()][$_primary_name] . '</strong>'; ?></h3>
    </div>
    <?php echo notification_msg(); ?>
   <?php echo form_open_multipart(null, 'onSubmit="return confirm(\'Do you want to continue ? \')"'); ?>
    <div class="panel-body">
        <div class="table table-bordered table-hover">
            <table class="table">
               