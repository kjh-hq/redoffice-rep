 <div class="panel panel-default">
 <div class="page-header">
    <div class="container-fluid">
	 <div class="pull-right">
	 <a data-original-title="Add New" href="<?php echo site_url('admin/customer/edit'); ?>" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus"></i></a>
     </div>
      <h1>Customer</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('admin/dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('admin/customer'); ?>">Customer</a></li>
      </ul>
    </div>
 </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i>Customer List</h3>
  </div>
	<div class="panel-body">
	 <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
            <li class=""><a href="#tab-history" data-toggle="tab">History</a></li>
            <li class=""><a href="#tab-transaction" data-toggle="tab">Transactions</a></li>
            <li><a href="#tab-reward" data-toggle="tab">Reward Points</a></li>
            <li><a href="#tab-ip" data-toggle="tab">IP Addresses</a></li>
                      </ul>
	<div class="tab-content">
            <div class="tab-pane active" id="tab-general">
             
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<tbody>
		<tr>
			<td class="text-left">Id</td>
			<td class="text-left"><?php echo $customer->id; ?></td>
		</tr>
		<tr>
			<td class="text-left">Name</td>
			<td class="text-left"><?php echo $customer->name; ?></td>
		</tr>
		<tr>
			<td class="text-left">Email</td>
			<td class="text-left"><?php echo $customer->email; ?></td>
		</tr>
		<tr>
			<td class="text-left">Phone</td>
			<td class="text-left"><?php echo $customer->phone; ?></td>
		</tr>
		<tr>
			<td class="text-left">Ip</td>
			<td class="text-left"><?php echo $customer->ip; ?></td>
		</tr>
		<tr>
			<td class="text-left">Registered On</td>
			<td class="text-left"><?php $da=new Datetime($customer->created);echo $da->format('Y/m/d H:i:s A'); ?></td>
		</tr>
		<tr>
			<td class="text-left">Last Active On</td>
			<td class="text-left"><?php $d=new Datetime($customer->active);echo $d->format('Y/m/d H:i:s A'); ?></td>
		</tr>
		</tbody>
	</table>
</div>
</div>
</div>
</div>
</div>