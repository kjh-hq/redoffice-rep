 <div class="panel panel-default">
 <div class="page-header">
    <div class="container-fluid">
	 <div class="pull-right">
	 <a data-original-title="Add New" href="<?php echo site_url('admin/customer/edit'); ?>" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus"></i></a>
     </div>
      <h1>Customer</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('admin/dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('admin/customer'); ?>">Customer</a></li>
      </ul>
    </div>
 </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i>Customer List</h3>
  </div>
	<div class="panel-body">
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<td class="text-left">Id</td>
				<td class="text-left">Name</td>
				<td class="text-left">Email</td>
				<td class="text-left">IP</td>
				<td class="text-left">Created</td>
				<td class="text-left">Last Active</td>
				<td class="text-right">Edit</td>
			</tr>
		</thead>
		<tbody>
<?php if($customer): foreach($customer as $customer): ?>	
		<tr>
			<td class="text-left"><?php echo $customer->id; ?></td>
			<td class="text-left"><?php echo $customer->name; ?></td>
			<td class="text-left"><?php echo $customer->email; ?></td>
			<td class="text-left"><?php echo $customer->ip; ?></td>
			<td class="text-left"><?php $da=new Datetime($customer->created);echo $da->format('Y/m/d H:i:s A'); ?></td>
			<td class="text-left"><?php $d=new Datetime($customer->active);echo $d->format('Y/m/d H:i:s A'); ?></td>
			
			<td  class="text-right"><a data-original-title="Edit" data-toggle="tooltip" title="" class="btn btn-primary" href="<?php echo site_url('admin/dashboard/details/'.$customer->id); ?>"><i class="fa fa-pencil"></i></a></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td  class="text-center">We could not find any customers.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
<nav>
<?php echo $pages_p?>
</nav>
</div>
</div>
</div>