<?php echo form_open_multipart(); ?>
<div class="panel panel-default">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <?php echo btn_add($perm_button, $this->uri->segment(1)  . '/edit', null, '', '<i class="fa fa-plus"></i> Add '.$page_title[$this->uri->segment(1) ]); ?>
                 <button type="submit" name="submit" data-toggle="tooltip" data-original-title="Save" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
            <h1><?php echo $page_title[$this->uri->segment(1) ] ?></h1>
             <ul class="breadcrumb">
                <li><a href="<?php echo site_url($page_title['home_slug']); ?>"><?php echo $page_title['home']; ?></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="fa fa-list"></i> &nbsp;<?php echo $page_title[$this->uri->segment(1) ] ?>
        </h3>
    </div>
    <?php echo notification_msg(); ?>
    <div class="panel-body">
        <div class="table-responsive">
            