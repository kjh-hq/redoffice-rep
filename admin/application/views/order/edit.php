 <div class="page-header">
    <div class="container-fluid">
      <h1>Order</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('order/details/'); ?>">Order</a></li>
              </ul>
    </div>
  </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Order List</h3>
      </div>
	<div class="panel-body">
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
	<tbody>
							
							<tr>
								<td >Order Status</td>
								<td>
									Order No: <b><?php echo $id;?></b>
								</td>
							
								
								<td class="gen-div-2">Placed:<?php $da= new DateTime( $article->created);echo '&nbsp'; 
								echo $da->format('g:i:s');echo '&nbsp';
								echo $da->format('a');echo '&nbsp';
								echo $da->format('F');echo '&nbsp';
								echo $da->format('d');echo '&nbsp';
								echo $da->format('Y');
								
								
								?></td>
								
								<td class="gen-div-2">
									Status: <?php echo $article->status; ?>
								</td>
								</tr>
								<tr>
							<td class="gen-div fnt-15">Delivery Address</td>
								<td class="gen-div-2">
									<b>Name:<br><?php echo $article->name ?></b>
								</td>
								<td >Address:<br><?php echo $article->addr ?>,<?php echo $article->area?>,<br /><?php echo $article->ct ?>,<?php echo $article->cnt ?></td>
								<td >Phone:<br><?php echo $article->phone ?>,<?php echo $article->phone2 ?><br>
								Email:<?php echo $article->email ?></td>
								</tr>
								</tbody>
							</table>
							<!-- /shipping address -->
					
						<!-- invoice table -->
							<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Sr</th>
									<th>Image</th>
									<th>Item Description</th>
									<th>Status</th>
									<th>Quantity</th>
									<th>Unit Price</th>
									<th>Subtotal</th>
								</tr>
							</thead>
							<tbody>
							
<?php
$this->load->database ();
$sql = "SELECT * FROM `order_list` WHERE order_id = ".$id."";
$query = $this->db->query($sql);
$r = $query->result();
$i=1;
$total=0;
	foreach ( $r as $r ) :
		?><?php 
	$p=$this->product_m->get($r->product_id);
	?>
<tr>
<td><?php echo $i;?></td>
<td><img style="height:100px;width:100px;" src="<?php echo base_url();?>../public/img_upload/small/<?php echo $this->product_image_m->singleImage1($p->id);?>"></td>
<td><a href="<?php echo site_url();?>../ps/product/<?php echo $p->id; ?>"><?php echo $p->code. ' : '; ?><?php echo $p->name; ?></a></td>
<td><?php echo $article->status; ?></td>
<td><?php echo $this->cart->format_number($r->qty)?></td>
<td><?php echo $this->cart->format_number($p->price-$p->discount);?></td>
<td><?php echo $this->cart->format_number(($p->price-$p->discount)*$r->qty);$total+=($p->price-$p->discount)*$r->qty;?></td>
<?php $i++; ?>
<?php endforeach; ?>
								<!-- /row 1 -->
								<!-- subtotal/total -->
								<tr>
									<td rowspan="2" colspan="4" class="border-0"></td> <td class="border-0"></td>
									<td>Subtotal</td>
									<td><?php echo $this->cart->format_number($total)?></td>
								</tr>
								<tr><td class="border-0"></td>
									<td>Shipping</td>
									<td><?php echo $this->cart->format_number(40)?></td>
								</tr>
								<tr><td class="border-0"></td>
									<td colspan="4" class="border-0"></td>
									<td><b>Total</b></td>
									<td><b><?php echo $this->cart->format_number($total+40)?> </b></td>
								</tr>
								<!-- /subtotal/total -->
								
							</tbody>
						</table>
						
						
						<a href="<?php echo site_url();?>order/status/?id=<?php echo $id?>&status=Completed"><button type="button" style="margin-left:3px;" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-thumbs-o-up"></i> Complete</button>&nbsp;&nbsp;&nbsp;</a>
						<a href="<?php echo site_url();?>order/status/?id=<?php echo $id?>&status=Cancel"><button type="button" style="margin-left:3px;" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-thumbs-o-down"></i> Cancel</button>&nbsp;&nbsp;&nbsp;</a>
						<a href="<?php echo site_url();?>order"><button type="button" style="margin-left:3px;" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-reply"></i> See Later</button>&nbsp;&nbsp;&nbsp;</a>
						<a href="<?php echo site_url();?>order/print_order/<?php echo $id?>"><button type="button" style="margin-left:3px;" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-reply"></i> Print Order</button>&nbsp;&nbsp;&nbsp;</a>
			<!-- /invoice table -->
</div>
					</div>
		