<!DOCTYPE html>
<html>
<head>
<title>Delivery Bill</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo base_url()?>public/font-awesome-4.2.0/css/font-awesome.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url()?>public/font-awesome-4.2.0/css/font-awesome.min.css" type="text/css" />
<link href="<?php echo base_url() ?>assets/opencart/bootstrap.less" rel="stylesheet/less">
<style id="less:admin-view-javascript-bootstrap-less-bootstrap" type="text/css"></style>
<link href="<?php echo base_url() ?>assets/opencart/summernote.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/opencart/bootstrap-datetimepicker.css" type="text/css" rel="stylesheet" media="screen">
<link type="text/css" href="<?php echo base_url() ?>assets/opencart/stylesheet.css" rel="stylesheet" media="screen">
<script src="<?php echo base_url() ?>assets/opencart/less-1.js"></script>
<style>div.hidden {display: none}</style></head><body>
<div class="panel-body">
<div class="table-responsive">
<table class="table table-bordered table-hover">
<tbody>
<tr>
<td colspan="3">
Order No: <b><?php echo $id;?></b>
</td>
</tr>
<tr>
<td>
<b>Name: <?php echo $article->name ?></b>
</td>
</tr>
<tr>
<td >Address: <?php echo $article->addr ?>,<?php echo $article->area?>,<?php echo $article->ct ?>,<?php echo $article->cnt ?></td>
</tr>
<tr>
<td >Phone: <?php echo $article->phone ?> , <?php echo $article->phone2 ?></td>
</tr>
</tbody>
</table>
<table class="table table-bordered table-hover">
<thead>
<tr>
<th>Sr</th>
<th>Item Description</th>
<th>Quantity</th>
<th>Unit Price</th>
<th>Subtotal</th>
</tr>
</thead>
<tbody>
<!-- row 1 -->
<?php
$this->load->database ();
$sql = "select * from `order_list` where order_id=".$id."";
$query = $this->db->query ( $sql );
$r = $query->result ();
$i=1;
$total=0;
$total_product=0;
foreach ( $r as $r ) :
?><?php $p=$this->product_m->get($r->product_id)?>
<tr>
<td><?php echo $i;?></td>
<td><?php echo $p->code. ' : '; ?><?php echo $p->name; ?></td>
<td><?php echo $this->cart->format_number($r->qty)?></td>
<td><?php echo $this->cart->format_number($p->price-$p->discount);?></td>
<td><?php echo $this->cart->format_number(($p->price-$p->discount)*$r->qty);$total+=($p->price-$p->discount)*$r->qty;$total_product++;?></td>
<?php $i++; ?>
<?php endforeach; ?>
<tr>
<td rowspan="2" colspan="3" class="border-0"></td>
<td>Subtotal</td>
<td><?php echo $this->cart->format_number($total)?></td>
</tr>
<tr>
<td>Shipping</td>
<td><?php echo $this->cart->format_number(40)?></td>
</tr>
<tr>
<td colspan="3" class="border-0"></td>
<td><b>Total</b></td>
<td><b><?php echo $this->cart->format_number($total+40)?> </b></td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="panel-body">
<div class="table-responsive">
<table class="table table-bordered table-hover">
<thead>
	<tr>
		<th colspan="2"><h2 style="text-align:center">Bill Voucher</h2></th>
	</tr>
</thead>
<tbody style="width: 100%;">
<tr>
<td style="border: 0px none;">Name: </td><td  style="border: 0px none;"><?php echo $article->name ?></td>
</tr>
<tr>
<td style="border: 0px none;">Order No: </td><td  style="border: 0px none;"><b>RB-O<?php echo $id;?></b></td>
</tr>
<tr>
<td style="border: 0px none;">Total Amount to pay: </td><td  style="border: 0px none;"><b><?php echo $this->cart->format_number($total+40)?></b></td>
</tr>
<tr>
<td style="border-top: 0px none;border-right: 0px none;border-left: 0px none;">Total Product Unit : </td>
<td style="border-top: 0px none;border-right: 0px none;border-left: 0px none;"><?php echo $total_product ?></td>
</tr>
<tr>
<td style="border: 0px none;">Signature By Receiver</td>
<td style="border: 0px none;">Signature By Delivery Officer</td>
</tr>
<tr>
<td style="border: 0px none;">&nbsp;</td>
<td style="border: 0px none;">&nbsp;</td>
</tr>
</tbody>
</table>
</div>
</div>
