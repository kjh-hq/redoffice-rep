  <div class="page-header">
    <div class="container-fluid">
      <h1>Order</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('order/details/'); ?>">Order</a></li>
              </ul>
    </div>
  </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Order List</h3>
      </div>
	<div class="panel-body">
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
	<?php if(count($articles)):?>
		<thead>
			<tr>
				<td  class="text-center">#</td>
				<td  class="text-left">Name</td>
				<td class="text-left">Status</td>
				<td class="text-left">Total</td>
				<td class="text-left">Order Created</td>
				<td class="text-center">Action</td>
			
			</tr>
		</thead>
		<tbody>
<?php  foreach($articles as $article): ?>	
		<tr>
			<td  class="text-center"><?php echo  $article->id; ?></td>
			<td  class="text-left"><?php echo $article->name; ?></td>
			<td  class="text-left"><?php echo $article->status; ?></td>
			<td  class="text-left"><?php echo $article->total; ?></td>
			<td  class="text-left"><?php $da= new DateTime($article->created); echo $da->format('M-d-Y h:i:s a')?></td>
			<td  class="text-right"><a data-original-title="View" data-toggle="tooltip" title="" class="btn btn-primary" href="<?php echo site_url('order/details/' . $article->id); ?>"><i class="fa fa-eye"></i></a>
			<a data-original-title="Delete" data-toggle="tooltip" title="" class="btn btn-danger" href="<?php echo site_url('order/delete/' . $article->id); ?>"><i class="fa fa-trash-o"></i></a></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td  class="text-center">We could not find any order.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
</div>
<nav>
<?php echo $pages_p?>
</nav>
</div>