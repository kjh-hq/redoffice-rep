<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1>Dashboard</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('dashboard'); ?>">Dashboard</a></li>
              </ul>
    </div>
  </div>
  <div class="container-fluid">
        <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile">
  <div class="tile-heading">Total Orders <span class="pull-right">
        <i class="fa fa-caret-up"></i>
        100%</span></div>
  <div class="tile-body"><i class="fa fa-shopping-cart"></i>
    <h2 class="pull-right" style="font-size: 20px"><?php echo $order;?></h2>
  </div>
  <div class="tile-footer"><a href="<?php echo site_url('order'); ?>">View more...</a></div>
</div>
</div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile">
  <div class="tile-heading">Total Sales <span class="pull-right">
        <i class="fa fa-caret-up"></i>
        100% </span></div>
  <div class="tile-body"><i class="fa fa-credit-card"></i>
    <h2 class="pull-right" style="font-size: 20px"><?php echo $total_sale->total." TK";?></h2>
  </div>
  <div class="tile-footer"><a href="<?php echo site_url('order'); ?>">View more...</a></div>
</div>
</div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile">
  <div class="tile-heading">Total Customers <span class="pull-right">
        <i class="fa fa-caret-down"></i>
        -300%</span></div>
  <div class="tile-body"><i class="fa fa-user"></i>
    <h2 class="pull-right" style="font-size: 20px"><?php echo $visitor;?></h2>
  </div>
  <div class="tile-footer"><a href="<?php echo site_url('dashboard/customer'); ?>">View more...</a></div>
</div>
</div>
      <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile">
  <div class="tile-heading">People Online</div>
  <div class="tile-body"><i class="fa fa-eye"></i>
    <h2 class="pull-right" style="font-size: 20px">0</h2>
  </div>
  <div class="tile-footer"><a href="<?php echo site_url('dashboard/customer'); ?>">View more...</a></div>
</div>
</div>
    </div>
  
    <div class="row">
     <div class="col-lg-6 col-md-12 col-sx-12 col-sm-12">
     <div class="panel panel-default">
  <div class="panel-heading">
    <div class="pull-right"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> <i class="caret"></i></a>
      <ul id="range" class="dropdown-menu dropdown-menu-right">
        <li><a href="day">Today</a></li>
        <li><a href="week">Week</a></li>
        <li class="active"><a href="month">Month</a></li>
        <li><a href="year">Year</a></li>
      </ul>
    </div>
    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Sales Analytics</h3>
  </div>
  <div class="panel-body">
    <div id="chart-sale" style="width: 100%; height: 260px;"></div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.flot.resize.min.js"></script>
<script type="text/javascript"><!--
$('#range a').on('click', function(e) {
  e.preventDefault();
  
  $(this).parent().parent().find('li').removeClass('active');
  
  $(this).parent().addClass('active');
  
  $.ajax({
    type: 'get',
    url: 'dashboard/status?range=' + $(this).attr('href'),
    dataType: 'json',
    success: function(json) {
       
     drawChart(json);
    },
        error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
  });
});
$('#range .active a').trigger('click');
//--></script> 
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1.1", {packages:["bar"]});
      google.setOnLoadCallback(drawChart);
      function drawChart(json) {
        var data = google.visualization.arrayToDataTable(json);
        var options = {
          
        };
        var chart = new google.charts.Bar(document.getElementById('chart-sale'));
        chart.draw(data, options);
      }
    </script>
</div>
    </div>
    </div>
    <div class="row">
		<?php if ($visitor_created) :?>	
      <div class="col-lg-4 col-md-12 col-sm-12 col-sx-12"><div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-calendar"></i> Recent Activity</h3>
  </div>
  <ul class="list-group">
  <?php foreach($visitor_created as $visitor_created): ?>
  
            <li class="list-group-item"><a href=""><?php echo $visitor_created->name ?></a> registered a new account.<br>
      <small class="text-muted"><i class="fa fa-clock-o"></i> <?php $da= new DateTime($visitor_created->created);echo $da->format('Y/m/d H:i:s a'); ?></small></li>
        
	<?php endforeach; ?>
         
		 </ul>
</div></div>
<?php else: ?>							
<?php endif; ?>	
<?php if ($order_created) :?> 
      <div class="col-lg-8 col-md-12 col-sm-12 col-sx-12"> <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Latest Orders</h3>
  </div>
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <td class="text-right">Order ID</td>
          <td>Customer</td>
          <td>Status</td>
          <td>Date Added</td>
          <td class="text-right">Total</td>
          <td class="text-right">Action</td>
        </tr>
      </thead>
      <tbody>
       <?php foreach($order_created as $order_created): ?>
                        <tr>
          <td class="text-right"><?php echo $order_created->order_id;?></td>
          <td><?php echo $order_created->visitor_name;?></td>
          <td><?php echo $order_created->status;?></td>
          <td><?php echo $order_created->created;?></td>
          <td class="text-right"><?php echo $order_created->total;?></td>
          <td class="text-right"><a class="btn btn-info" title="" data-toggle="tooltip" href="<?php echo site_url();?>order/details/<?php echo $order_created->order_id;?>" data-original-title="View"><i class="fa fa-eye"></i></a></td>
        </tr>
       <?php endforeach; ?>
                      </tbody>
    </table>
  </div>
</div>
 </div>
 <?php else: ?>             
<?php endif; ?> 
    </div>
  </div>
</div>
