<div class="panel panel-default">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a data-original-title="Add New" href="<?php echo site_url('upload1/upload'); ?>" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus"></i></a>
			</div>
			<h1>Slide Show</h1>
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url('admin/dashboard'); ?>">Home</a></li>
				<li><a href="<?php echo site_url('admin/dashboard/slide_show'); ?>">Slide Show</a></li>
			</ul>
		</div>
	</div>
	<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-list"></i>Slide Show Image List</h3>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td  class="text-left">Images</td>
						<td  class="text-right">Delete</td>
					</tr>
				</thead>
				<tbody>
					<?php if(count($slider)): foreach($slider as $slider): ?>	
						<tr>
							<td  class="text-left"><img style="width: 250px;"
								src="<?php echo base_url()?>../public/img_upload/<?php echo $slider->image ?>" /></td>
								<td  class="text-right"><a data-original-title="Delete" data-toggle="tooltip" title="" class="btn btn-danger" href="<?php echo site_url('admin/slider/delete?id='.$slider->id.'&image='.$slider->image); ?>"><i class="fa fa-trash-o"></i></a></td>
							</tr>
					<?php endforeach; ?>
				<?php else: ?>       <tr>
					<td colspan="3">We could not find any articles.</td>
				</tr>
			<?php endif; ?>	
		</tbody>
	</table>
</div>
</div>
</div>
