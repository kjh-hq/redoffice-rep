<!DOCTYPE html>
<html>
<head>
<title>Admin Panel</title>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome-4.2.0/css/font-awesome.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome-4.2.0/css/font-awesome.min.css" type="text/css" />
<!-- Open -->
<link href="<?php echo base_url() ?>assets/opencart/bootstrap.less" rel="stylesheet/less">
<style id="less:admin-view-javascript-bootstrap-less-bootstrap" type="text/css"></style>
<link href="<?php echo base_url() ?>assets/opencart/summernote.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/opencart/bootstrap-datetimepicker.css" type="text/css" rel="stylesheet" media="screen">
<link type="text/css" href="<?php echo base_url() ?>assets/opencart/stylesheet.css" rel="stylesheet" media="screen">
	<script type="text/javascript" src="<?php echo base_url() ?>assets/opencart/jquery-2.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/opencart/bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/opencart/less-1.js"></script>
<script type="<?php echo base_url() ?>assets/opencart/summernote.js"></script>
<script src="<?php echo base_url() ?>assets/opencart/moment.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/opencart/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/opencart/common.js" type="text/javascript"></script>
 <link href="<?php echo base_url(); ?>assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
        <!-- TinyMCE -->
	<script type="text/javascript" src="<?php echo base_url('assets/tiny_mce/tiny_mce.js'); ?>"></script>
	<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "textareas",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
		});
	</script>
	<!-- /TinyMCE -->
<style>div.hidden {display: none}</style></head>
<body>
