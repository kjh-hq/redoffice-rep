<link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/global.js"></script>
<script>
    $(function () {
        $('body').tooltip({selector: '[data-toggle="tooltip"]'});
    });
</script>	
<script type="text/javascript">
$("img").error(function () {
  $(this).unbind("error").attr("src", "<?php echo base_url();?>assets/image/no_image-512.png");
});
</script>
</body>
</html>
