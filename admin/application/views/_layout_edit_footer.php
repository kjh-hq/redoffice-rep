            <?php if (empty($manage[$this->router->fetch_class()][$_primary_key])) { ?>
                <tr>
                    <td></td>
                    <td>
                        <?php echo form_submit('submit', 'Save and Continue', 'class="btn btn-primary"'); ?>
                        <?php echo form_submit('submit', 'Save', 'class="btn btn-primary"'); ?>
                    </td>
                </tr>
            <?php } else {
                ?>
                <tr>
                    <td></td>
                    <td>
                        <?php echo form_submit('submit', 'Update', 'class="btn btn-primary"'); ?>
                        <?php echo form_submit('submit', 'Save', 'class="btn btn-primary"'); ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?php echo form_close(); ?>
    </div>
</div>
</div>