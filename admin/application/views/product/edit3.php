<?php 
if(!empty($this->input->get('rdirBack'))){
	$rdirBack = str_replace(".and.", "&", $this->input->get('rdirBack'));
	 }else if(!empty($this->input->post('rdirBack'))){
	 	$rdirBack = str_replace(".and.", "&", $this->input->post('rdirBack'));
	 }
?>
<?php echo form_open_multipart(); ?>
 <div class="panel panel-default">
 <div class="page-header">
    <div class="container-fluid">
	 <div class="pull-right">
	 	
	 
	 <a data-original-title="Add New" href="<?php echo site_url('product/edit'); ?>" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus"></i></a>
     <button type="submit" name="submit" data-toggle="tooltip" data-original-title="Save" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
     <?php if(!empty($rdirBack)){?>
     <a href="<?php echo $rdirBack; ?>" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
     <?php } ?>
     </div>
      <h1>Product</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('product'); ?>">Product</a></li>
      </ul>
    </div>
 </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i><?php echo empty($manage[$this->router->fetch_class()]['id']) ? 'Add a new product' : 'Edit product ' . $manage[$this->router->fetch_class()]['name']; ?></h3>
  </div>
<?php echo notification_msg();?>
<!-- 
<script type="text/javascript" src="<?php echo base_url();?>public/shop/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/shop/jquery.selectboxes.pack.js"></script>
 -->
	<div class="panel-body">
	<ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
            <li class=""><a href="#tab-image" data-toggle="tab">Image</a></li>
            <li class=""><a href="#tab-pf" data-toggle="tab">Features</a></li>
            <li class=""><a href="#tab-pts" data-toggle="tab">Technical Specifications</a></li>
            <li class=""><a href="#tab-pd" data-toggle="tab">Downloads</a></li>
     </ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-general">
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
	<tr>
		<td class="input-style">Category</td>
		<td><?php 
                echo "<select name='_parent_id' value='' placeholder='Model' id='input-model' class='form-control' autocomplete='off'>";
				foreach ($pages_with_parent as $key => $value) {
				echo "<option value='$key' ".($key==$manage[$this->router->fetch_class()]['_parent_id']? 'selected' : '')." class='optionGroup' disabled>".$value['title']."</option>";
					if(!empty($value['children'])){
					foreach ($pages_with_parent[$key]['children'] as $key2 => $value2) {
						echo "<option value='$key2' ".($key2==$manage[$this->router->fetch_class()]['_parent_id']? 'selected' :'' )." class='optionChild'>&nbsp;&nbsp;&nbsp;$value2</option>";
					}
				}
				}
				echo "</select>";
                ?>
 <style type="text/css">
				.optionGroup
				{
					font-family: monospace;
				    font-weight:bolder !important;
				    color:#14628c !important;
				    font-size: 16px;
				    font-style:italic;
				}
				.optionChild
				{
					font-family: monospace;
				    padding-left:15px;
				    font-size: 14px;
				}
				 </style>
                </td>
	</tr>
	
	<tr>
		<td class="input-style">Name</td>
		<td><?php echo form_input('name', set_value('name', $manage[$this->router->fetch_class()]['name']),' class="form-control"'); ?></td>
	</tr>	
	 <tr>
        <td>Link</td>
        <td><?php echo form_input('_link', set_value('_link', $this->input->post('_link') ? $this->input->post('_link') : urldecode($manage[$this->router->fetch_class()]['_link']),false), 'class="form-control"'); ?></td>
    </tr>
	<tr>
		<td class="input-style">Short Description</td>
		<td><?php echo form_input('shortdesc', set_value('shortdesc', $manage[$this->router->fetch_class()]['shortdesc']),' class="form-control"'); ?></td>
	</tr>		
	<tr>
		<td class="input-style">Short Feature</td>
		<td><?php echo form_textarea('product_feature_short', set_value('product_feature_short', $manage[$this->router->fetch_class()]['product_feature_short'],false),' class="tinymce form-control"'); ?></td>
	</tr>	
			
	<tr>
	
		<td class="input-style">Sales Type</td>
			<td><?php echo form_dropdown('sales_type', array('0'=>'None','1'=> 'featured','2'=>'bestsellers','3'=>'specials','4'=>'latest'), $this->input->post('sales_type') ? $this->input->post('sales_type') : $manage[$this->router->fetch_class()]['sales_type'],' class="form-control"'); ?></td>
	</tr>
	<tr>
	
		<td class="input-style">Sale</td>
			<td><?php echo form_dropdown('sale', array('0'=>'Hide','1'=> 'Show'), $this->input->post('sale') ? $this->input->post('sale') : $manage[$this->router->fetch_class()]['sale'],' class="form-control"'); ?></td>
	</tr>
	<tr>
	
		<td class="input-style">Menu Featured</td>
			<td><?php echo form_dropdown('menu_featured', $no_parents, $this->input->post('menu_featured') ? $this->input->post('menu_featured') : $manage[$this->router->fetch_class()]['menu_featured'],' class="form-control"'); ?></td>
	</tr>
	<tr>
	
		<td class="input-style">Menu Sales</td>
			<td><?php echo form_dropdown('menu_sales', $no_parents, $this->input->post('menu_sales') ? $this->input->post('menu_sales') : $manage[$this->router->fetch_class()]['menu_sales'],' class="form-control"'); ?></td>
	</tr>
</table>
</div>
</div>
<input type="hidden" value="<?php echo !empty($rdirBack)?$rdirBack:'';?>" name="rdirBack">
<div class="tab-pane" id="tab-image">
	<div class="table-responsive">
	<table id="images" class="table table-bordered table-hover">
	<thead>
			<tr>
				<th class="text-center">Image</th>
				<th class="text-center"><a class="asc" href="" >Sort</a></th>
				<th class="text-right"> Action </th>
			</tr>
		</thead>
		 <script type="text/javascript">
var image_row = 2;
function addImage() {
	html  = '<tr id="image-row' + image_row + '">';
	html += '  <td  class="text-center"><img id="srcimage'+image_row+'" class="img-thumbnail" style="width:100px;heigth:100px;" src="<?php echo base_url();?>assets/image/logo.png" alt="" title="" data-placeholder="<?php echo base_url();?>assets/image/logo.png" />';
	html += ''
    html += ''
    html += '<input type="file" id="file'+image_row+'" name="userfile[]" class="upload" />'
	html += '</td>';
	html += '  <td  class="text-center"><input type="text" name="_sort[' + image_row + ']" value="" placeholder="Sort Order" class="form-control" /></td>';
	html += '  <td class="text-right"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#images tbody').append(html);
	
	image_row++;
}
</script> 
		<tbody id="actions">
		<?php $imageInfo=$this->product_image_m->selectImage($manage[$this->router->fetch_class()]['id']);?>
		<?php foreach ($imageInfo as $key) {?>
		<tr>
		<td class="text-center"><img class="img-thumbnail" style="heigth:100px;width:100px;" src="<?php echo base_url();?>../public/img_upload/<?php echo $key->image;?>"></td>
		<td class="text-center"><?php echo form_input('_sort['.$key->id.']', set_value('_sort['.$key->id.']', $key->_sort),' class="form-control"'); ?></td>
		<td class="text-right"><a data-original-title="Delete" data-toggle="tooltip" title="" class="btn btn-danger" href="<?php echo site_url();?>product/deleteImg/<?php echo $key->id; ?>?rdirBack=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>#tab-image"
			onclick="if (confirm('Delete selected item?\n\nName:<?php echo $key->image; ?>')){return true;}else{event.stopPropagation(); event.preventDefault();};"
			><i class="fa fa-trash-o"></i> Delete</a></td>
		</tr>
<?php }?>
<tfoot>
<tr>
<td></td>
<td></td>
<td class="text-right"><a data-toggle="tooltip" onclick="addImage();" title="Add Image" data-original-title="Add Image" class="btn btn-warning"><i class="fa fa-plus"></i></a></td>
</tr>
</tfoot>
</table>
</div>
</div>
<div class="tab-pane" id="tab-pf">
<div class="table-responsive">
	<table class="table table-bordered table-hover">
	<thead></thead>
	<tbody>
	<tr>
		<td class="input-style">Product Feature</td>
		<td><?php echo form_textarea('product_features', set_value('product_features', $manage[$this->router->fetch_class()]['product_features'],false),'class="tinymce form-control"'); ?></td>
	</tr>
	</tbody>
</table>
</div>
</div>
<div class="tab-pane" id="tab-pts">
<table class="table table-bordered table-hover">
	<thead></thead>
	<tbody>
	<tr>
		<td class="input-style">Technical Specifications</td>
		<td><?php echo form_textarea('technical_specifications', set_value('technical_specifications', $manage[$this->router->fetch_class()]['technical_specifications'],false),'class="tinymce form-control"'); ?></td>
	</tr>
	</tbody>
</table>
</div>
<div class="tab-pane" id="tab-pd">
<table id="download_file" class="table table-bordered table-hover">
	<thead>
			<tr>
				<th class="text-center" width="100">Downloads</th>
				<th class="text-center">File Title</th>
				<th class="text-center" width="15"><a class="asc" href="" >Sort</a></th>
				<th class="text-right" width="80"> Action </th>
			</tr>
		</thead>
		 <script type="text/javascript">
var downloadfile_row = 2;
function adddownloadfile() {
	html  = '<tr id="image-row' + downloadfile_row + '">';
	html += '  <td  class="text-center"><img id="srcdownloadfile'+downloadfile_row+'" class="img-thumbnail" style="width:100px;heigth:100px;" src="<?php echo base_url();?>assets/image/logo.png" alt="" title="" data-placeholder="<?php echo base_url();?>assets/image/logo.png" />';
	html += ''
    html += ''
    html += '<input type="file" id="downloadfile'+downloadfile_row+'" name="downloadfile[]" class="upload" />'
	html += '</td>';
	html += '  <td  class="text-center"><input type="text" name="_dnpfile_name[]" value="" placeholder="File Title" class="form-control" /></td>';
	html += '  <td  class="text-center"><input type="text" name="_dnsort[]" value="" placeholder="Sort Order" class="form-control" /></td>';
	html += '  <td class="text-right"><button type="button" onclick="$(\'#image-row' + downloadfile_row  + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#download_file tbody').append(html);
	
	downloadfile_row++;
}
</script> 
		<tbody id="actions">
		<?php $imageInfo=$this->product_file_m->selectfile($manage[$this->router->fetch_class()]['id'],'downloadfile');?>
		<?php foreach ($imageInfo as $key) {?>
		<tr>
		<td class="text-center">
		<?php if($key->pfile_ext=='.jpg' || $key->pfile_ext=='.png' || $key->pfile_ext=='.gif'){?>
		<img class="img-thumbnail" style="heigth:100px;width:100px;" src="<?php echo base_url();?>../public/img_upload/<?php echo $key->pfile_filename;?>">
		<?php }else{ ?>
		<img class="img-thumbnail" style="heigth:90px;width:90px;" src="<?php echo base_url();?>../public/images/<?php echo $key->pfile_image;?>">
		<?php } ?>
		</td>
		<td class="text-center"><?php echo form_input('_dpfile_name['.$key->pfile_id.']', set_value('_dpfile_name['.$key->pfile_id.']', $key->pfile_name),' class="form-control"'); ?></td>
		<td class="text-center"><?php echo form_input('_dsort['.$key->pfile_id.']', set_value('_dsort['.$key->pfile_id.']', $key->_sort),' class="form-control"'); ?></td>
		<td class="text-right"><a data-original-title="Delete" data-toggle="tooltip" title="" class="btn btn-danger" href="<?php echo site_url();?>product/deletefile/<?php echo $key->pfile_id; ?>?rdirBack=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>#tab-image"
			onclick="if (confirm('Delete selected item?\n\nName:<?php echo $key->pfile_filename; ?>')){return true;}else{event.stopPropagation(); event.preventDefault();};"
			><i class="fa fa-trash-o"></i> Delete</a></td>
		</tr>
<?php }?>
<tfoot>
<tr>
<td></td>
<td></td>
<td></td>
<td class="text-right"><a data-toggle="tooltip" onclick="adddownloadfile();" title="Add Downloads" data-original-title="Add Downloads" class="btn btn-warning"><i class="fa fa-plus"></i></a></td>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false
});
</script>
<script type="text/javascript">
    function newImage(value)
    {
        var holder = document.getElementById("srcimage"+value);
        holder.src = myFunction(value);
    }
   
    function newdownloadfile(value)
    {
        var holder = document.getElementById("srcdownloadfile"+value);
        holder.src = mydownloadfile(value);
    }
</script>
<script>
function myFunction(value) {
    var x = document.getElementById("file"+value).value;
    return x;
}
function mydownloadfile(value) {
    var x = document.getElementById("downloadfile"+value).value;
    return x;
}
</script>
	<style type="text/css">
	.input-style{
padding: 12px 16px !important;
border-left: 4px solid #4CAF50 !important;
margin: 20px 0 !important;
color: black !important;
}
	</style>