<?php 
$rdirBack = $_SERVER['HTTP_HOST'];
$rdirBack .= $_SERVER['REQUEST_URI'];
$rdirBack = str_replace("&", ".and.", "http://".$rdirBack);
?>
 <div class="page-header">
    <div class="container-fluid">
	 <div class="pull-right">
	 <a data-original-title="Add New" href="<?php echo site_url('product/edit'); ?>" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus"></i></a>
     </div>
      <h1>Product</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('product'); ?>">Product</a></li>
      </ul>
    </div>
 </div>
 <div class="container-fluid">
  <div class="panel panel-default">
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Product List</h3>
  </div>
	<div class="panel-body">
<?php echo notification_msg();?>
<?php echo form_open('',array('method'=>'get')); ?>
	<div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-name">Product Name</label>
                <input type="text" name="filter_name" value="<?php echo $this->input->get('filter_name');?>" placeholder="Product Name" id="input-name" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-model">Category</label>
                <?php 
                echo "<select name='filter_category' value='' class='form-control'>";
				foreach ($pages_with_parent as $key => $value) {
					echo "<option value='$key'";
					if($this->input->get('filter_category')==$key){ echo "selected";}
					echo " class='optionGroup'>".$value['title']."</option>";
					if($value['children']){
					foreach ($pages_with_parent[$key]['children'] as $key => $value) {
						echo "<option value='$key'";
						if($this->input->get('filter_category')==$key){ echo "selected";}
						echo " class='optionChild'>&nbsp;&nbsp;&nbsp;".$value['title']."&nbsp;&nbsp;(<i style='float:right'>".$value['topic']."</i>)</option>";
					}
				}
				}
				echo "</select>";
                ?>
                 <style type="text/css">
				.optionGroup
				{
					font-family: monospace;
				    font-weight:bolder !important;
				    color:#14628c !important;
				    font-size: 16px;
				    font-style:italic;
				}
				.optionChild
				{
					font-family: monospace;
				    padding-left:15px;
				    font-size: 14px;
				}
				 </style>
                <ul class="dropdown-menu"></ul>
               
              </div>
            </div>
            <div class="col-sm-4">
            <label class="control-label" for="input-price">Price</label>
              <div class="form-group form-inline">
                
                
                <select class="form-control" value="" name="filter_price_condition">
                	<option value="=">=</option>
                	<option value="<"><</option>
                	<option value="<="><=</option>
                	<option value=">">></option>
                	<option value=">=">>=</option>
                </select>
   
                <input type="text" name="filter_price" value="<?php echo $this->input->get('filter_price')?>" placeholder="Price" id="input-price" class="form-control">
              </div>
              <div class="form-group">
                <label class="control-label" for="input-quantity">Quantity</label>
                <input type="text" name="filter_quantity" value="" placeholder="Quantity" id="input-quantity" class="form-control">
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status">Status</label>
                <select name="filter_status" id="input-status" value="2" class="form-control">
                    <option <?php if($this->input->get('filter_status')==2){ echo "selected";}?> value="2">All</option>
                    <option <?php if($this->input->get('filter_status')==1){ echo "selected";}?> value="1">Enabled</option>
                    <option <?php if($this->input->get('filter_status')==0){ echo "selected";}?> value="0">Disabled</option>
                </select>
              </div>
              <button type="submit" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Filter</button>
            </div>
          </div>
        </div>
        <?php echo form_close(); ?>
	<div class="table-responsive" style="overflow-x: visible;">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th class="text-center"><a href="<?php echo $filter ?>&id=asc" >Id</a></th>
				<th class="text-center">Image</th>
				<th class="text-left"><a href="" >Name <i class="fa fa-sort"></i> <i class="fa fa-sort-asc"></i> 
				<i class="fa fa-sort-desc"></i> </a></th>
				<th class="text-center"><a href="" >Code </th>
				<th class="text-center"><a href="" >Category </th>
				<th class="text-center"><a href="" >Publisher </th>
				<th class="text-center"><a href="" >Status </th>
				<th class="text-right"> Action </th>
			</tr>
		</thead>
		<tbody>
<?php if(count($products)): foreach($products as $product): ?>	
		<tr>
			<td class="text-center"><?php echo $product->id;?></td>
			<td class="text-center"><img class="img-thumbnail" style="heigth:100pc;width:100px;" src="<?php echo base_url();?>../public/img_upload/small/<?php echo $this->product_image_m->singleImage($product->id); ?>"></td>
			<td class="text-left"><?php echo $product->name; ?></td>
			<td class="text-center"><?php echo $product->code; ?></td>
			<td class="text-center"><?php echo $this->page_m->get_cat_name($product->_parent_id); ?></td>
			<td class="text-center"><?php echo $this->admin_users_m->get_name($product->publisher); ?></td>
			<td class="text-center">
			<?php if($product->_status==1){?>
			<a class="list-action-enable action-enabled" title="Enabled" href="<?php echo site_url();?>product/status/<?php echo $product->id;?>"><i class="fa fa-check"></i></a>
			<?php }else if($product->_status==0){?>
			<a class="list-action-enable action-disabled" title="Disabled" href="<?php echo site_url();?>product/status/<?php echo $product->id;?>"><i class="fa fa-times" style="color:#D9534F;"></i></a>
			<?php }?>
			</td>
			
			<td  class="text-right">
			<div class="btn-group-action">
			<div class="btn-group pull-right">
			<a data-original-title="Edit" data-toggle="tooltip" title="" class="edit btn btn-default" href="<?php echo site_url(); ?>product/edit/<?php echo $product->id?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a>
			<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-caret-down"></i>
			</button>
			<ul class="dropdown-menu">
			<li ><a  target="_blank" href="<?php echo site_url();?>../ps/product/<?php echo $product->id;?>" class="btn"><i class="fa fa-eye"></i>&nbsp;Preview</a></li>
			<li class="divider"> </li>
			<li>
			<a data-original-title="Delete" data-toggle="tooltip" title="" class="btn" 
			onclick="if (confirm('Delete selected item?\n\nName : <?php echo $product->name; ?>')){return true;}else{event.stopPropagation(); event.preventDefault();};"
			href="<?php echo site_url();?>product/delete?id=<?php echo $product->id;?>"><i class="fa fa-trash-o"></i>&nbsp;Delete</a>
			</li>
			</ul>
			</div>
			</div>
			</td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td  class="text-center">We could not find any products.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
<nav>
<?php echo $pages_p?>
</nav>
</div>
</div>
</div>
<!-- ?rdirBack=<?php //echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>
?rdirBack=<?php //echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?> -->