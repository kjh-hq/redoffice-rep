 <script src='<?php echo base_url() ?>public/color/spectrum.js'></script>
<link rel='stylesheet' href='<?php echo base_url() ?>public/color/spectrum.css' />
 
 <div class="panel panel-default">
 <div class="page-header">
    <div class="container-fluid">
      <h1>Design</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('admin/dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('admin/dashboard/design'); ?>">Design</a></li>
      </ul>
    </div>
 </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i><?php echo 'Edit Design '?></h3>
  </div>
<?php echo validation_errors(); ?>
<?php echo form_open(); ?>
<div class="panel-body">
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
	
	<tr>
		<td>Background</td>
		<td><?php echo form_input('background', set_value('background', $dashboard->background),'class="preferredHex3"'); ?></td>
	</tr>
		<tr>
		<td>Header</td>
		<td><?php echo form_input('header', set_value('header', $dashboard->header),'class="preferredHex3"'); ?></td>
	</tr>
	<tr>
		<td>Button(Buy Now)</td>
		<td><?php echo form_input('buy_now', set_value('buy_now', $dashboard->buy_now),'class="preferredHex3"'); ?></td>
	</tr>	
	<tr>
		<td>Button(Buy Now:hover)</td>
		<td><?php echo form_input('buy_now_hover', set_value('buy_now_hover', $dashboard->buy_now_hover),'class="preferredHex3"'); ?></td>
	</tr>
	<tr>
		<td>Button(Buy Now:hover) text color</td>
		<td><?php echo form_input('buy_now_hover_color', set_value('buy_now_hover_color', $dashboard->buy_now_hover_color),'class="preferredHex3"'); ?></td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary"'); ?></td>
	</tr>
</table>
<?php echo form_close();?>
</div>
</div>
</div>
<script>
$(".preferredHex3").spectrum({
    preferredFormat: "hex3",
    showInput: true,
    showPalette: true,
    palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
});
</script>