			<div class="span12" data-motopress-wrapper-type="content">
			<h2>News Images</h2>
	
	
	<?php echo anchor('admin/news/edit', '<i class="fa fa-plus"></i> Add an image to slider'); ?>
	<table class="table table-striped">
				<thead>
					<tr>
						<th>Images</th>
						<th>Details</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
<?php if(count($slider4)): foreach($slider4 as $slider): ?>	
		
	
		<tr>
							<td style="font-size: 15px;"><img style="width: 150px;"
								src="<?php echo base_url()?>/public/img_upload/<?php echo $slider->image ?>" /></td>
							<td style="font-size: 22px"><?php echo $slider->body?></td>
							<td style="font-size: 22px"><?php echo btn_edit('admin/news/edit/'.$slider->id)?></td>
							<td style="font-size: 22px"><?php echo btn_delete('admin/news/delete?id='.$slider->id.'&image='.$slider->image)?></td>
						</tr>
		
<?php endforeach; ?>
<?php else: ?>       <tr>
							<td colspan="3">We could not find any news.</td>
						</tr>
<?php endif; ?>	
		</tbody>
			</table>
		</div>
		
		<style>
.pagination {
    border-radius: 4px;
    display: inline-block;
    margin: 20px 0;
    padding-left: 0;
}
.pagination > li {
    display: inline;
}
.pagination > li > a, .pagination > li > span {
    background-color: #fff;
    border: 1px solid #ddd;
    color: #337ab7;
    float: left;
    line-height: 1.42857;
    margin-left: -1px;
    padding: 6px 12px;
    position: relative;
    text-decoration: none;
}
.pagination > li:first-child > a, .pagination > li:first-child > span {
    border-bottom-left-radius: 4px;
    border-top-left-radius: 4px;
    margin-left: 0;
}
.pagination > li:last-child > a, .pagination > li:last-child > span {
    border-bottom-right-radius: 4px;
    border-top-right-radius: 4px;
}
.pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
    background-color: #eee;
    border-color: #ddd;
    color: #23527c;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    background-color: #337ab7;
    border-color: #337ab7;
    color: #fff;
    cursor: default;
    z-index: 2;
}
.pagination > .disabled > span, .pagination > .disabled > span:hover, .pagination > .disabled > span:focus, .pagination > .disabled > a, .pagination > .disabled > a:hover, .pagination > .disabled > a:focus {
    background-color: #fff;
    border-color: #ddd;
    color: #777;
    cursor: not-allowed;
}
.pagination-lg > li > a, .pagination-lg > li > span {
    font-size: 18px;
    padding: 10px 16px;
}
.pagination-lg > li:first-child > a, .pagination-lg > li:first-child > span {
    border-bottom-left-radius: 6px;
    border-top-left-radius: 6px;
}
.pagination-lg > li:last-child > a, .pagination-lg > li:last-child > span {
    border-bottom-right-radius: 6px;
    border-top-right-radius: 6px;
}
.pagination-sm > li > a, .pagination-sm > li > span {
    font-size: 12px;
    padding: 5px 10px;
}
.pagination-sm > li:first-child > a, .pagination-sm > li:first-child > span {
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
}
.pagination-sm > li:last-child > a, .pagination-sm > li:last-child > span {
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
}
</style>
<nav>
<?php echo $pages_p?>
</nav>