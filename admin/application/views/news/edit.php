<h3><?php echo empty($slider4->id) ? 'Add a new news' : 'Edit news ' . $slider4->title; ?></h3>
<?php echo validation_errors(); ?>
<h1>
<?php echo $slider4->id?>
</h1>
<?php echo form_open(); ?>
<table class="table">
	<tr>
		<td>Title</td>
		<td><?php echo form_input('title', set_value('title', $slider4->title)); ?></td>
	</tr>	
	<tr>
		<td>Body</td>
		<td><?php echo form_textarea('body', set_value('body', $slider4->body),'class="tinymce"'); ?></td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary"'); ?></td>
	</tr>
</table>
<?php echo form_close();?>
