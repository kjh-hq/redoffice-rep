	<table id="images" class="table table-bordered table-hover">
	<thead>
			<tr>
				<th width="120" class="text-center">Image</th>
				<th class="text-center"><a class="asc" href="" >Sort</a></th>
				<th width="100" class="text-center"> Details </th>
				<th width="200" class="text-right"> Action </th>
			</tr>
		</thead>
		<tbody id="actions">
  <?php
        $i = 1;
        if (count($manage[$this->router->fetch_class()])) {
            foreach ($manage[$this->router->fetch_class()] as $data) {
                ?>
		<tr>
		<td class="text-center">
		<?php if (!empty($data['image'])) { ?>
                            <img style="width: 100%;" title="<?php echo base_url();?>../public/img_upload/<?php echo $data['image']; ?>" src="<?php echo base_url();?>../public/img_upload/<?php echo $data['image']; ?>">
                        <?php } else { ?>
                            <span  title="<?php echo base_url();?>../public/img_upload/<?php echo $data['image']; ?>" class='label label-danger'>No Image</span>
                        <?php } ?>
		</td>
		<td class="text-center"><?php echo form_input('_sort['.$data['id'].']', set_value('_sort['.$data['id'].']', $data['_sort']),' class="form-control"'); ?></td>
		<td class="text-center">
		Title 1 : <?php echo $data['title1'];?></br>
		Title 2 : <?php echo $data['title2'];?>
		</td>
		<td class="text-right">
<div class="btn-group-action">
			<div class="btn-group pull-right">
			<a data-original-title="Edit" data-toggle="tooltip" title="" class="edit btn btn-default" href="<?php echo site_url('slider/edit/'.$data['id']); ?>"><i class="fa fa-pencil"></i>&nbsp;Edit</a>
			<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-caret-down"></i>
			</button>
			<ul class="dropdown-menu">
			<li><a href="" data-original-title="Preview" data-toggle="tooltip"  class="btn btn-info"><i class="fa fa-eye"></i>&nbsp;Preview</a></li>
			<li class="divider"> </li>
			<li>
			<a data-original-title="Delete" data-toggle="tooltip" title="" class="btn btn-danger" 
			onclick="if (confirm('Delete selected item?')){return true;}else{event.stopPropagation(); event.preventDefault();};"
			href="<?php echo site_url('slider/delete?id='.$data['id']); ?>"><i class="fa fa-trash-o"></i>&nbsp;Delete</a>
			</li>
			</ul>
			</div>
			</div>
		</td>
		</tr>
        <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3" class="text-center">We could not find any  <?php echo ucwords($page_title[$this->uri->segment(1)]) ?>.</td>
            </tr>
        <?php } ?>	
    </tbody>
</table>