<div class="panel panel-default">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                 <?php echo btn_add($perm_button, 'user/edit',  null , '','<i class="fa fa-plus"></i>', 'data-original-title="Add New" data-toggle="tooltip" type="button" class="btn btn-primary dim"'); ?>
            </div>
             <h1><?php echo $page_title[$this->uri->segment(1)] ?></h1>
            <ul class="breadcrumb">
                <li><a href="<?php echo site_url($page_title['home_slug']); ?>"><?php echo $page_title['home']; ?></a></li>
                <li><a href="<?php echo site_url($this->uri->segment(1)); ?>"><?php echo $page_title[$this->uri->segment(1)] ?></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="fa fa-list"></i> &nbsp;<?php echo $page_title[$this->uri->segment(1)] ?> List
        </h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Authentication Level</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($users)): foreach ($users as $data): ?>	
                           
                                <tr>
                                    <td><?php echo $data->admin_id; ?></td>
                                    <td><?php echo $data->admin_name; ?></td>
                                    <td><?php echo $data->admin_email; ?></td>
                                    <td><?php echo $data->admin_email; ?></td>
                                    <td class="text-right">
                                        <?php echo btn_edit($perm_button, 'user/edit/(:num)', $data->admin_id, '' ,'<i class="fa fa-pencil"></i>'); ?>
                                <?php echo btn_delete($perm_button, 'user/delete/(:num)', $data->admin_id,'' , '<i class="fa fa-remove"></i>'); ?>
                                     
                                    </td>
                                </tr>
                           
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="3">We could not find any users.</td>
                        </tr>
                    <?php endif; ?>	
                </tbody>
            </table>
        </div>
    </div>
</div>