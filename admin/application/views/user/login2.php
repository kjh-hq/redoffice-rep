<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login or Sign Up | <?php echo $meta_title; ?></title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    </head>
    <body class="gray-bg">
        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <h3>Welcome to <?php echo $site_name; ?></h3>
                <p>Login in. To see it in action.</p>
                <?php echo notification_msg(); ?>
                <?php echo form_open(); ?>  
                <div class="form-group">
                    <?php echo form_input('username', '', 'class="form-control" id="input-username" placeholder="Mobile"'); ?>
                </div>
                <div class="form-group">
                    <?php echo form_password('password', '', ' class="form-control" id="input-password" placeholder="Password" '); ?>
                </div>
                <div class="form-group">
                    <?php echo $cap['image']; ?>&nbsp;
                    <button class="btn btn-primary" type="submit"><i class="fa fa-refresh"></i></button>
                    <?php echo form_input('cap', set_value('cap', ''), "class='form-control' style='text-align: center;' placeholder='Captcha'"); ?> 
                    <style>
                        #icon {
                            background: url('<?php echo base_url(); ?>assets/sys_images/refresh.png') 3px 3px  no-repeat;
                            width:30px;
                            height:30px;
                            cursor:pointer;
                        }
                    </style>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <a href="<?php echo site_url(); ?>checkin/forgot_password"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="<?php echo site_url(); ?>checkin/registration">Create an account</a>
                <?php echo form_close(); ?>
                <p class="m-t"> <small><?php echo $meta_title; ?> &copy; <?php echo date('Y') ?></small> </p>
            </div>
        </div>
        <!-- Mainly scripts -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    </body>
</html>
