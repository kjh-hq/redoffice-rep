<div class="panel panel-default">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <?php echo btn_add($perm_button, 'user/edit', null , '', '<i class="fa fa-plus"></i>', 'data-original-title="Add New" data-toggle="tooltip" type="button" class="btn btn-primary dim"'); ?>
            </div>
            <h1><?php echo $page_title[$this->uri->segment(1)] ?></h1>
            <ul class="breadcrumb">
                <li><a href="<?php echo site_url($page_title['home_slug']); ?>"><?php echo $page_title['home']; ?></a></li>
                <li><a href="<?php echo site_url($this->uri->segment(1)); ?>"><?php echo $page_title[$this->uri->segment(1)] ?></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i><?php echo ' Edit profile of ' . $user->admin_name; ?></h3>
    </div>
    <?php echo notification_msg(); ?>
    <?php echo form_open(); ?>
    <div class="panel-body">
        <div class="table table-bordered table-hover">
            <table class="table">
                <tr>
                    <td>First Name</td>
                    <td><?php echo form_input('admin_name', set_value('admin_name', $user->admin_name), 'class="form-control"'); ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?php echo form_input('admin_email', set_value('admin_email', $user->admin_email), 'class="form-control"'); ?></td>
                </tr>
                <tr>
                    <td>Authentication Level</td>
                    <td><?php echo form_dropdown('admin_permission_id', $admin_profile, 'class="form-control"'); ?></td>
                </tr>
                <tr>
                    <td>Alternative Mobile Number</td>
                    <td><?php echo form_input('admin_recover_mobile_no', set_value('admin_recover_mobile_no', $user->admin_recover_mobile_no), 'class="form-control"'); ?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><?php echo form_input('admin_address', set_value('admin_address', $user->admin_address), 'class="form-control"'); ?></td>
                </tr>
                <tr>
                    <td>Country</td>
                    <td><?php echo form_input('admin_country_id', set_value('admin_country_id', $user->admin_country_id), 'class="form-control"'); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary"'); ?></td>
                </tr>
            </table>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>