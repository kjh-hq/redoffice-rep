<?php $this->load->view('component/header');?>
<div id="container">
<header class="navbar navbar-static-top" id="header">
  <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo site_url();?>user/login">
        <img title="AdminPanel" alt="AdminPanel" style="width: 60px;" src="<?php echo base_url();?>assets/image/logo.png"></a></div>
  </header>
<div id="content">
  <div class="container-fluid"><br>
    <br>
    <div class="row">
      <div class="col-sm-offset-4 col-sm-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h1 class="panel-title"><i class="fa fa-lock"></i> Please enter your login details.</h1>
          </div>
          <div class="panel-body">
 <?php echo notification_msg(); ?>
<?php echo form_open(); ?>
              <div class="form-group">
                <label for="input-username">Email</label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <?php echo form_input('username', '', 'class="form-control" id="input-username" placeholder="Mobile"'); ?>
                </div>
              </div>
              <div class="form-group">
                <label for="input-password">Password</label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                  <?php echo form_password('password','',' class="form-control" id="input-password" placeholder="Password" '); ?>
                </div>
                              </div>
                              <div class="form-group">
                    <?php echo $cap['image']; ?>&nbsp;
                    <button class="btn btn-primary" type="submit"><i class="fa fa-refresh"></i></button>
                    <?php echo form_input('cap', set_value('cap', ''), "class='form-control' style='text-align: center;' placeholder='Captcha'"); ?> 
                    <style>
                        #icon {
                            background: url('<?php echo base_url(); ?>assets/sys_images/refresh.png') 3px 3px  no-repeat;
                            width:30px;
                            height:30px;
                            cursor:pointer;
                        }
                    </style>
                </div>
              <div class="text-right">
                <button class="btn btn-primary" type="submit"><i class="fa fa-key"></i> Login</button>
              </div>
                     <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer id="footer">
<a href="#">JProgramming</a> &copy; 2014-<?php echo date('Y');?> All Rights Reserved.<br>
</footer>
</div>