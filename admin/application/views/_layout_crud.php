<?php $this->load->view('component/header');?>
<div id="container">
<header id="header" class="navbar navbar-static-top">
<div class="navbar-header">
<a type="button" id="button-menu" class="pull-left">
<i class="fa fa-dedent fa-lg">
</i>
</a>
<a href="" class="navbar-brand">
<img src="<?php echo base_url();?>assets/image/logo.png" style="width: 35px;" alt="KJHProgrammer" title="KJHProgrammer">
</a>
</div>
<!-- <span id="rtime" style="color:#fff"></span> -->
<ul class="nav pull-right">
<script>
// setInterval(function(){
//     document.getElementById("rtime").innerHTML = (new Date()).toLocaleString();
// }, 1000);
// setInterval(); 
</script>
<li>
<a href="
<?php echo site_url('user/logout'); ?>">
<span class="hidden-xs hidden-sm hidden-md">Logout
</span> 
<i class="fa fa-sign-out fa-lg">
</i>
</a>
</li>
</ul>
</header>
<nav class="active" id="column-left">
<ul id="menu">
<?php echo get_menu($ap_menu) ?>
</ul>
</nav>
<div id="content">
<?php $this->load->view('_layout_' . $this->router->fetch_method() . '_header'); ?>
<?php $this->load->view($subview); ?>
<?php $this->load->view('_layout_' . $this->router->fetch_method() . '_footer'); ?>
<?php $this->load->view('component/footer');?>
