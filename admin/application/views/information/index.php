<table class="table table-bordered table-hover">
    <thead>
        <tr>

            <td width="120" class="text-center">Snippet</td>
            <td class="text-left">Details</td>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($manage[$this->router->fetch_class()])) {
            foreach ($manage[$this->router->fetch_class()] as $data) {
                ?>	
                <tr>
                    <td  class="text-right"><?php echo $data['snippets_title']; ?></td>
                    <td  class="text-left"><?php echo $data['snippets_details']; ?></td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3" class="text-center">We could not find any  <?php echo ucwords($page_title[$this->uri->segment(1)]) ?>.</td>
            </tr>
        <?php } ?>	
    </tbody>
</table>