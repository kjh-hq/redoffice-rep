 <div class="panel panel-default">
 <div class="page-header">
    <div class="container-fluid">
	 <div class="pull-right">
    
     </div>
      <h1>Banner</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('banner'); ?>">Banner List</a></li>
      </ul>
    </div>
 </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i>Banner List</h3>
  </div>
<?php echo validation_errors(); ?>
<!-- 
<script type="text/javascript" src="<?php echo base_url();?>public/shop/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/shop/jquery.selectboxes.pack.js"></script>
 -->
	<div class="panel-body">
	<div class="table-responsive" style="overflow-x:visible">
	<table id="images" class="table table-bordered table-hover">
	<thead>
			<tr>
				<th class="text-center">Image</th>
				<th class="text-center"><a class="asc" href="" >Name</a></th>
				<th class="text-center">Links</th>
				<th class="text-center">New Image</th>
				<th class="text-right"> Action </th>
			</tr>
		</thead>
		<tbody id="actions">
		<?php foreach ($banner as $key) {?>
		<?php echo form_open_multipart(); ?>
		<tr>
		<td class="text-center"><img class="img-thumbnail" style="heigth:100px;width:100px;" src="<?php echo base_url();?>../public/img_upload/<?php echo $key->image;?>"></td>
		<td class="text-center"><?php echo form_input('name', set_value('name', $key->name),' class="form-control"'); ?>
		<td class="text-center"><?php echo form_input('link', set_value('link', $key->link),' class="form-control"'); ?>
		<input type="hidden" name="id" value="<?php echo $key->id;?>">
		</td>
		<td class="text-center"><input type="file" name="userfile" ></td>
		<td class="text-right">
		<button type="submit" name="submit" data-toggle="tooltip" data-original-title="Change" class="btn btn-primary"><i class="fa fa-exchange"></i></i>&nbsp;Change</button>
		</td>
		</tr>
		<?php echo form_close();?>
<?php }?>
<tfoot>
</tfoot>
</div>
</div>
</div>
</div>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script>
<script type="text/javascript">
    function newImage(value)
    {
        var holder = document.getElementById("srcimage"+value);
        holder.src = myFunction(value);
    }
</script>
<script>
function myFunction(value) {
    var x = document.getElementById("file"+value).value;
    return x;
}
</script>