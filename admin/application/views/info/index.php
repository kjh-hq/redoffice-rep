 <div class="panel panel-default">
 <div class="page-header">
    <div class="container-fluid">
	 <div class="pull-right">
	 <a data-original-title="Edit" href="<?php echo site_url('dashboard/info_edit'); ?>" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
     </div>
      <h1>Information</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('dashboard/info'); ?>">Information</a></li>
      </ul>
    </div>
 </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i>Information List</h3>
  </div>
	<div class="panel-body">
	<div class="table-responsive">
	<table class="table table-bordered table-hover">

		<thead>
			<tr>
				<td class="text-left">Title</td>
				<td class="text-center">Details</td>
				<td class="text-center">About</td>
	
			
			</tr>
		</thead>
		<tbody>

		<tr>
			<td  class="text-left">Phone Number</td>
			<td  class="text-left"><?php echo $dashboard->phone ?></td>
		</tr>
		<tr>
			<td  class="text-left">Email</td>
			<td  class="text-left"><?php  echo $dashboard->email  ?></td>
		</tr>


		</tbody>
	</table>
</div>
</div>
</div>