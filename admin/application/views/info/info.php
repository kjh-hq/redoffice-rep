 <div class="panel panel-default">
 <div class="page-header">
    <div class="container-fluid">
      <h1>Information</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('admin/dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('admin/dashboard/info'); ?>">Information</a></li>
      </ul>
    </div>
 </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i><?php echo 'Edit Information '?></h3>
  </div>

<?php echo validation_errors(); ?>
<?php echo form_open(); ?>
<div class="panel-body">
	<div class="table-responsive">
	<table class="table table-bordered table-hover">
	
	<tr>
		<td>Phone Number</td>
		<td><?php echo form_input('phone', set_value('phone', $dashboard->phone)); ?></td>
	</tr>
	
	<tr>
		<td>Phone Number</td>
		<td><?php echo form_input('email', set_value('email', $dashboard->email)); ?></td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary"'); ?></td>
	</tr>
</table>
<?php echo form_close();?>
</div>
</div>
</div>