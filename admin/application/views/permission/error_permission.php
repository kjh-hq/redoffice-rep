<!DOCTYPE html>
<html class="error-page">
    <head>
        <title>Access Denied | JAdmin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" media="screen" href="<?php echo base_url();?>/jadmin/stylesheets/jAdmin.min.css">
        <link rel="stylesheet" media="screen" href="<?php echo base_url();?>/jadmin/stylesheets/jAdmin-theme.min.css">
        <link rel="stylesheet" media="screen" href="<?php echo base_url();?>/jadmin/stylesheets/jAdmin-admin-theme.css">
        <link rel="stylesheet" media="screen" href="<?php echo base_url();?>/jadmin/stylesheets/jAdmin-admin-theme-change-size.css">
        <link rel="stylesheet" media="screen" href="<?php echo base_url();?>/jadmin/stylesheets/jAdmin-error-page.css">
    </head>
    <body>
        <!-- content -->
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-lg-12">
                    <div class="centering text-center">
                        <div class="text-center">
                            <h2 class="without-margin">Don't worry. It's <span class="text-danger"><big>Permission</big></span> error only.</h2>
                            <h4 class="text-danger">Please contact Superadmin</h4>
                        </div>
                        <div class="text-center">
                            <h3><small>Choose an option below</small></h3>
                        </div>
                        <hr>
                       <ul class="pager">
                            <!-- <li><a href="about.html">&larr; About</a></li> -->
                            <li><a href="<?php echo site_url()?>">Home</a></li>
                            <!-- <li><a href="error-pages.html">Other error pages &rarr;</a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer -->
        <div class="navbar navbar-footer navbar-fixed-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <footer role="contentinfo">
                            <p class="left">Byson Theme</p>
                            <p class="right">&copy; <?php echo date('Y');?> <a href="https://www.bysontech.com" target="_blank">BYSON TECHNOLOGIES</a></p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
 <?php //echo $severity; ?>
 <?php //echo $message; ?>
<?php //echo $filepath; ?>
<?php //echo $line; ?>
