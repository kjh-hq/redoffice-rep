<div class="panel panel-default">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $page_title[$this->uri->segment(1)] ?></h1>
            <ul class="breadcrumb">
                <li><a href="<?php echo site_url($page_title['home_slug']); ?>"><?php echo $page_title['home']; ?></a></li>
                <li><a href="<?php echo site_url($this->uri->segment(1) . ($this->uri->segment(2) != NULL ? '/' . $this->uri->segment(2) : '')); ?>"><?php echo $page_title[$this->uri->segment(1) . ($this->uri->segment(2) != NULL ? '/' . $this->uri->segment(2) : '')] ?></a></li>
            </ul>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/permission/jquery-1.11.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/permission/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/permission/jquery.alerts.js"></script>
    <script src="<?php echo base_url(); ?>assets/permission/jquery.chosen.js"></script>
    <script src="<?php echo base_url(); ?>assets/permission/jquery.growl.js"></script>
    <script src="<?php echo base_url(); ?>assets/permission/admin.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/permission/admin-theme.css">
    <script type="text/javascript">
        $(document).ready(function () {
            $('div.productTabs').find('a').each(function () {
                $(this).attr('href', '#');
            });
            $('div.productTabs a').click(function () {
                var id = $(this).attr('id');
                $('.nav-profile').removeClass('selected');
                $(this).addClass('selected active');
                $(this).siblings().removeClass('active');
                $('.tab-profile').hide()
                $('.' + id).show();
            });
            $('.ajaxPower').change(function () {
                var tout = $(this).data('rel').split('||');
                var id_tab = tout[0];
                var id_profile = tout[1];
                var perm = tout[2];
                var enabled = $(this).is(':checked') ? 1 : 0;
                var tabsize = tout[3];
                var tabnumber = tout[4];
                var table = 'table#table_' + id_profile;
                if (perm == 'all' && $(this).parent().parent().hasClass('parent'))
                {
                    if (enabled) {
                        $(this).parent().parent().parent().find('.child-' + id_tab + ' input[type=checkbox]').removeAttr("checked");
                        $(this).parent().parent().parent().find('.child-' + id_tab + ' input[type=checkbox]').attr("checked", "checked");
                    } else {
                        $(this).parent().parent().parent().find('.child-' + id_tab + ' input[type=checkbox]').removeAttr("checked");
                    }
                    $.ajax({
                        url: "permission/save?controller=AdminAccess",
                        cache: false,
                        data: {
                            ajaxMode: '1',
                            id_tab: id_tab,
                            id_profile: id_profile,
                            perm: perm,
                            enabled: enabled,
                            submitAddAccess: '1',
                            addFromParent: '1',
                            action: 'updateAccess',
                            ajax: '1'
                        },
                        success: function (res, textStatus, jqXHR)
                        {
                            try {
                                if (res == 'ok')
                                    showSuccessMessage("Update successful");
                                else
                                    showErrorMessage("Update error");
                            } catch (e) {
                                jAlert('Technical error');
                            }
                        }
                    });
                }
                perfect_access_js_gestion(this, perm, id_tab, tabsize, tabnumber, table);
                $.ajax({
                    url: "permission/save?controller=AdminAccess",
                    cache: false,
                    data: {
                        ajaxMode: '1',
                        id_tab: id_tab,
                        id_profile: id_profile,
                        perm: perm,
                        enabled: enabled,
                        submitAddAccess: '1',
                        action: 'updateAccess',
                        ajax: '1'
                    },
                    success: function (res, textStatus, jqXHR)
                    {
                        try
                        {
                            if (res == 'ok')
                                showSuccessMessage("Update successful");
                            else
                                showErrorMessage("Update error");
                        }
                        catch (e)
                        {
                            jAlert('Technical error');
                        }
                    }
                });
            });
        });
    </script>
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i>Permission List</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="productTabs col-lg-2">
                        <div class="tab list-group">
                            <a id="super_admin" class="list-group-item nav-profile active" href="">Super Admin</a>
                            <?php foreach ($admin_permissions_profile as $data) { ?>
                                <a id="profile-<?php echo $data->prof_id; ?>" class="list-group-item nav-profile " href="#profile-<?php echo $data->prof_id; ?>"><?php echo $data->prof_name; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                    <div  class="super_admin tab-profile">
                        <div class="row">			
                            <div class="col-lg-4">
                                <div class="panel">
                                    Super Administrator permissions cannot be modified.
                                </div>
                            </div>
                        </div>
                    </div>
                    <form id="access_form" class="defaultForm form-horizontal col-lg-10" enctype="multipart/form-data" method="post" action="permission/save?controller=AdminAccess&submitAddaccess=1&token=<?php echo $this->session->userdata('cisession') ?>">
                        <?php foreach ($admin_permissions_profile as $data) { ?>
                            <div  style="display:none" class="profile-<?php echo $data->prof_id; ?> tab-profile">
                                <div class="row">			
                                    <div class="col-lg-6">	
                                        <div class="panel-body">
                                            <h3>Menu</h3>
                                            <a data-original-title="Edit" data-toggle="tooltip" title="" class="btn btn-primary" href="<?php echo site_url('permission/edit/' . $data->prof_id); ?>"><i class="fa fa-pencil"></i></a>
                                            <a data-original-title="Delete" onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');" data-toggle="tooltip" title="" class="btn btn-danger" href="<?php echo site_url('permission/delete/' . $data->prof_id); ?>"><i class="fa fa-trash-o"></i></a>
                                            <table id="table_<?php echo $data->prof_id; ?>" class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>
                                                            <input type="checkbox" data-rel="-1||<?php echo $data->prof_id; ?>||view||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>" class="viewall ajaxPower" name="1">
                                                            View
                                                        </th>
                                                        <th>
                                                            <input type="checkbox" data-rel="-1||<?php echo $data->prof_id; ?>||add||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>" class="addall ajaxPower" name="1">
                                                            Add
                                                        </th>
                                                        <th>
                                                            <input type="checkbox" data-rel="-1||<?php echo $data->prof_id; ?>||edit||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>" class="editall ajaxPower" name="1">
                                                            Edit
                                                        </th>
                                                        <th>
                                                            <input type="checkbox" data-rel="-1||<?php echo $data->prof_id; ?>||delete||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>" class="deleteall ajaxPower" name="1">
                                                            Delete
                                                        </th>
                                                        <th>
                                                            <input type="checkbox" data-rel="-1||<?php echo $data->prof_id; ?>||all||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>" class="allall ajaxPower" name="1">
                                                            All
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $perm_data = $this->admin_permissions_m->get_perm($data->prof_id);
                                                    foreach ($perm_data as $perm_data) {
                                                        ?>
                                                        <tr class="parent">
                                                            <td class="bold"><strong>>&nbsp;&nbsp;<?php echo $perm_data->ap_name; ?></strong></td>
                                                            <td>
                                                                <input type="checkbox" <?php echo ($perm_data->perm_view == 1 ? "checked='checked'" : ""); ?> class="ajaxPower view <?php echo $perm_data->perm_id; ?>" data-rel="<?php echo $perm_data->perm_id; ?>||<?php echo $perm_data->perm_profile; ?>||view||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>">
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" <?php echo ($perm_data->perm_add == 1 ? "checked='checked'" : ""); ?>  class="ajaxPower add <?php echo $perm_data->perm_id; ?>" data-rel="<?php echo $perm_data->perm_id; ?>||<?php echo $perm_data->perm_profile; ?>||add||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>">
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" <?php echo ($perm_data->perm_edit == 1 ? "checked='checked'" : ""); ?>  class="ajaxPower edit <?php echo $perm_data->perm_id; ?>" data-rel="<?php echo $perm_data->perm_id; ?>||<?php echo $perm_data->perm_profile; ?>||edit||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>">
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" <?php echo ($perm_data->perm_delete == 1 ? "checked='checked'" : ""); ?>  class="ajaxPower delete <?php echo $perm_data->perm_id; ?>" data-rel="<?php echo $perm_data->perm_id; ?>||<?php echo $perm_data->perm_profile; ?>||delete||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>">
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" <?php echo ($perm_data->perm_all == 1 ? "checked='checked'" : ""); ?>  class="ajaxPower all <?php echo $perm_data->perm_id; ?>" data-rel="<?php echo $perm_data->perm_id; ?>||<?php echo $perm_data->perm_profile; ?>||all||<?php echo $total_perm + 1; ?>||<?php echo $total_perm; ?>">
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">	
                                        <div class="panel-body">
                                            <h3>Module</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        ?>
                    </form>
                    <div id="growls" class="default"></div>
                </div>
            </div>
        </div>
    </div>
</div>