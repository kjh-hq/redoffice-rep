<div class="panel panel-default">
    <div class="page-header">
        <div class="container-fluid">
             <h1><?php echo $page_title[$this->uri->segment(1)] ?></h1>
            <ul class="breadcrumb">
                <li><a href="<?php echo site_url($page_title['home_slug']); ?>"><?php echo $page_title['home']; ?></a></li>
                <li><a href="<?php echo site_url($this->uri->segment(1)); ?>"><?php echo $page_title[$this->uri->segment(1)] ?></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-edit"></i><?php echo empty($admin_permissions_profile->prof_id) ? 'Add a new Admin Permissions Profile ' : ' Edit  Admin Permissions Profile <strong>' . $admin_permissions_profile->prof_name.'</strong>'; ?></h3>
    </div>
    <?php echo notification_msg(); ?>
    <?php echo form_open(); ?>
    <div class="panel-body">
        <table class="table table-bordered table-hover">
            <tr>
                <td>Profile Name</td>
                <td><?php echo form_input('prof_name', set_value('prof_name', $admin_permissions_profile->prof_name),'class="form-control"'); ?></td>
            </tr>
            <tr>
                <td>Sort</td>
                <td><?php echo form_input('_sort', set_value('_sort', $admin_permissions_profile->_sort),'class="form-control"'); ?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary"'); ?></td>
            </tr>
        </table>
        <?php echo form_close(); ?>
    </div>
</div>
