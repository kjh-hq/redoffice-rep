<div class="panel panel-default">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <?php echo btn_add($perm_button, 'permission/edit', null , '', '<i class="fa fa-plus"></i>', 'data-original-title="Add New" data-toggle="tooltip" type="button" class="btn btn-primary dim"'); ?>
            </div>
            <h1><?php echo $page_title[$this->uri->segment(1)] ?></h1>
            <ul class="breadcrumb">
                <li><a href="<?php echo site_url($page_title['home_slug']); ?>"><?php echo $page_title['home']; ?></a></li>
                <li><a href="<?php echo site_url($this->uri->segment(1)); ?>"><?php echo $page_title[$this->uri->segment(1)] ?></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> &nbsp;<?php echo $page_title[$this->uri->segment(1)] ?> List</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td>Permission Profile</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>SuperAdmin</td>
                        <td></td>
                    </tr>
                    <?php foreach ($admin_permissions_profile as $data) { ?>
                        <tr>
                            <td><?php echo $data->prof_name; ?></td>
                            <td>
                                <?php echo btn_edit($perm_button, 'permission/edit/(:num)', $data->prof_id, '', '<i class="fa fa-pencil"></i>'); ?>
                                <?php echo btn_delete($perm_button, 'permission/delete/(:num)', $data->prof_id, '', '<i class="fa fa-remove"></i>'); ?>
    <!--                                <a data-original-title="Edit" data-toggle="tooltip" title="" class="btn btn-primary" href="<?php echo site_url('permission/edit/' . $data->prof_id); ?>"><i class="fa fa-pencil"></i></a>
                                <a data-original-title="Delete" onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');" data-toggle="tooltip" title="" class="btn btn-danger" href="<?php echo site_url('permission/delete/' . $data->prof_id); ?>"><i class="fa fa-trash-o"></i></a>
                                -->
                                <?php
                                $perm_missed = $this->admin_page_m->perm_miss($data->prof_id);
                                if ($perm_missed) {
                                    ?>
                                    <a data-original-title="(<?php echo $perm_missed; ?> page missed) Sync" onclick="return confirm('<?php echo $perm_missed; ?> page missed.\nThis permissions will be added if you synchronize.\nYou really want to synchronize?');" data-toggle="tooltip" title="" class="btn btn-warning" href="<?php echo site_url('permission/sync/' . $data->prof_id); ?>"><i class="fa fa-recycle"></i></a>
                                <?php } ?>
                                <?php
                                $perm_extra = $this->admin_page_m->perm_extra($data->prof_id);
                                if ($perm_extra) {
                                    ?>
                                    <a data-original-title="(<?php echo $perm_extra; ?> page extra) Sync" onclick="return confirm('<?php echo $perm_extra; ?> page extra.\nThis permissions will be deleted if you synchronize.\nYou really want to synchronize?');"  data-toggle="tooltip" title="" class="btn btn-warning" href="<?php echo site_url('permission/sync/' . $data->prof_id); ?>"><i class="fa fa-recycle"></i></a>
                                    <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </div>
                    </div>
