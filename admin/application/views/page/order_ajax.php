<?php
echo get_ol($pages);
function get_ol ($array, $child = FALSE)
{
	$str = '';
	
	if (count($array)) {
		$str .= $child == FALSE ? '<ol class="sortable">' : '<ol>';
		
		foreach ($array as $item) {
			$str .= '<li id="list_' . $item['id'] .'">';
			$str .= '<div>' . $item['title'];
			$str .='<a style="float:right;padding: 1px 14px;" data-original-title="Delete" data-toggle="tooltip" title="" class="btn btn-danger" href="'.site_url('page/delete/' .$item['id']).'"><i class="fa fa-trash-o"></i></a>
			<a style="float:right;padding: 1px 14px;margin: 0 5px;" data-original-title="Edit" data-toggle="tooltip" title="" class="btn btn-primary" href="'.site_url('page/edit/' . $item['id']).'"><i class="fa fa-pencil"></i></a>';
			if($item['id']['q']>0){
			$str .='<span  style="float:right;padding: 6px 14px;margin: 0 5px;" class="label label-success">'. $item['id']['q'].'</span>';
			 }else{
			$str .='<span  style="float:right;padding: 6px 14px;margin: 0 5px;" class="label label-danger">'.$item['id']['q'].'</span>';
			 }
			$str .= '</div>';
			if (isset($item['children']) && count($item['children'])) {
				$str .= get_ol($item['children'], TRUE);
			}
			
			$str .= '</li>' . PHP_EOL;
		}
		
		$str .= '</ol>' . PHP_EOL;
	}
	
	return $str;
}
?>
<script>
$(document).ready(function(){
    $('.sortable').nestedSortable({
        handle: 'div',
        items: 'li',
        toleranceElement: '>div',
        maxLevels: 2
    });
});
</script>