	<tr>
		<td width="200">Parent</td>
		<td><?php echo form_dropdown('_parent_id', $all_menu, $this->input->post('_parent_id') ? $this->input->post('_parent_id') : $manage[$this->router->fetch_class()]['_parent_id'], 'class="form-control"'); ?></td>
	</tr>
    <tr>
        <td>Title</td>
        <td><?php echo form_input('title', set_value('title', $this->input->post('title') ? $this->input->post('title') : $manage[$this->router->fetch_class()]['title'],false), 'class="form-control"'); ?></td>
    </tr>	
	<tr>
		<td>Layout</td>
		<td><?php echo form_dropdown('_layout', $layout, $this->input->post('_layout') ? $this->input->post('_layout') : $manage[$this->router->fetch_class()]['_layout'], 'class="form-control"'); ?></td>
	</tr>
    <tr>
        <td>Link</td>
        <td><?php echo form_input('_link', set_value('_link', $this->input->post('_link') ? $this->input->post('_link') : urldecode($manage[$this->router->fetch_class()]['_link']),false), 'class="form-control"'); ?></td>
    </tr>
    <tr>
        <td>Type</td>
        <td><?php echo form_dropdown('topic', array(''=>'None','product'=>'Product','solution'=>'Solution','technology'=>'Technology','support'=>'Support','project_referrance'=>'Project Referrance'), $this->input->post('topic') ? $this->input->post('topic') : $manage[$this->router->fetch_class()]['topic'], 'class="form-control"'); ?></td>
    </tr>
    <tr>
    
        <td>Fullwidth-Class</td>
            <td><?php echo form_dropdown('fullwidthclass', array('0'=>'None','fullwidth fullwidth-grey banner'=> 'fullwidth fullwidth-grey banner','fullwidth fullwidth-dark banner'=>'fullwidth fullwidth-dark banner','fullwidth banner'=>'fullwidth banner','fullwidth fullwidth-light banner'=>'fullwidth fullwidth-light banner','fullwidth fullwidth-light fullwidth-centered'=>'fullwidth fullwidth-light fullwidth-centered'), $this->input->post('fullwidthclass') ? $this->input->post('fullwidthclass') : $manage[$this->router->fetch_class()]['fullwidthclass'],' class="form-control"'); ?></td>
    </tr>
    <tr>
    <td>Short Description</td>
    <td><?php echo form_textarea('short_description', $this->input->post('short_description') ? $this->input->post('short_description') : $manage[$this->router->fetch_class()]['short_description'], 'class="tinymce form-control"'); ?></td>
</tr>
	<tr>
    <td>Description</td>
    <td><?php echo form_textarea('description', $this->input->post('description') ? $this->input->post('description') : $manage[$this->router->fetch_class()]['description'], 'class="tinymce form-control"'); ?></td>
</tr>
    <td>Image</td>
    <td>
        <?php
        $data_form = array('type' => 'file', 'name' => 'userfile');
        echo form_upload($data_form);
        ?> 
    </td>
</tr>
<?php if (!empty($manage[$this->router->fetch_class()]['image'])) { ?>
    <tr>
        <td></td>
        <td>
            <img style="width: 150px;height: 150px" src="<?php echo base_url();?>../public/img_upload/<?php echo $manage[$this->router->fetch_class()]['image']; ?>">
            <div class="checkbox checkbox-danger">
                <input name="rm[image]" id="image" type="checkbox">
                <label for="image">
                    DELETE IMAGE
                </label>
            </div>
            <p>
                <?php echo base_url();?>../public/img_upload/<?php echo $manage[$this->router->fetch_class()]['image']; ?>
            </p>
        </td>
    </tr> 
<?php } ?>