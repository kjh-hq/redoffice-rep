 <div class="panel panel-default">
 <div class="page-header">
    <div class="container-fluid">
      <h1>Menu</h1>
      <ul class="breadcrumb">
                <li><a href="<?php echo site_url('dashboard'); ?>">Home</a></li>
                <li><a href="<?php echo site_url('page'); ?>">Menu</a></li>
      </ul>
    </div>
 </div>
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> Order pages</h3>
  </div>
	<div class="panel-body">
	<div class="table-responsive">
	
	<link rel="stylesheet" media="screen" href="<?php echo base_url();?>assets/css/admin.css">
	<?php if(isset($sortable) && $sortable === TRUE): ?>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.mjs.nestedSortable.js"></script>
	<?php endif; ?>
	
<table class="table table-bordered table-hover">
<section>
	<p class="alert alert-info">Drag to order menu and then click 'Save'</p>
	<div id="orderResult"></div>
	<input type="button" id="save" value="Save" class="btn btn-primary" />
</section>
</table>
</div>
</div>
</div>
<script>
$(function() {
	$.post('<?php echo site_url('page/order_ajax'); ?>', {}, function(data){
		$('#orderResult').html(data);
	});
	var post_data = {
    '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
	}
	$('#save').click(function(){
		oSortable = $('.sortable').nestedSortable('toArray');
		$('#orderResult').slideUp(function(){
			$.post('<?php echo site_url('page/order_ajax'); ?>', { sortable: oSortable }, function(data){
				$('#orderResult').html(data);
				$('#orderResult').slideDown();
			});
		});
		
	});
});
</script>