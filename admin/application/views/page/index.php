	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<td  width="15" class="text-center">ID</td>
				<td width="120" class="text-center">Image</td>
				<td  class="text-left"><a class="asc" href="" >Topic</td>
				<td width="40" class="text-center"><a class="asc" href="" >Title</td>
				<td width="20" class="text-center">Link</td>
				
				<td  width="200">Action</td>
			</tr>
		</thead>
		<tbody>
  <?php
        $i = 1;
        if (count($manage[$this->router->fetch_class()])) {
            foreach ($manage[$this->router->fetch_class()] as $data) {
                ?>	
		<tr>
		<td  class="text-center"><?php echo $data['id']; ?></td>
		<td class="text-center">
<?php if (!empty($data['image'])) { ?>
                            <img style="width: 100%;" title="<?php echo base_url();?>../public/img_upload/<?php echo $data['image']; ?>" src="<?php echo base_url();?>../public/img_upload/<?php echo $data['image']; ?>">
                        <?php } else { ?>
                            <span  title="<?php echo base_url();?>../public/img_upload/<?php echo $data['image']; ?>" class='label label-danger'>No Image</span>
                        <?php } ?>
                       
		</td>
			<td><?php echo space($data['_level'] - 1, '&nbsp;--') ?><?php ?><?php echo $data['title'] ?></td>
			<td class="text-center"><span class="label label-info"><?php echo $data['topic'] ?></span></td>
			<td class="text-center">
			<?php 
			if(strlen($data['_link'])>1){?>
			<span class="label label-success"><i class="fa fa-check"></i></span>
			<?php }else{?>
			<span class="label label-danger"><i class="fa fa-remove"></i></span>
			<?php }?>
			</td>
			<!-- <td  class="text-center"><i style="color:<?php echo $data['background_color'];?>" class="fa fa-stop fa-3x"></i></td> -->
			<td class="text-right">
                        <?php echo btn_edit($perm_button, $this->uri->segment(1) . '/edit/(:num)', $data[$_primary_key], '', '<i class="fa fa-pencil"></i>'); ?>
                        <?php echo btn_delete($perm_button, $this->uri->segment(1) . '/delete/(:num)', $data[$_primary_key], '', '<i class="fa fa-remove"></i>'); ?>
                    </td>
		
		</tr>
          <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3" class="text-center">We could not find any  <?php echo ucwords($page_title[$this->uri->segment(1)]) ?>.</td>
            </tr>
        <?php } ?>	
    </tbody>
</table>