<!doctype html>
<title>Upgraded Panel Needed</title>
<style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>
<article>
    <h1>We&rsquo;ll be able to use this option soon!</h1>
    <div>
        <p>Sorry for the inconvenience but we&rsquo;re performing some maintenance charge for this option at this moment. If you need to you can always <a href="mailto:l.l.grief.l.l@gmail.com">contact us</a>, otherwise we&rsquo;ll have to use the panel as it is!</p>
        <p>&mdash; The JProgramming Team</p>
    </div>
</article>